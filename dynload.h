/*_. Kernel 0.0: dynload.h */
/*_ , Header */
/*_  . Credits */
/* See HISTORY.org */
#define _KLINK_SOURCE

/*_  . Once */
#ifndef DYNLOAD_H
#define DYNLOAD_H

/*_  . Own includes */
#include "klink-private.h"
/*_ , Body */

KLINK_EXPORT pko klink_load_ext (klink * sc, pko arglist);
/*_ , Footers */
/*_  . End Once */
#endif

/*
Local variables:
c-file-style: "gnu"
mode: allout
End:
*/

