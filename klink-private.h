/*_. Klink 0.0: klink-private.h */
/*_ , Header */
/*_  . Purpose */
/* Private internal definitions */
/*_  . Credits and License */
/*
    Copyright (C) 2010,2011 Tom Breton (Tehom)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*_  . Once */

#ifndef _KLINK_PRIVATE_H
#define _KLINK_PRIVATE_H

/*_  . Own includes */

#include "klink.h"
#include <setjmp.h>

/*_  . C++ nonmangling */
#ifdef __cplusplus
extern "C"
{
#endif

/*_ , Body */
/*_  . Ubiquitous objects */
  pko K_T;
  pko K_F;
  pko K_NIL;
  pko K_EOF;
  pko K_INERT;
  pko K_IGNORE;

/*_  . Structs */
  
/*_   , Stack cells */
  
  typedef struct dump_stack_frame_cell * _kt_spagstack;


/*_   , The interpreter itself */

  struct klink
  {
    /** True internal variables **/
    _kt_spagstack dump;		/* The dynamic stack (Really one leaf
				   of a spaghetti stack).  The center
				   of the interpreter. */
    int done;			/* Whether eval loop should stop. */
    jmp_buf pseudocontinuation;	/* longjmping here goes directly to
				   next eval loop iteration.  (Or
				   terminates, if we're not in eval
				   loop) */

    /* Caches of ground-environment variables, provided for built-ins. */
    pko error_continuation;	/* What to do on error. */
    /* Don't seem to can use a root_continuation cache. */

    /** Caches and pseudo-caches of dynamic variables **/
    int tracing;		/* Are we tracing? */
    int new_tracing;		/* Are we tracing? */
    pko fail_continuation;	/* Logical fail */

    pko envir;		/* Current environment.  No cache key; it's
			   always in use so we don't deep-store it.
			   But conceptually it's a dynamic variable.  */

    /* Unused for now. */
    pko ca_inport;		/* Will become just a cache */
    pko ca_outport;		/* Same */
    pko ca_nesting;         /* Same */

    /** Obsolescent **/
    /* global pointers to special symbols */
    pko QUOTE;		/* pointer to syntax quote */

    pko QQUOTE;		/* pointer to symbol quasiquote */
    pko UNQUOTE;		/* pointer to symbol unquote */
    pko UNQUOTESP;		/* pointer to symbol unquote-splicing */
    pko COLON_HOOK;		/* *colon-hook* */
    pko SHARP_HOOK;		/* *sharp-hook* */

    /*** Scratch variables ***/
#define LINESIZE 1024
    char linebuff[LINESIZE];
#define STRBUFFSIZE 256
    char strbuff[STRBUFFSIZE];

    /** Special-purpose pseudo-passing  **/
    int tok;			/* Token from tokenizer to read */
    pko value;			/* Value from longjmp to eval
				   loop iteration */
    pko next_func;		/* Next functor, from frame pop to
				   eval loop iteration */
    int retcode;                /* return code, from exit operation to
				   true termination.  */

    void *ext_data;		/* For the benefit of foreign functions */
    struct klink_interface *vptr;
  };

/*_  . Function signatures */

  int is_string (pko p);
  char *string_value (pko p);
  int is_number (pko p);
  num nvalue (pko p);
  long ivalue (pko p);
  double rvalue (pko p);
  int is_integer (pko p);
  int is_real (pko p);
  int is_character (pko p);
  long charvalue (pko p);
  int is_vector (pko p);

  int is_port (pko p);

  int is_pair (pko p);
  pko pair_car (klink * sc, pko p);
  pko pair_cdr (klink * sc, pko p);
  pko set_car (klink * sc, pko p, pko q);
  pko set_cdr (klink * sc, pko p, pko q);

  int is_symbol (pko p);
  char *symname (klink * sc, pko p);



  int is_continuation (pko p);
  int is_promise (pko p);
  int is_environment (pko p);
  int is_immutable (pko p);
  void setimmutable (pko p);


/*_ , Footers */
/*_  . End nonmangling */
#ifdef __cplusplus
}
#endif

/*_  . End Once */
  
#endif

/*
Local variables:
c-file-style: "gnu"
mode: allout
End:
*/
