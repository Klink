/*_. Klink 0.0: fftypes.inc */
/*_ , Header */
/*_  . Purpose */
/* Function types for Kernel interpreter. */
/*_  . Credits and License */
/*
    Copyright (C) 2010,2011 Tom Breton (Tehom)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*_ , Body */

FFTYPE(b00a1,1 )
FFTYPE(b00a2,2 )
FFTYPE(bs0a2,2 )
FFTYPE(p00a0,0 )
FFTYPE(p00a1,1 )
FFTYPE(p00a2,2 )
FFTYPE(p00a3,3 )
FFTYPE(ps0a0,0 )
FFTYPE(ps0a1,1 )
FFTYPE(ps0a2,2 )
FFTYPE(ps0a3,3 )
FFTYPE(ps0a4,4 )
FFTYPE(ps0a5,5 )
FFTYPE(vs0a2,2 )
FFTYPE(vs0a3,3 )

