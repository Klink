/*_. Klink 0.0 */
/* Interpreter for the Kernel programming language*/
/*_ , Header */
/*_  . Credits and License */
/*
    Copyright (C) 2010,2011 Tom Breton (Tehom)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*_  . Includes */
#define _KLINK_SOURCE
#include "klink-private.h"
#ifndef WIN32
# include <unistd.h>
#endif
#ifdef WIN32
#define snprintf _snprintf
#endif
#if USE_DL
# include "dynload.h"
#endif
#if USE_MATH
# include <math.h>
#endif

#include <limits.h>
#include <float.h>
#include <ctype.h>
#include <assert.h>
#include <err.h>
#include <gc.h>

#if USE_STRCASECMP
#include <strings.h>
# ifndef __APPLE__
#  define stricmp strcasecmp
# endif
#endif

/* Used for documentation purposes, to signal functions in 'interface' */
#define INTERFACE

#include <string.h>
#include <stdlib.h>

#ifdef __APPLE__
static int
stricmp (const char *s1, const char *s2)
{
  unsigned char c1, c2;
  do
    {
      c1 = tolower (*s1);
      c2 = tolower (*s2);
      if (c1 < c2)
	return -1;
      else if (c1 > c2)
	return 1;
      s1++, s2++;
    }
  while (c1 != 0);
  return 0;
}
#endif /* __APPLE__ */

#if USE_STRLWR
static const char *
strlwr (char *s)
{
  const char *p = s;
  while (*s)
    {
      *s = tolower (*s);
      s++;
    }
  return p;
}
#endif

/*_  . Configuration */

#define banner "Klink 0.0\n"

#ifndef prompt
# define prompt "klink> "
#endif

#ifndef InitFile
# define InitFile "init.krn"
#endif

/*_ , Internal declarations */
/*_  . Macros */
/*_   , Name-mangling */
#define KEY(C_NAME)         _k_key_##C_NAME
#define DESTR_NAME(C_NAME)  _k_destructure_##C_NAME
#define OPER(C_NAME)        _k_oper_##C_NAME
#define APPLICATIVE(C_NAME) _k_appl_##C_NAME
#define CHAIN_NAME(C_NAME)  _k_chain_##C_NAME
#define CHKARRAY(C_NAME)    _k_chkvec_##C_NAME

/*_   , For forward declarations of combiners */
#define FORWARD_DECL_CFUNC(LINKAGE,SUFFIX,C_NAME)	\
  LINKAGE KERNEL_FUN_SIG_##SUFFIX (C_NAME);		\
  kt_boxed_cfunc OPER(C_NAME)

#define FORWARD_DECL_PRED(LINKAGE,C_NAME)	\
  FORWARD_DECL_CFUNC(LINKAGE,b00a1,C_NAME)

#define FORWARD_DECL_T_PRED(LINKAGE,C_NAME)	\
  LINKAGE kt_boxed_T OPER(C_NAME)

#define FORWARD_DECL_CHAIN(LINKAGE,C_NAME)	\
  LINKAGE kt_boxed_vector OPER(C_NAME)

#define FORWARD_DECL_APPLICATIVE(LINKAGE,SUFFIX,C_NAME)	\
  FORWARD_DECL_CFUNC(LINKAGE,SUFFIX,C_NAME);		\
  kt_boxed_encap APPLICATIVE(C_NAME);			\


/*_   , WITH_ARGS */
/* No noun/number agreement for WITH_1_ARGS because I prefer name
   regularity. */
#define WITH_1_ARGS(A1)				\
  pko A1 = arg1
#define WITH_2_ARGS(A1,A2)			\
  WITH_1_ARGS(A1), A2 = arg2
#define WITH_3_ARGS(A1,A2,A3)			\
  WITH_2_ARGS(A1,A2), A3 = arg3
#define WITH_4_ARGS(A1,A2,A3,A4)		\
  WITH_3_ARGS(A1,A2,A3), A4 = arg4
#define WITH_5_ARGS(A1,A2,A3,A4,A5)		\
  WITH_4_ARGS(A1,A2,A3,A4), A5 = arg5
/*_   , WITH_REPORTER */
#define WITH_REPORTER(SC)			\
  sc_or_null _err_reporter = (SC)
/*_   , Defining sub-T types */
/*_    . As C */
#define VEC_DEF_FROM_ARRAY(ARRAY_NAME)				\
  {								\
    sizeof(ARRAY_NAME)/sizeof(ARRAY_NAME[0]),			\
    ARRAY_NAME,							\
  }
/*_    . As boxed */
#define DEF_VEC(T_ENUM, NAME, ARRAY_NAME)			\
  kt_boxed_vector NAME =					\
  {								\
    T_ENUM,							\
    VEC_DEF_FROM_ARRAY (ARRAY_NAME),				\
  }								\

/*_   , Checking type */
/*_    . Certain destructurers and type checks */
#define K_ANY REF_OPER(is_any)
#define K_NO_TYPE       REF_KEY(K_TYCH_NO_TYPE)
#define K_ANY_SINGLETON REF_OBJ(_K_any_singleton)

/*_    . Internal: Arrays to be in typechecks and destructurers */
/* Elements of this array should not call Kernel - should be T_NO_K */
/* $$IMPROVE ME  Check that when registering combiners */
#define SIG_CHKARRAY(C_NAME) pko CHKARRAY(C_NAME)[]
/*_    . Boxed destructurers */
#define REF_DESTR(C_NAME) REF_OBJ(DESTR_NAME(C_NAME))
#define DESTR_DEF_FROM_ARRAY(ARRAY_NAME)	\
  { VEC_DEF_FROM_ARRAY (ARRAY_NAME), -1, }

#define DEF_DESTR(NAME,ARRAY_NAME)		\
  kt_boxed_destr_list NAME =			\
    {						\
      T_DESTRUCTURE | T_IMMUTABLE | T_NO_K,	\
      DESTR_DEF_FROM_ARRAY(ARRAY_NAME),		\
    }

  /* DEF_VEC(T_DESTRUCTURE | T_IMMUTABLE | T_NO_K, NAME, ARRAY_NAME) */

#define DEF_SIMPLE_DESTR(C_NAME)			\
  DEF_DESTR(DESTR_NAME(C_NAME), CHKARRAY(C_NAME))
  

/*_   , BOX macros */
/*_    . Allocators */
/* Awkward because we both declare stuff and assign stuff. */
#define ALLOC_BOX(NAME,T_ENUM,BOXTYPE)				\
  typedef BOXTYPE _TT;						\
  _TT * NAME = GC_MALLOC(sizeof(_TT));				\
  NAME->type = T_ENUM

/* ALLOC_BOX_PRESUME defines the following:
   pbox - a pointer to the box
   pdata - a pointer to the box's contents
 */
#define ALLOC_BOX_PRESUME(TYPE,T_ENUM)			\
  TYPE * pdata;						\
  ALLOC_BOX(pbox,T_ENUM,BOX_OF(TYPE));			\
  pdata = &(pbox)->data

/*_    . Unboxers */
/*_     , General */
#define WITH_BOX_TYPE(NAME,P)					\
  _kt_tag * NAME = &((kt_boxed_any *)(P))->type;	

/*_     , Raw */
/* This could mostly be an inlined function, but it wouldn't know
   types. */
#define WITH_UNBOXED_RAW(P,NAME,TYPE,BOXTYPE)		\
  TYPE * NAME;						\
  {							\
    typedef BOXTYPE _TT;				\
    _TT * _pbox = (_TT *)(P);				\
    NAME = &_pbox->data;				\
  }

/*_     , Entry points */
#define WITH_UNBOXED_UNSAFE(NAME,TYPE,P)		\
  WITH_UNBOXED_RAW(P,NAME,TYPE,BOX_OF(TYPE))


/* WITH_PSYC_UNBOXED defines the following:
   pdata - a pointer to the box's contents
 */
#define WITH_PSYC_UNBOXED(TYPE,P,T_ENUM,SC)	\
  assert_type(SC,(P),T_ENUM);			\
  WITH_UNBOXED_UNSAFE(pdata,TYPE,P)

/*_   , Boxes of */
/*_    . void */
#define REF_KEY(NAME) REF_OBJ(KEY(NAME))

#define BOX_OF_VOID(NAME)				\
  kt_boxed_void KEY(NAME) = { T_KEY | T_IMMUTABLE };	\
  pko NAME = REF_KEY(NAME)

/*_    . Operatives */
/* All operatives use this, regardless whether they are cfuncs,
   curried, etc. */
#define REF_OPER(C_NAME) REF_OBJ(OPER(C_NAME))

/*_    . Cfuncs */
#define DEF_CFUNC_RAW(NAME,SUFFIX,C_NAME,DESTR,XTRA_FLAGS)		\
  RGSTR(all-builtins,"C-" #C_NAME, REF_OBJ (NAME))			\
  kt_boxed_cfunc NAME =							\
    { T_CFUNC | T_IMMUTABLE | XTRA_FLAGS,				\
    {{C_NAME}, klink_ftype_##SUFFIX, DESTR, 0}};

#define DEF_CFUNC_PSYCNAME(SUFFIX,C_NAME, DESTR,XTRA_FLAGS)		\
  DEF_CFUNC_RAW(OPER(C_NAME),SUFFIX,C_NAME, DESTR,XTRA_FLAGS)

#define DEF_CFUNC(SUFFIX,C_NAME,DESTR,XTRA_FLAGS)	\
  KERNEL_FUN_SIG_##SUFFIX(C_NAME);			\
  DEF_CFUNC_PSYCNAME(SUFFIX,C_NAME,DESTR,XTRA_FLAGS);	\
  KERNEL_FUN_SIG_##SUFFIX(C_NAME)

#define DEF_SIMPLE_CFUNC(SUFFIX,C_NAME,XTRA_FLAGS)		\
  DEF_SIMPLE_DESTR(C_NAME);					\
  KERNEL_FUN_SIG_##SUFFIX(C_NAME);				\
  DEF_CFUNC_PSYCNAME(SUFFIX,C_NAME,REF_DESTR(C_NAME),XTRA_FLAGS);	\
  KERNEL_FUN_SIG_##SUFFIX(C_NAME)

/*_    . Applicatives */
#define REF_APPL(C_NAME) REF_OBJ(APPLICATIVE(C_NAME))

#define DEF_BOXED_APPLICATIVE(C_NAME,FF)	\
  kt_boxed_encap APPLICATIVE (C_NAME) =		\
  { T_ENCAP | T_IMMUTABLE,			\
    {REF_KEY(K_APPLICATIVE), FF}};

#define DEF_APPLICATIVE_W_DESTR(SUFFIX,C_NAME,DESTR,XTRA_FLAGS,RG,K_NAME) \
  RGSTR(RG,K_NAME, REF_APPL(C_NAME))					\
  KERNEL_FUN_SIG_##SUFFIX(C_NAME);					\
  DEF_CFUNC_PSYCNAME(SUFFIX,C_NAME,DESTR,XTRA_FLAGS);			\
  DEF_BOXED_APPLICATIVE(C_NAME, REF_OPER (C_NAME));			\
  KERNEL_FUN_SIG_##SUFFIX(C_NAME)

#define DEF_SIMPLE_APPLICATIVE(SUFFIX,C_NAME,XTRA_FLAGS,RG,K_NAME)	\
  RGSTR(RG,K_NAME, REF_APPL(C_NAME))					\
  DEF_SIMPLE_DESTR(C_NAME);						\
  KERNEL_FUN_SIG_##SUFFIX(C_NAME);					\
  DEF_CFUNC_PSYCNAME(SUFFIX,C_NAME,REF_DESTR(C_NAME),XTRA_FLAGS);	\
  DEF_BOXED_APPLICATIVE(C_NAME, REF_OPER (C_NAME));			\
  KERNEL_FUN_SIG_##SUFFIX(C_NAME)

/*_    . Abbreviations for predicates */
/* The underlying C function takes the whole value as its sole arg.
   Above that, in init.krn an applicative wrapper applies it over a
   list, using `every?'.
 */
#define DEF_SIMPLE_PRED(C_NAME,XTRA_FLAGS,RG,K_NAME)	\
  RGSTR(RG,K_NAME, REF_OPER(C_NAME))			\
  DEF_CFUNC(b00a1,C_NAME,K_ANY_SINGLETON,XTRA_FLAGS)  

/* The cfunc is there just to be exported for C use. */
#define DEF_T_PRED(C_NAME,T_ENUM,RG,K_NAME)			\
  RGSTR(RG,K_NAME, REF_OPER(C_NAME))				\
  kt_boxed_T OPER(C_NAME) =					\
  { T_TYPEP | T_IMMUTABLE | T_NO_K, {T_ENUM}};			\
  int C_NAME(pko p) { return is_type(p,T_ENUM); }
  

/*_    . Curried Functions */

#define DEF_BOXED_CURRIED(CURRY_NAME,DECURRIER,ARGS,NEXT)	\
  RGSTR(all-builtins,"C-" #CURRY_NAME, REF_OBJ (CURRY_NAME))	\
  kt_boxed_curried CURRY_NAME =					\
  { T_CURRIED  | T_IMMUTABLE,					\
    {DECURRIER, ARGS, NEXT, 0}};
/*_    . Pairs */
#define DEF_BOXED_PAIR(C_NAME,CAR,CDR)	\
  boxed_vec2 C_NAME =			\
    { T_PAIR | T_IMMUTABLE, {CAR, CDR, }}

/* $$OBSOLESCENT */
#define DEF_LISTSTAR2 DEF_BOXED_PAIR

#define DEF_LISTSTAR3(C_NAME,A1,A2,A3) \
  DEF_BOXED_PAIR(C_NAME##__1,A2,A3);   \
  DEF_BOXED_PAIR(C_NAME,A1,REF_OBJ(C_NAME##__1))

#define DEF_LIST1(C_NAME,A1)    DEF_LISTSTAR2(C_NAME,A1,   REF_KEY(K_NIL))
#define DEF_LIST2(C_NAME,A1,A2) DEF_LISTSTAR3(C_NAME,A1,A2,REF_KEY(K_NIL))
  
/*_   , Building objects in C */
#define ANON_OBJ( TYPE, X )			\
  (((BOX_OF( TYPE )[]) { X })[0])

/* Middle is the same as ANON_OBJ but we can't just use that because
   of expansion issues */
#define ANON_REF( TYPE, X )			\
  REF_OBJ((((BOX_OF( TYPE )[]) { X })[0]))

#define PAIR_DEF( CAR, CDR )			\
  { T_PAIR | T_IMMUTABLE, { CAR, CDR, }, }

#define ANON_PAIR( CAR, CDR ) \
  ANON_REF(kt_vec2, PAIR_DEF( CAR, CDR ))
							
#define INT_DEF( N )				\
  { T_NUMBER | T_IMMUTABLE, { 1, { N }, }, }


/*_   , Building lists in C */
/*_    . Anonymous lists */
/*_     , Dotted */
#define ANON_LISTSTAR2(A1, A2)			\
  ANON_PAIR(A1, A2)

#define ANON_LISTSTAR3(A1, A2, A3)		\
  ANON_PAIR(A1, ANON_LISTSTAR2(A2, A3))

#define ANON_LISTSTAR4(A1, A2, A3, A4)		\
  ANON_PAIR(A1, ANON_LISTSTAR3(A2, A3, A4))

/*_     , Undotted */
#define ANON_LIST1(A1) \
  ANON_LISTSTAR2(A1, REF_KEY(K_NIL))

#define ANON_LIST2(A1, A2)			\
  ANON_PAIR(A1, ANON_LIST1(A2))

#define ANON_LIST3(A1, A2, A3)			\
  ANON_PAIR(A1, ANON_LIST2(A2, A3))

#define ANON_LIST4(A1, A2, A3, A4)		\
  ANON_PAIR(A1, ANON_LIST3(A2, A3, A4))

#define ANON_LIST5(A1, A2, A3, A4, A5)		\
  ANON_PAIR(A1, ANON_LIST4(A2, A3, A4, A5))

#define ANON_LIST6(A1, A2, A3, A4, A5, A6)	\
  ANON_PAIR(A1, ANON_LIST5(A2, A3, A4, A5, A6))


/*_    . Dynamic lists */
/*_     , Dotted */
#define LISTSTAR2(A1, A2)			\
  cons (A1, A2)
#define LISTSTAR3(A1, A2, A3)			\
  cons (A1, LISTSTAR2(A2, A3))
#define LISTSTAR4(A1, A2, A3, A4)		\
  cons (A1, LISTSTAR3(A2, A3, A4))

/*_     , Undotted */

#define LIST1(A1)				\
  cons (A1, K_NIL)
#define LIST2(A1, A2)				\
  cons (A1, LIST1 (A2))
#define LIST3(A1, A2, A3)			\
  cons (A1, LIST2 (A2, A3))
#define LIST4(A1, A2, A3, A4)			\
  cons (A1, LIST3 (A2, A3, A4))
#define LIST5(A1, A2, A3, A4, A5)		\
  cons (A1, LIST4 (A2, A3, A4, A5))
#define LIST6(A1, A2, A3, A4, A5, A6)		\
  cons (A1, LIST5 (A2, A3, A4, A5, A6))

/*_   , Kernel continuation macros */
/*_    . W/o decurrying */
#define CONTIN_0_RAW(C_NAME,SC)			\
  klink_push_cont((SC), (C_NAME))
#define CONTIN_0(OPER_NAME,SC)			\
  klink_push_cont((SC), REF_OPER (OPER_NAME))

/*_    . Dotting */
/* The use of REF_OPER requires these to be macros. */

#define CONTIN_1R(DECURRIER,C_NAME,SC,ARGS)				\
  klink_push_cont((SC),							\
		  mk_curried(DECURRIER, ARGS, REF_OPER (C_NAME)))

#define CONTIN_2R(DECURRIER,C_NAME,SC,ARG1,ARG2)			\
  CONTIN_1R(DECURRIER,C_NAME,SC,cons(ARG1,ARG2))

#define CONTIN_3R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3)			\
  CONTIN_2R(DECURRIER,C_NAME,SC,ARG1,cons(ARG2,ARG3))

#define CONTIN_4R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,ARG4)		\
  CONTIN_3R(DECURRIER,C_NAME,SC,ARG1,ARG2,cons(ARG3,ARG4))

#define CONTIN_5R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,ARG4,ARG5)		\
  CONTIN_4R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,cons(ARG4,ARG5))

#define CONTIN_6R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,ARG4,ARG5,ARG6)	\
  CONTIN_5R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,ARG4,cons(ARG5,ARG6))


/*_    . Straight */
#define CONTIN_1(DECURRIER,C_NAME,SC,ARG1)	\
  CONTIN_2R(DECURRIER,C_NAME,SC,ARG1,K_NIL)

#define CONTIN_2(DECURRIER,C_NAME,SC,ARG1,ARG2)		\
  CONTIN_3R(DECURRIER,C_NAME,SC,ARG1,ARG2,K_NIL)

#define CONTIN_3(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3)	\
  CONTIN_4R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,K_NIL)

#define CONTIN_4(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,ARG4)	\
  CONTIN_5R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,ARG4,K_NIL)

#define CONTIN_5(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,ARG4,ARG5)	\
  CONTIN_6R(DECURRIER,C_NAME,SC,ARG1,ARG2,ARG3,ARG4,ARG5,K_NIL)

/*_   , C to bool */
#define kernel_bool(tf)    ((tf) ? K_T : K_F)

/*_   , Control macros */

/* These never return because _klink_error_1 longjmps. */
/* $$IMPROVE ME If GCC is used or C99 is available, use __func__ as the function name. */ 
#define KERNEL_ERROR_1(sc,s, a) { _klink_error_1(sc,s,a); return 0; }
#define KERNEL_ERROR_0(sc,s)    { _klink_error_1(sc,s,0); return 0; }

/*_  . Enumerations */
/*_   , The port types & flags */

enum klink_port_kind
{
  port_free = 0,
  port_file = 1,
  port_string = 2,
  port_srfi6 = 4,
  port_input = 16,
  port_output = 32,
  port_saw_EOF = 64,
};

/*_   , Tokens */

typedef enum klink_token
{
  TOK_LPAREN,
  TOK_RPAREN,
  TOK_DOT,
  TOK_ATOM,
  TOK_QUOTE,
  TOK_COMMENT,
  TOK_DQUOTE,
  TOK_BQUOTE,
  TOK_COMMA,
  TOK_ATMARK,
  TOK_SHARP,
  TOK_SHARP_CONST,
  TOK_VEC,

  TOK_EOF = -1,
} token_t;
/*_   , List metrics */
typedef enum
{
  lm_num_pairs,
  lm_num_nils,
  lm_acyc_len,
  lm_cyc_len,
  lm_max,
} lm_index;
typedef int int4[lm_max];

/*_  . Struct definitions */

/*_   , FF */
typedef BOX_OF (kt_cfunc)
kt_boxed_cfunc;

/*_   , Encap */
typedef
struct
{
  /* Object identity lets us compare instances. */
  pko  type;
  pko  value;
} kt_encap;

typedef BOX_OF (kt_encap)
kt_boxed_encap;

/*_   , Curried calls */

typedef pko (* decurrier_f) (klink * sc, pko args, pko value);

typedef
struct
{
  decurrier_f decurrier;
  pko     args;
  pko     next;
  pko     argcheck;
} kt_curried;

typedef BOX_OF (kt_curried)
kt_boxed_curried;

/*_   , T_typep calls */
/*_    . Structures */
typedef struct
{
  _kt_tag T_tag;
} typep_t;

typedef BOX_OF(typep_t)
kt_boxed_T;

/*_   , Ports */

  typedef struct port
  {
    unsigned char kind;
    union
    {
      struct
      {
	FILE *file;
	int closeit;
#if SHOW_ERROR_LINE
	int curr_line;
	char *filename;
#endif
      } stdio;
      struct
      {
	char *start;
	char *past_the_end;
	char *curr;
      } string;
    } rep;
  } port;
/*_   , Vectors */
typedef struct
{
  long int len;
  pko * els;
} kt_vector;

typedef BOX_OF(kt_vector)
kt_boxed_vector;
/*_   , Destructurer */
/*_     , kt_destr_list */
typedef struct
{
  kt_vector cvec;
  int       num_targets;
} kt_destr_list;

typedef BOX_OF(kt_destr_list)
kt_boxed_destr_list;

/*_  . Signatures */
/*_   , Initialization */
static void klink_setup_error_cont (klink * sc);
static void klink_cycle_restarting (klink * sc, pko combiner);
static int  klink_cycle_no_restart (klink * sc, pko combiner);
static void _klink_cycle (klink * sc);


/*_   , Error handling */
static void _klink_error_1 (klink * sc, const char *s, pko a);
/*_    . Stack control */
static int klink_pop_cont (klink * sc);

/*_   , Evaluation */
static pko klink_call_cfunc (klink * sc, pko functor, pko env, pko args);
FORWARD_DECL_CFUNC (static, ps0a2, k_resume_to_cfunc);

/*_    . load */
extern pko
mk_load_ix (int x, int y);
extern pko
mk_load (pko data);
/*_    . store */
extern pko
mk_store (pko data, int depth);
/*_    . curried */
/* $$DEPRECATED */
static pko
call_curried(klink * sc, pko curried, pko value);

/*_   , Top level operatives */
FORWARD_DECL_APPLICATIVE(static,ps0a0,kernel_repl);
FORWARD_DECL_APPLICATIVE(static,ps0a0,kernel_rel);
FORWARD_DECL_APPLICATIVE(static,ps0a1,kernel_internal_eval);

/*_   , Oblist */
static INLINE pko oblist_find_by_name (const char *name);
static pko oblist_add_by_name (const char *name);

/*_   , Numbers */
static pko mk_number (num n);
/*_    . Operations */
static num num_add (num a, num b);
static num num_mul (num a, num b);
static num num_div (num a, num b);
static num num_intdiv (num a, num b);
static num num_sub (num a, num b);
static num num_rem (num a, num b);
static num num_mod (num a, num b);
static int num_eq (num a, num b);
static int num_gt (num a, num b);
static int num_ge (num a, num b);
static int num_lt (num a, num b);
static int num_le (num a, num b);

#if USE_MATH
static double round_per_R5RS (double x);
#endif

/*_   , Lists and vectors */
FORWARD_DECL_PRED (extern, is_finite_list);
FORWARD_DECL_PRED (extern, is_countable_list);
extern int list_length (pko a);
static pko reverse (klink * sc, pko a);
static pko unsafe_v2reverse_in_place (pko term, pko list);
static pko append (klink * sc, pko a, pko b);

static pko alloc_basvector (int len, _kt_tag t_enum);
static void unsafe_basvector_fill (pko vec, pko obj);

static pko mk_vector (int len, pko fill);
INTERFACE static void fill_vector (pko vec, pko obj);
INTERFACE static pko vector_elem (pko vec, int ielem);
INTERFACE static void set_vector_elem (pko vec, int ielem, pko a);
INTERFACE static int vector_len (pko vec);
extern void
get_list_metrics_aux (pko a, int4 presults);

extern pko
k_counted_map_car(klink * sc, int count, pko list, _kt_tag t_enum);
extern pko
k_counted_map_cdr(klink * sc, int count, pko list, _kt_tag t_enum);

/*_   , Ports */
static pko port_from_filename (const char *fn, int prop);
static pko port_from_file (FILE *, int prop);
static pko port_from_string (char *start, char *past_the_end, int prop);
static void port_close (pko p, int flag);
static void port_finalize_file(GC_PTR obj, GC_PTR client_data);
static port *port_rep_from_filename (const char *fn, int prop);
static port *port_rep_from_file (FILE *, int prop);
static port *port_rep_from_string (char *start, char *past_the_end, int prop);
static void port_close_port (port * pt, int flag);
INLINE port * portvalue (pko p);
static int basic_inchar (port * pt);
static int inchar (port *pt);
static void backchar (port * pt, int c);
/*_   , Typechecks */
FORWARD_DECL_APPLICATIVE (extern,ps0a1, mk_typecheck);
FORWARD_DECL_APPLICATIVE (extern,ps0a1, mk_destructurer);
FORWARD_DECL_CFUNC (extern, ps0a5, destructure_resume);
FORWARD_DECL_PRED (extern, is_any);
FORWARD_DECL_T_PRED (extern, is_environment);
FORWARD_DECL_PRED (extern, is_integer);
/*_   , Promises */
FORWARD_DECL_CFUNC (extern,ps0a2,handle_promise_result);
FORWARD_DECL_CFUNC (extern, ps0a1, mk_promise_lazy);
FORWARD_DECL_APPLICATIVE (extern, ps0a1, force);
/*_   , About encapsulation */
FORWARD_DECL_CFUNC (static,b00a2, is_encap);
FORWARD_DECL_CFUNC (static,p00a2, mk_encap);
FORWARD_DECL_CFUNC (static,ps0a2, unencap);
FORWARD_DECL_APPLICATIVE (extern,p00a0, mk_encapsulation_type);

/*_   , About combiners per se */
FORWARD_DECL_PRED(extern,is_combiner);
/*_   , About operatives */
FORWARD_DECL_PRED(extern,is_operative);
extern void
schedule_rv_list(klink * sc, pko list);

/*_   , About applicatives */

FORWARD_DECL_PRED(extern,is_applicative);
FORWARD_DECL_APPLICATIVE(extern,p00a1,wrap);
FORWARD_DECL_APPLICATIVE(extern,ps0a1,unwrap);
FORWARD_DECL_APPLICATIVE(extern,p00a1,unwrap_all);

/*_   , About currying */
static INLINE int
is_curried (pko p);

/*_    . Decurriers */
static pko dcrry_2A01VLL (klink * sc, pko args, pko value);
static pko dcrry_3A01dotVLL (klink * sc, pko args, pko value);
static pko dcrry_2CA01VLLA02 (klink * sc, pko args, pko value);
/* May not be needed */
static pko dcrry_3A01A02VLL  (klink * sc, pko args, pko value);
static pko dcrry_2ALLVLL     (klink * sc, pko args, pko value);
static pko dcrry_2ALLV01     (klink * sc, pko args, pko value);

static pko dcrry_NdotALL (klink * sc, pko args, pko value);
#define dcrry_1A01    dcrry_NdotALL
#define dcrry_1dotALL dcrry_NdotALL
#define dcrry_2dotALL dcrry_NdotALL
#define dcrry_3dotALL dcrry_NdotALL
#define dcrry_4dotALL dcrry_NdotALL

static pko dcrry_1ALL (klink * sc, pko args, pko value);

static pko dcrry_5ALLdotVLL (klink * sc, pko args, pko value);
#define dcrry_3ALLdotVLL dcrry_5ALLdotVLL

static pko dcrry_NVLLdotALL (klink * sc, pko args, pko value);
#define dcrry_2VLLdotALL dcrry_NVLLdotALL
#define dcrry_3VLLdotALL dcrry_NVLLdotALL
#define dcrry_4VLLdotALL dcrry_NVLLdotALL
#define dcrry_5VLLdotALL dcrry_NVLLdotALL

static pko dcrry_1VLL (klink * sc, pko args, pko value);
static pko dcrry_NCVLLA01dotAX1 (klink * sc, pko args, pko value);
#define dcrry_2CVLLA01dotAX1 dcrry_NCVLLA01dotAX1
#define dcrry_3CVLLA01dotAX1 dcrry_NCVLLA01dotAX1
#define dcrry_4CVLLA01dotAX1 dcrry_NCVLLA01dotAX1
#define dcrry_5CVLLA01dotAX1 dcrry_NCVLLA01dotAX1
/*_    . Associated */
FORWARD_DECL_CFUNC(static,ps0a4,values_pair);


/*_   , Of Kernel evaluation */
/*_    .  Public functions */
FORWARD_DECL_APPLICATIVE(extern,ps0a2,kernel_eval);
FORWARD_DECL_CFUNC (extern,ps0a3, vau_1);
/*_    . Other signatures */
FORWARD_DECL_APPLICATIVE(static,ps0a3,kernel_eval_aux);
FORWARD_DECL_APPLICATIVE(static,ps0a3,kernel_mapeval);
FORWARD_DECL_APPLICATIVE(static,ps0a3, kernel_mapand_aux);
FORWARD_DECL_APPLICATIVE(extern,ps0a2, kernel_mapand);
FORWARD_DECL_APPLICATIVE(static,ps0a5,eval_vau);

/*_   , Reading */

FORWARD_DECL_APPLICATIVE(static,ps0a0,kernel_read_internal);
FORWARD_DECL_CFUNC(extern,ps0a0,kernel_read_sexp);
FORWARD_DECL_CFUNC(static,ps0a2,kernel_read_list);
FORWARD_DECL_CFUNC(static,ps0a2,kernel_treat_dotted_list);
FORWARD_DECL_CFUNC(static,ps0a1,kernel_treat_qquoted_vec);

static INLINE int is_one_of (char *s, int c);
static long binary_decode (const char *s);
static char *readstr_upto (klink * sc, char *delim);
static pko readstrexp (klink * sc);
static INLINE int skipspace (klink * sc);
static int token (klink * sc);
static pko mk_atom (klink * sc, char *q);
static pko mk_sharp_const (char *name);

/*_   , Printing */
/* $$IMPROVE ME These should mostly be just operatives. */
FORWARD_DECL_APPLICATIVE(static,ps0a2,kernel_print_sexp);
FORWARD_DECL_APPLICATIVE(static,ps0a3,kernel_print_sexp_aux);
FORWARD_DECL_APPLICATIVE(static,ps0a3,kernel_print_list);
FORWARD_DECL_APPLICATIVE(static,ps0a4,kernel_print_vec_from);
static kt_boxed_curried k_print_terminate_list;

static void printslashstring (klink * sc, char *s, int len);
static void atom2str (klink * sc, pko l, char **pp, int *plen);
static void printatom (klink * sc, pko l);

/*_   , Stack & continuations */
/*_    . Continuations */
static pko mk_continuation (_kt_spagstack d);
static void klink_push_cont (klink * sc, pko combiner);
static _kt_spagstack 
klink_push_cont_aux (_kt_spagstack old_frame, pko ff, pko env);
FORWARD_DECL_APPLICATIVE(extern,p00a1,continuation_to_applicative);
FORWARD_DECL_CFUNC(static,vs0a2,invoke_continuation);
FORWARD_DECL_CFUNC(static,ps0a2,continue_abnormally);
static _kt_spagstack special_dynxtnt
(_kt_spagstack outer, _kt_spagstack prox_dest, pko envir);
static _kt_spagstack
cont_dump (pko p);

/*_    . Dynamic bindings */
static void klink_push_dyn_binding (klink * sc, pko id, pko value);
static pko klink_find_dyn_binding(klink * sc, pko id);
/*_    . Profiling */
struct stack_profiling;
static void
k_profiling_done_frame(klink * sc, struct stack_profiling * profile);
/*_    . Stack args */
static pko
get_nth_arg( _kt_spagstack frame, int n );
static void
push_arg (klink * sc, pko value);

/*_   , Environment and defining */
FORWARD_DECL_CFUNC(static,vs0a3,kernel_define_tree);
FORWARD_DECL_CFUNC(extern,p00a3,kernel_define);
FORWARD_DECL_CFUNC(extern,ps0a2,eval_define);
FORWARD_DECL_CFUNC(extern,ps0a3,set);
FORWARD_DECL_CFUNC(static,ps0a4,set_aux);

static pko find_slot_in_env (pko env, pko sym, int all);
static INLINE pko slot_value_in_env (pko slot);
static INLINE void set_slot_in_env (pko slot, pko value);
static pko
reverse_find_slot_in_env_aux (pko env, pko value);
/*_    . Standard environment */
FORWARD_DECL_CFUNC(extern,p00a0, mk_std_environment);
FORWARD_DECL_APPLICATIVE (extern,ps0a0, get_current_environment);
/*_   , Misc kernel functions */

FORWARD_DECL_CFUNC(extern,ps0a1,arg1);
FORWARD_DECL_APPLICATIVE(extern,ps0a1,val2val)

/*_   , Error functions */
FORWARD_DECL_CFUNC(static,ps0a1,kernel_err);
FORWARD_DECL_CFUNC(static,ps0a1,kernel_err_x);

/*_   , For DL if present */
#if USE_DL
FORWARD_DECL_APPLICATIVE(extern,ps0a1,klink_load_ext);
#endif

/*_   , Symbols */
static pko mk_symbol_obj (const char *name);

/*_   , Strings */
static char *store_string (int len, const char *str, char fill);

/*_  . Object declarations */
/*_   , Keys */
/* These objects are declared here because some macros use them, but
   should not be directly used. */
/* $$IMPROVE ME Somehow hide these better without hiding it from the
   applicative & destructure macros. */
kt_boxed_void KEY(K_APPLICATIVE);
kt_boxed_void KEY(K_NIL);
/*_   , Typechecks */
kt_boxed_destr_list _K_any_singleton;
/*_   , Pointers to base environments */
static pko print_lookup_env;
static pko all_builtins_env;
static pko ground_env;
static pko typecheck_env_syms;
/* Caches */
static pko print_lookup_unwraps;
static pko print_lookup_to_xary;

/*_ , Body */
/*_  . Low-level treating T-types */
/*_   , Type itself */
/*_    . _get_type */
INLINE int
_get_type (pko p)
{
  WITH_BOX_TYPE(ptype,p);
  return *ptype & T_MASKTYPE;
}

/*_    . is_type */
INLINE int
is_type (pko p, int T_index)
{
  return _get_type (p) == T_index;
}
/*_    . type_err_string */
const char *
type_err_string(_kt_tag t_enum)
{
  switch(t_enum)
    {
    case T_STRING:
      return "Must be a string";
    case T_NUMBER:
      return "Must be a number";
    case T_SYMBOL:
      return "Must be a symbol";
    case T_PAIR:
      return "Must be a pair";
    case T_CHARACTER:
      return "Must be a character";
    case T_PORT:
      return "Must be a port";
    case T_ENCAP:
      return "Must be an encapsulation";
    case T_CONTINUATION:
      return "Must be a continuation";
    case T_ENV_FRAME:
      return "Must be an environment";
    case T_RECURRENCES:
      return "Must be a recurrence table";
    case T_RECUR_TRACKER:
      return "Must be a recurrence tracker";
    case T_DESTR_RESULT:
      return "Must be a destructure result";
    default:
      /* Left out types that shouldn't be distinguished in Kernel. */
      return "Error message for this type needs to be coded";
    }
}  
/*_    . assert_type */
/* If sc is given, it's a assertion making a Kernel error, otherwise
   it's a C assertion. */
INLINE void
assert_type (sc_or_null sc, pko p, _kt_tag t_enum)
{
  if(sc && (_get_type(p) != (t_enum)))
    {
      const char * err_msg = type_err_string(t_enum);
      _klink_error_1(sc,err_msg,p);
      return;			/* NOTREACHED */
    }
  else
    { assert (_get_type(p) == (t_enum)); }
}

/*_   , Mutability */

INTERFACE INLINE int
is_immutable (pko p)
{
  WITH_BOX_TYPE(ptype,p);
  return *ptype & T_IMMUTABLE;
}

INTERFACE INLINE void
setimmutable (pko p)
{
  WITH_BOX_TYPE(ptype,p);
  *ptype |= T_IMMUTABLE;
}

/* If sc is given, it's a assertion making a Kernel error, otherwise
   it's a C assertion. */
INLINE void
assert_mutable (sc_or_null sc, pko p)
{
  WITH_BOX_TYPE(ptype,p);
  if(sc && (*ptype & T_IMMUTABLE))
    {
      _klink_error_1(sc,"Attempt to mutate immutable object",p);
      return;
    }
  else
    { assert(!(*ptype & T_IMMUTABLE)); }
}

#define DEBUG_assert_mutable assert_mutable

/*_   , No-call-Kernel */
inline int
no_call_k(pko p)
{
  WITH_BOX_TYPE(ptype,p);
  return *ptype & T_NO_K;
}
/*_   , eq? */
SIG_CHKARRAY(eqp) = { K_ANY, K_ANY, };
DEF_SIMPLE_APPLICATIVE(p00a2,eqp,T_NO_K,ground,"eq?")
{
  WITH_2_ARGS(a,b);
  return kernel_bool(a == b);
}
/*_  . Low-level object types */
/*_   , vec2 (Low lists) */
/*_    . Struct */
typedef struct
{
  pko _car;
  pko _cdr;
} kt_vec2;
typedef BOX_OF(kt_vec2) boxed_vec2;

/*_    . Type assert */
/* $$IMPROVE ME  Disable this if DEBUG_LEVEL is low */
void assert_T_is_v2(_kt_tag t_enum)
{
  t_enum &= T_MASKTYPE;
  assert(
	 t_enum == T_PAIR
	 || t_enum == T_ENV_PAIR
	 || t_enum == T_ENV_FRAME
	 || t_enum == T_PROMISE
	 || t_enum == T_DESTR_RESULT
	 );
}  

/*_    . Create */
pko
v2cons (_kt_tag t_enum, pko a, pko b)
{
  ALLOC_BOX_PRESUME (kt_vec2, t_enum);
  pbox->data._car = a;
  pbox->data._cdr = b;
  return PTR2PKO(pbox);
}

/*_    . Unsafe operations (Typechecks can be disabled) */
INLINE pko
unsafe_v2car (pko p)
{
  assert_T_is_v2(_get_type(p));
  WITH_UNBOXED_UNSAFE(pdata,kt_vec2,p);
  return pdata->_car;
}

INLINE pko
unsafe_v2cdr (pko p)
{
  assert_T_is_v2(_get_type(p));
  WITH_UNBOXED_UNSAFE(pdata,kt_vec2,p);
  return pdata->_cdr;
}

INLINE void
unsafe_v2set_car (pko p, pko q)
{
  assert_T_is_v2(_get_type(p));
  DEBUG_assert_mutable(0,p);
  WITH_UNBOXED_UNSAFE(pdata,kt_vec2,p);
  pdata->_car = q;
  return;
}

INLINE void
unsafe_v2set_cdr (pko p, pko q)
{
  assert_T_is_v2(_get_type(p));
  DEBUG_assert_mutable(0,p);
  WITH_UNBOXED_UNSAFE(pdata,kt_vec2,p);
  pdata->_cdr = q;
  return;
}

/*_    . Checked operations */
pko
v2car (sc_or_null err_reporter, _kt_tag t_enum, pko p)
{
  assert_type(err_reporter,p,t_enum);
  return unsafe_v2car(p);
}

pko
v2cdr (sc_or_null err_reporter, _kt_tag t_enum, pko p)
{
  assert_type(err_reporter,p,t_enum);
  return unsafe_v2cdr(p);
}

void
v2set_car (sc_or_null err_reporter, _kt_tag t_enum, pko p, pko q)
{
  assert_type(err_reporter,p,t_enum);
  assert_mutable(err_reporter,p);
  unsafe_v2set_car(p,q);
  return;
}

void
v2set_cdr (sc_or_null err_reporter, _kt_tag t_enum, pko p, pko q)
{
  assert_type(err_reporter,p,t_enum);
  assert_mutable(err_reporter,p);
  unsafe_v2set_cdr(p,q);
  return;
}

/*_    . "Psychic" macros */
#define WITH_V2(T_ENUM)				\
  _kt_tag _t_enum = T_ENUM;		\
  assert_T_is_v2(_t_enum)

/*  These expect WITH_REPORTER and WITH_V2 to be used in scope. */
#define PSYC_v2cons(A,B) v2cons (_t_enum, (A), (B))
#define PSYC_v2car(X) v2car (_err_reporter, _t_enum, (X))
#define PSYC_v2cdr(X) v2cdr (_err_reporter, _t_enum, (X))
#define PSYC_v2set_car(A,B) v2set_car (_err_reporter, _t_enum, (A), (B))
#define PSYC_v2set_cdr(A,B) v2set_cdr (_err_reporter, _t_enum, (A), (B))

/*_    . Container macros */

/* This expects _EXPLORE_FUNC to be defined as a macro taking OBJ,
   inspecting it but not mutating it. */
#define EXPLORE_v2(OBJ)				\
  {						\
    WITH_UNBOXED_UNSAFE(pdata,kt_vec2,OBJ);	\
    _EXPLORE_FUNC(pdata->_car);			\
    _EXPLORE_FUNC(pdata->_cdr);			\
  }

/* #define _EXPLORE_FUNC(X) trace_tree_cycles(X, p_cycles_data) */

/*_    . Low list operations */
/*_     , v2list_star */
pko v2list_star(sc_or_null sc, pko d, _kt_tag t_enum)
{
  WITH_REPORTER(sc);
  WITH_V2(t_enum);
  pko p, q;
  pko cdr_d = PSYC_v2cdr (d);
  if (cdr_d == K_NIL)
    {
      return PSYC_v2car (d);
    }
  p = PSYC_v2cons (PSYC_v2car (d), cdr_d);
  q = p;

  while (PSYC_v2cdr (PSYC_v2cdr (p)) != K_NIL)
    {
      pko cdr_p = PSYC_v2cdr (p);
      d = PSYC_v2cons (PSYC_v2car (p), cdr_p);
      if (PSYC_v2cdr (cdr_p) != K_NIL)
	{
	  p = PSYC_v2cdr (d);
	}
    }
  PSYC_v2set_cdr (p, PSYC_v2car (PSYC_v2cdr (p)));
  return q;
}

/*_     , reverse list -- produce new list */
pko v2reverse(pko a, _kt_tag t_enum)
{
  WITH_V2(t_enum);
  pko p = K_NIL;
  for (; is_type (a, t_enum); a = unsafe_v2cdr (a))
    {
      p = v2cons (t_enum, unsafe_v2car (a), p);
    }
  return (p);
}

/*_     , reverse list -- in-place (Not typechecked) */
/* last_cdr will be the tail of the resulting list.  It is usually
   K_NIL.

   list is the list to be reversed.  Caller guarantees that list is a
   proper list, each link being either some type of vec2 or K_NIL.
*/ 
static pko
unsafe_v2reverse_in_place (pko last_cdr, pko list)
{
  pko p = list, result = last_cdr;
  while (p != K_NIL)
    {
      pko scratch = unsafe_v2cdr (p);
      unsafe_v2set_cdr (p, result);
      result = p;
      p = scratch;
    }
  return (result);
}
/*_     , append list -- produce new list */
pko v2append(sc_or_null err_reporter, pko a, pko b, _kt_tag t_enum)
{
  WITH_V2(t_enum);
  if (a == K_NIL)
    { return b; }
  else
    {
      a = v2reverse (a, t_enum);
      /* Correct even if b is nil or a non-list. */
      return unsafe_v2reverse_in_place(b, a);
    }
}


/*_   , basvectors (Low vectors) */
/*_    . Struct */
/* Above so it can be visible to early typecheck declarations. */
/*_    . Type assert */
void assert_T_is_basvector(_kt_tag t_enum)
{
  t_enum &= T_MASKTYPE;
  assert(
	 t_enum == T_VECTOR ||
	 t_enum == T_TYPECHECK ||
	 t_enum == T_DESTRUCTURE
	 );
}

/*_    . Initialize */
/*_     , rough_basvec_init */
/* Create the elements but don't assign to them. */
static void
basvec_init_rough (kt_vector * pvec, int len)
{
  pvec->len = len;
  pvec->els = (pko *)GC_MALLOC ((sizeof (pko) * len));
}
/*_     , basvec_init_by_list */
/* Initialize the elements of PVEC with the first LEN elements of
   ARGS.  ARGS must be a list with at least LEN elements. */
static void
basvec_init_by_list (kt_vector * pvec, pko args)
{
  WITH_REPORTER (0);
  int i;
  const int num = pvec->len;
  pko x;
  for (x = args, i = 0; i < num; x = cdr (x), i++)
    {
      assert (is_pair (x));
      pvec->els[i] = car (x);
    }
}
/*_     , basvec_init_by_array */
/* Initialize the elements of PVEC with the first LEN elements of
   ARRAY.  ARRAY must be an array with at least LEN elements. */
static void
basvec_init_by_array (kt_vector * pvec, pko * array)
{
  int i;
  const int num = pvec->len;
  for (i = 0; i < num; i++)
    {
      pvec->els [i] = array [i];
    }
}
/*_     , basvec_init_by_single */
static void
basvec_init_by_single (kt_vector * pvec, pko obj)
{
  int i;
  const int num = pvec->len;

  for (i = 0; i < num; i++)
    { pvec->els[i] = obj; }
}
/*_    . Access */
/*_     , Get element */
static pko
basvec_get_element (kt_vector * pvec, int index)
{
  assert(index >= 0);
  assert(index <  pvec->len);
  return pvec->els[index];
}
/*_     , Fill array */
static void
basvec_fill_array(kt_vector * pvec, int max_len, pko * array)
{
  int i;
  const int num = pvec->len;
  
  assert (num <= max_len);
  for (i = 0; i < num; i++)
    {
      array [i] = pvec->els [i];
    }
  return;
}
/*_    . Mutate */
static void
basvec_set_element (kt_vector * pvec, int index, pko obj)
{
  assert(index >= 0);
  assert(index <  pvec->len);
  pvec->els[index] = obj;
}

/*_    . Treat as boxed */
/* Functions following here assume that kt_vector is in a box by itself. */
/*_     , alloc_basvector */
static pko
alloc_basvector (int len, _kt_tag t_enum)
{
  assert_T_is_basvector(t_enum);
  ALLOC_BOX_PRESUME(kt_vector, t_enum);
  basvec_init_rough(&pbox->data, len);
  return PTR2PKO(pbox);
}
/*_     , mk_basvector_w_args */
static pko
mk_basvector_w_args(klink * sc, pko args, _kt_tag t_enum)
{
  assert_T_is_basvector(t_enum);
  int4 metrics;
  get_list_metrics_aux(args, metrics);
  if (metrics[lm_num_nils] != 1)
    {
      KERNEL_ERROR_1 (sc, "mk_basvector_w_args: not a proper list:", args);
    }
  int len = metrics[lm_acyc_len];
  pko vec = alloc_basvector(len, t_enum);
  WITH_UNBOXED_UNSAFE(pdata,kt_vector,vec);
  basvec_init_by_list (pdata, args);
  return vec;
}
/*_     , mk_filled_basvector */
pko
mk_filled_basvector(int len, pko fill, _kt_tag t_enum)
{
  assert_T_is_basvector(t_enum);
  pko vec = alloc_basvector(len, t_enum);
  unsafe_basvector_fill (vec, fill);
  return vec;
}  
/*_     , mk_basvector_from_array */
pko
mk_basvector_from_array(int len, pko * array, _kt_tag t_enum)
{
  assert_T_is_basvector(t_enum);
  pko vec = alloc_basvector(len, t_enum);
  WITH_UNBOXED_UNSAFE(pdata,kt_vector,vec);
  basvec_init_by_array (pdata, array);
  return vec;
}
/*_     , mk_foresliced_basvector */
pko
mk_foresliced_basvector (pko vec, int excess, _kt_tag t_enum)
{
  WITH_UNBOXED_UNSAFE(pdata,kt_vector,vec);
  const int len = pdata->len;
  assert (len >= excess);
  const int remnant_len = len - excess;
  return mk_basvector_from_array (remnant_len,
				  pdata->els + excess,
				  t_enum);
}
/*_    . Unsafe operations (Typechecks can be disabled) */
/*_     , unsafe_basvector_fill */
static void
unsafe_basvector_fill (pko vec, pko obj)
{
  assert_T_is_basvector(_get_type(vec));
  assert_mutable(0,vec);
  WITH_UNBOXED_UNSAFE(pdata,kt_vector,vec);
  basvec_init_by_single (pdata, obj);
}
/*_     , basvector_len */
static int
basvector_len (pko vec)
{
  assert_T_is_basvector(_get_type(vec));
  WITH_UNBOXED_UNSAFE(pdata,kt_vector,vec);
  return pdata->len;
}

/*_     , basvector_elem */
static pko
basvector_elem (pko vec, int ielem)
{
  assert_T_is_basvector(_get_type(vec));
  WITH_UNBOXED_UNSAFE(pdata,kt_vector,vec);
  return basvec_get_element (pdata, ielem);
}

/*_     , basvector_set_elem */
static void
basvector_set_elem (pko vec, int ielem, pko a)
{
  assert_T_is_basvector(_get_type(vec));
  assert_mutable(0,vec);
  WITH_UNBOXED_UNSAFE(pdata,kt_vector,vec);
  basvec_set_element (pdata, ielem, a);
  return;
}
/*_     , basvector_fill_array */
static void
basvector_fill_array(pko vec, int max_len, pko * array)
{
  assert_T_is_basvector(_get_type(vec));
  WITH_UNBOXED_UNSAFE (p_vec, kt_vector, vec);
  basvec_fill_array (p_vec, max_len, array);
  return;
}
/*_    . Checked operations */
/*_   , Basic strings (Low strings) */
/*_    . Struct kt_string */

typedef struct
{
  char *_svalue;
  int _length;
} kt_string;

/*_    . Get parts */
INLINE char *
bastring_value (sc_or_null sc, _kt_tag t_enum, pko p)
{
  WITH_PSYC_UNBOXED(kt_string,p, t_enum, sc);
  return pdata->_svalue;
}

INLINE int
bastring_len (sc_or_null sc, _kt_tag t_enum, pko p)
{
  WITH_PSYC_UNBOXED(kt_string,p, t_enum, sc);
  return pdata->_length;
}

/*_    . Create */

static char *
store_string (int len_str, const char *str, char fill)
{
  char *q;

  q = (char *) GC_MALLOC_ATOMIC (len_str + 1);
  if (str != 0)
    {
      snprintf (q, len_str + 1, "%s", str);
    }
  else
    {
      memset (q, fill, len_str);
      q[len_str] = 0;
    }
  return (q);
}

INLINE pko
mk_bastring (_kt_tag t_enum, const char *str, int len, char fill)
{
  ALLOC_BOX_PRESUME (kt_string, t_enum);
  pbox->data._svalue = store_string(len, str, fill);
  pbox->data._length = len;
  return PTR2PKO(pbox);
}

/*_    . Type assert */
void assert_T_is_bastring(_kt_tag t_enum)
{
  t_enum &= T_MASKTYPE;
  assert(
	 t_enum == T_STRING ||
	 t_enum == T_SYMBOL);
}  

/*_  . Individual object types */
/*_   , Booleans */

BOX_OF_VOID (K_T);
BOX_OF_VOID (K_F);

DEF_SIMPLE_PRED(is_bool,T_NO_K,ground, "boolean?/o1")
{
  WITH_1_ARGS(p);
  return (p == K_T) || (p == K_F);
}
/*_    . Operations */
SIG_CHKARRAY(not) = { REF_OPER(is_bool), };
DEF_SIMPLE_APPLICATIVE(p00a1,not,T_NO_K,ground, "not?")
{
  WITH_1_ARGS(p);
  if(p == K_T) { return K_F; }
  if(p == K_F) { return K_T; }
  errx(6, "not: Argument must be boolean");
}

/*_   , Numbers */
/*_    . Number constants */
#if 0
/* We would use these for "folding" operations like cumulative addition. */
static num num_zero = { 1, {0}, };
static num num_one  = { 1, {1}, };
#endif
/*_    . Macros */
#define num_ivalue(n)       (n.is_fixnum?(n).value.ivalue:(long)(n).value.rvalue)
#define num_rvalue(n)       (!n.is_fixnum?(n).value.rvalue:(double)(n).value.ivalue)

/*_    . Making them */

INTERFACE pko
mk_integer (long num)
{
  ALLOC_BOX_PRESUME (struct num, T_NUMBER);
  pbox->data.value.ivalue = num;
  pbox->data.is_fixnum = 1;
  return PTR2PKO(pbox);
}

INTERFACE pko
mk_real (double n)
{
  ALLOC_BOX_PRESUME (num, T_NUMBER);
  pbox->data.value.rvalue = n;
  pbox->data.is_fixnum = 0;
  return PTR2PKO(pbox);
}

static pko
mk_number (num n)
{
  if (n.is_fixnum)
    {
      return mk_integer (n.value.ivalue);
    }
  else
    {
      return mk_real (n.value.rvalue);
    }
}

/*_    . Checking them */
static int is_zero_double (double x);

static INLINE int
num_is_integer (pko p)
{
  WITH_PSYC_UNBOXED(num,p,T_NUMBER,0);
  return (pdata->is_fixnum);
}

DEF_T_PRED (is_number,T_NUMBER,ground,"number?/o1");

DEF_SIMPLE_PRED (is_posint,T_NO_K,ground,"posint?/o1")
{
  WITH_1_ARGS(p);
  return is_integer (p) && ivalue (p) >= 0;
}

/* $$IMPROVE ME later Integer and real should be separate T_ types. */
DEF_SIMPLE_PRED (is_integer,T_NO_K,ground, "integer?/o1")
{
  WITH_1_ARGS(p);
  if(!is_number (p)) { return 0; }
  WITH_UNBOXED_UNSAFE(pdata,num,p);
  return (pdata->is_fixnum);
}

DEF_SIMPLE_PRED (is_real,T_NO_K,ground, "real?/o1")
{
  WITH_1_ARGS(p);
  if(!is_number (p)) { return 0; }
  WITH_UNBOXED_UNSAFE(pdata,num,p);
  return (!pdata->is_fixnum);
}
DEF_SIMPLE_PRED (is_zero,T_NO_K,ground, "zero?/o1")
{
  WITH_1_ARGS(p);
  /* Behavior on non-numbers wasn't specified so I'm assuming the
     predicate just fails. */
  if(!is_number (p)) { return 0; }
  WITH_UNBOXED_UNSAFE(pdata,num,p);
  if(pdata->is_fixnum)
    {
      return (ivalue (p) == 0);
    }
  else
    {
      return is_zero_double(rvalue(p));
    }
}
/* $$WRITE ME positive? negative? odd? even? */
/*_    . Getting their values */
INLINE num
nvalue (pko p)
{
  WITH_PSYC_UNBOXED(num,p,T_NUMBER,0);
  return ((*pdata));
}

INTERFACE long
ivalue (pko p)
{
  WITH_PSYC_UNBOXED(num,p,T_NUMBER,0);
  return (num_is_integer (p) ? pdata->value.ivalue : (long) pdata->
	  value.rvalue);
}

INTERFACE double
rvalue (pko p)
{
  WITH_PSYC_UNBOXED(num,p,T_NUMBER,0);
  return (!num_is_integer (p)
	  ? pdata->value.rvalue : (double) pdata->value.ivalue);
}

INTERFACE void
set_ivalue (pko p, long i)
{
  assert_mutable(0,p);
  WITH_PSYC_UNBOXED(num,p,T_NUMBER,0);
  assert (num_is_integer (p));
  pdata->value.ivalue = i;
  return;
}

INTERFACE void
add_to_ivalue (pko p, long i)
{
  assert_mutable(0,p);
  WITH_PSYC_UNBOXED(num,p,T_NUMBER,0);
  assert (num_is_integer (p));
  pdata->value.ivalue += i;
  return;
}

/*_    . Operating on numbers */
static num
num_add (num a, num b)
{
  num ret;
  ret.is_fixnum = a.is_fixnum && b.is_fixnum;
  if (ret.is_fixnum)
    {
      ret.value.ivalue = a.value.ivalue + b.value.ivalue;
    }
  else
    {
      ret.value.rvalue = num_rvalue (a) + num_rvalue (b);
    }
  return ret;
}

static num
num_mul (num a, num b)
{
  num ret;
  ret.is_fixnum = a.is_fixnum && b.is_fixnum;
  if (ret.is_fixnum)
    {
      ret.value.ivalue = a.value.ivalue * b.value.ivalue;
    }
  else
    {
      ret.value.rvalue = num_rvalue (a) * num_rvalue (b);
    }
  return ret;
}

static num
num_div (num a, num b)
{
  num ret;
  ret.is_fixnum = a.is_fixnum && b.is_fixnum
    && a.value.ivalue % b.value.ivalue == 0;
  if (ret.is_fixnum)
    {
      ret.value.ivalue = a.value.ivalue / b.value.ivalue;
    }
  else
    {
      ret.value.rvalue = num_rvalue (a) / num_rvalue (b);
    }
  return ret;
}

static num
num_intdiv (num a, num b)
{
  num ret;
  ret.is_fixnum = a.is_fixnum && b.is_fixnum;
  if (ret.is_fixnum)
    {
      ret.value.ivalue = a.value.ivalue / b.value.ivalue;
    }
  else
    {
      ret.value.rvalue = num_rvalue (a) / num_rvalue (b);
    }
  return ret;
}

static num
num_sub (num a, num b)
{
  num ret;
  ret.is_fixnum = a.is_fixnum && b.is_fixnum;
  if (ret.is_fixnum)
    {
      ret.value.ivalue = a.value.ivalue - b.value.ivalue;
    }
  else
    {
      ret.value.rvalue = num_rvalue (a) - num_rvalue (b);
    }
  return ret;
}

static num
num_rem (num a, num b)
{
  num ret;
  long e1, e2, res;
  ret.is_fixnum = a.is_fixnum && b.is_fixnum;
  e1 = num_ivalue (a);
  e2 = num_ivalue (b);
  res = e1 % e2;
  /* modulo should have same sign as second operand */
  if (res > 0)
    {
      if (e1 < 0)
	{
	  res -= labs (e2);
	}
    }
  else if (res < 0)
    {
      if (e1 > 0)
	{
	  res += labs (e2);
	}
    }
  ret.value.ivalue = res;
  return ret;
}

static num
num_mod (num a, num b)
{
  num ret;
  long e1, e2, res;
  ret.is_fixnum = a.is_fixnum && b.is_fixnum;
  e1 = num_ivalue (a);
  e2 = num_ivalue (b);
  res = e1 % e2;
  if (res * e2 < 0)
    {				/* modulo should have same sign as second operand */
      e2 = labs (e2);
      if (res > 0)
	{
	  res -= e2;
	}
      else
	{
	  res += e2;
	}
    }
  ret.value.ivalue = res;
  return ret;
}

static int
num_eq (num a, num b)
{
  int ret;
  int is_fixnum = a.is_fixnum && b.is_fixnum;
  if (is_fixnum)
    {
      ret = a.value.ivalue == b.value.ivalue;
    }
  else
    {
      ret = num_rvalue (a) == num_rvalue (b);
    }
  return ret;
}


static int
num_gt (num a, num b)
{
  int ret;
  int is_fixnum = a.is_fixnum && b.is_fixnum;
  if (is_fixnum)
    {
      ret = a.value.ivalue > b.value.ivalue;
    }
  else
    {
      ret = num_rvalue (a) > num_rvalue (b);
    }
  return ret;
}

static int
num_ge (num a, num b)
{
  return !num_lt (a, b);
}

static int
num_lt (num a, num b)
{
  int ret;
  int is_fixnum = a.is_fixnum && b.is_fixnum;
  if (is_fixnum)
    {
      ret = a.value.ivalue < b.value.ivalue;
    }
  else
    {
      ret = num_rvalue (a) < num_rvalue (b);
    }
  return ret;
}

static int
num_le (num a, num b)
{
  return !num_gt (a, b);
}

#if USE_MATH
/* Round to nearest. Round to even if midway */
static double
round_per_R5RS (double x)
{
  double fl = floor (x);
  double ce = ceil (x);
  double dfl = x - fl;
  double dce = ce - x;
  if (dfl > dce)
    {
      return ce;
    }
  else if (dfl < dce)
    {
      return fl;
    }
  else
    {
      if (fmod (fl, 2.0) == 0.0)
	{			/* I imagine this holds */
	  return fl;
	}
      else
	{
	  return ce;
	}
    }
}
#endif

static int
is_zero_double (double x)
{
  return x < DBL_MIN && x > -DBL_MIN;
}

static long
binary_decode (const char *s)
{
  long x = 0;

  while (*s != 0 && (*s == '1' || *s == '0'))
    {
      x <<= 1;
      x += *s - '0';
      s++;
    }

  return x;
}
/*_     , Macros */
/* "Psychically" defines a and b.  */
#define WITH_PSYC_AB_ARGS(A_TYPE,B_TYPE)	  \
  WITH_UNBOXED_UNSAFE(a,A_TYPE,arg1); \
  WITH_UNBOXED_UNSAFE(b,B_TYPE,arg2)


/*_     , Interface */
/*_      . Binary operations */
SIG_CHKARRAY(num_binop) = { REF_OPER(is_number), REF_OPER(is_number), };
DEF_SIMPLE_DESTR(num_binop);

DEF_APPLICATIVE_W_DESTR(ps0a2,k_add,REF_DESTR(num_binop),0,ground, "add")
{
  WITH_PSYC_AB_ARGS(num,num);
  ALLOC_BOX_PRESUME(num,T_NUMBER);
  *pdata = num_add (*a, *b);
  return PTR2PKO(pbox);
}

DEF_APPLICATIVE_W_DESTR(ps0a2,k_sub,REF_DESTR(num_binop),0,ground, "sub")
{
  WITH_PSYC_AB_ARGS(num,num);
  ALLOC_BOX_PRESUME(num,T_NUMBER);
  *pdata = num_sub (*a, *b);
  return PTR2PKO(pbox);
}

DEF_APPLICATIVE_W_DESTR(ps0a2,k_mul,REF_DESTR(num_binop),0,ground, "mul")
{
  WITH_PSYC_AB_ARGS(num,num);
  ALLOC_BOX_PRESUME(num,T_NUMBER);
  *pdata = num_mul (*a, *b);
  return PTR2PKO(pbox);
}

DEF_APPLICATIVE_W_DESTR(ps0a2,k_div,REF_DESTR(num_binop),0,ground, "div")
{
  WITH_PSYC_AB_ARGS(num,num);
  ALLOC_BOX_PRESUME(num,T_NUMBER);
  *pdata = num_div (*a, *b);
  return PTR2PKO(pbox);
}

DEF_APPLICATIVE_W_DESTR(ps0a2,k_mod,REF_DESTR(num_binop),0,ground, "mod")
{
  WITH_PSYC_AB_ARGS(num,num);
  ALLOC_BOX_PRESUME(num,T_NUMBER);
  *pdata = num_mod (*a, *b);
  return PTR2PKO(pbox);
}
/*_      . Binary predicates */
DEF_APPLICATIVE_W_DESTR(bs0a2,k_gt,REF_DESTR(num_binop),0,ground, ">?/2")
{
  WITH_PSYC_AB_ARGS(num,num);
  ALLOC_BOX_PRESUME(num,T_NUMBER);
  return num_gt (*a, *b);
}

DEF_APPLICATIVE_W_DESTR(bs0a2,k_eq,REF_DESTR(num_binop),0,simple, "equal?/2-num-num")
{
  WITH_PSYC_AB_ARGS(num,num);
  ALLOC_BOX_PRESUME(num,T_NUMBER);
  return num_eq (*a, *b);
}


/*_   , Characters */
DEF_T_PRED (is_character,T_CHARACTER,ground, "character?/o1");

INTERFACE long
charvalue (pko p)
{
  WITH_PSYC_UNBOXED(long,p,T_CHARACTER,0);
  return *pdata;
}

INTERFACE pko
mk_character (int c)
{
  ALLOC_BOX_PRESUME (long, T_CHARACTER);
  pbox->data = c;
  return PTR2PKO(pbox);
}

/*_    . Classifying characters */
#if USE_CHAR_CLASSIFIERS
static INLINE int
Cisalpha (int c)
{
  return isascii (c) && isalpha (c);
}

static INLINE int
Cisdigit (int c)
{
  return isascii (c) && isdigit (c);
}

static INLINE int
Cisspace (int c)
{
  return isascii (c) && isspace (c);
}

static INLINE int
Cisupper (int c)
{
  return isascii (c) && isupper (c);
}

static INLINE int
Cislower (int c)
{
  return isascii (c) && islower (c);
}
#endif
/*_    . Character names */
#if USE_ASCII_NAMES
static const char *charnames[32] = {
  "nul",
  "soh",
  "stx",
  "etx",
  "eot",
  "enq",
  "ack",
  "bel",
  "bs",
  "ht",
  "lf",
  "vt",
  "ff",
  "cr",
  "so",
  "si",
  "dle",
  "dc1",
  "dc2",
  "dc3",
  "dc4",
  "nak",
  "syn",
  "etb",
  "can",
  "em",
  "sub",
  "esc",
  "fs",
  "gs",
  "rs",
  "us"
};

static int
is_ascii_name (const char *name, int *pc)
{
  int i;
  for (i = 0; i < 32; i++)
    {
      if (stricmp (name, charnames[i]) == 0)
	{
	  *pc = i;
	  return 1;
	}
    }
  if (stricmp (name, "del") == 0)
    {
      *pc = 127;
      return 1;
    }
  return 0;
}

#endif

/*_   , Void objects */
/*_    . is_key */
DEF_T_PRED (is_key, T_KEY,no,"");


/*_    . Others */
BOX_OF_VOID (K_NIL);
BOX_OF_VOID (K_EOF);
BOX_OF_VOID (K_INERT);
BOX_OF_VOID (K_IGNORE);
/*_    . "Secret" objects for built-in keyed dynamic bindings */
BOX_OF_VOID (K_PRINT_FLAG);
BOX_OF_VOID (K_TRACING);
BOX_OF_VOID (K_INPORT);
BOX_OF_VOID (K_OUTPORT);
BOX_OF_VOID (K_NEST_DEPTH);
/*_    . Keys for typecheck */
BOX_OF_VOID (K_TYCH_DOT);
BOX_OF_VOID (K_TYCH_REPEAT);
BOX_OF_VOID (K_TYCH_OPTIONAL);
BOX_OF_VOID (K_TYCH_IMP_REPEAT);
BOX_OF_VOID (K_TYCH_NO_TYPE);

/*_    . Making them dynamically */
DEF_CFUNC(p00a0, mk_void, K_NO_TYPE,T_NO_K)
{
  ALLOC_BOX(pbox,T_KEY,kt_boxed_void);
  return PTR2PKO(pbox);
}
/*_    . Type */
DEF_SIMPLE_PRED(is_null,T_NO_K,ground, "null?/o1")
{
  WITH_1_ARGS(p);
  return p == K_NIL;
}
DEF_SIMPLE_PRED(is_inert,T_NO_K,ground, "inert?/o1")
{
  WITH_1_ARGS(p);
  return p == K_INERT;
}
DEF_SIMPLE_PRED(is_ignore,T_NO_K,ground, "ignore?/o1")
{
  WITH_1_ARGS(p);
  return p == K_IGNORE;
}


/*_   , Typecheck & destructure objects */
/*_    . Structures */
/* _car is vector component, _cdr is list component. */
typedef kt_vec2 kt_destr_result;
/*_    . Enumeration */
typedef enum
{
  destr_success,
  destr_err,
  destr_must_call_k,
} kt_destr_outcome;
/*_    . Checks */
DEF_T_PRED (is_destr_result, T_DESTR_RESULT, no, "");
/*_    . Building them */
/*_     , can_be_trivpred */
/* Return true if the object can be used as a trivial predicate: An
   xary operative that does not call Kernel and returns a boolean as
   an int. */
DEF_SIMPLE_PRED(can_be_trivpred,T_NO_K,unsafe,"trivpred?/o1")
{
  WITH_1_ARGS(p);
  if(!no_call_k(p)) { return 0; }
  switch(_get_type(p))
    {
    case T_CFUNC:
      {
	WITH_UNBOXED_UNSAFE(pdata,kt_cfunc,p);
	switch(pdata->type)
	  {
	  case klink_ftype_b00a1:
	    { return 1; }
	  default:
	    { return 0; }
	  }
      }
      /* NOTREACHED */
      
    case T_DESTRUCTURE:
      { return 1; }
      /* NOTREACHED */
      
    case T_TYPECHECK:
      { return 1; }
      /* NOTREACHED */
    case T_TYPEP:
      { return 1; }
      /* NOTREACHED */
    default: return 0; 
    }
}

/*_     , k_to_trivpred */
/* Convert a unary or nary function to xary.  If not possible, return
   nil. */
/* $$OBSOLESCENT  Only used in print lookup, which will change */
pko
k_to_trivpred(pko p)
{
  if(is_applicative(p))
    { p = unwrap_all(p); }

  if(can_be_trivpred(p))
    { return p; }  
  return K_NIL;
}

/*_     , type-keys environment */
RGSTR(type-keys, "cyclic-repeat",   REF_KEY(K_TYCH_IMP_REPEAT) )
RGSTR(type-keys, "optional",        REF_KEY(K_TYCH_OPTIONAL)   )
RGSTR(type-keys, "repeat",          REF_KEY(K_TYCH_REPEAT)     )
RGSTR(type-keys, "dot",             REF_KEY(K_TYCH_DOT)        )
/*_     , any_k */
int any_k (kt_vector * p_vec_guts)
{
  int i;
  for (i = 0; i < p_vec_guts->len; i++)
    {
      pko obj = p_vec_guts->els [i];
      WITH_BOX_TYPE(tag,obj);
      if (*tag | ~(T_NO_K)) { return 1; }
    }
  return 0;
}

/*_     , Typecheck */
DEF_APPLICATIVE_W_DESTR (ps0a1, mk_typecheck, REF_OPER(is_finite_list),T_NO_K,unsafe, "listtype/N-trivpred")
{
  pko vec = mk_basvector_w_args(sc, arg1, T_TYPECHECK | T_IMMUTABLE | T_NO_K);
#if 0		 /* $$ENABLE ME later */
  /* If everything is T_NO_K, then give flag T_NO_K. */
  WITH_UNBOXED_UNSAFE (pdata, kt_vector, vec);
  if (!any_k (pdata))
    {
      WITH_BOX_TYPE(tag,vec);
      *tag |= T_NO_K;
    }
#endif  
  return vec;
}
/*_     , Destructurer */
DEF_APPLICATIVE_W_DESTR (ps0a1, mk_destructurer, REF_OPER(is_finite_list),T_NO_K,unsafe, "destructure-list/N-trivpred")
{
  /* $$IMPROVE MY SUPPORT A destructurer should fill up this */
  int4 metrics;
  get_list_metrics_aux(arg1, metrics);
  if (metrics[lm_num_nils] != 1)
    {
      KERNEL_ERROR_1 (sc, "mk_destructurer: not a proper list:", arg1);
    }
  int len = metrics[lm_acyc_len];
  ALLOC_BOX_PRESUME(kt_destr_list, T_DESTRUCTURE | T_IMMUTABLE | T_NO_K);
  basvec_init_rough (&pdata->cvec, len);
  basvec_init_by_list (&pdata->cvec, arg1);
  pdata->num_targets = -1;

#if 0		 /* $$ENABLE ME later when typemiss check is OK for this */
  /* If everything is T_NO_K, then give flag T_NO_K. */
  if (!any_k (&pdata->cvec))
    {
      WITH_BOX_TYPE(tag,vec);
      *tag |= T_NO_K;
    }
#endif  
  return PTR2PKO(pbox);
}
/*_     , Destructurer Result state */
/* Really a mixed vector/list */
/*_      . mk_destr_result */
pko
mk_destr_result
(int len, pko * array, pko more_vals)
{
  pko vec = mk_basvector_from_array(len, array, T_VECTOR);
  return v2cons (T_DESTR_RESULT, vec, more_vals);
}
/*_      . mk_destr_result_add */
pko
mk_destr_result_add
(pko old, int len, pko * array)
{
  pko val_list = unsafe_v2cdr (old);
  int i;
  for (i = 0; i < len; i++)
    {
      val_list = cons ( array [i], val_list);
    }
  return v2cons (T_DESTR_RESULT,
		 unsafe_v2car (old),
		 val_list);
}
/*_      . destr_result_fill_array */
void
destr_result_fill_array (pko dr, int max_len, pko * array)
{
  /* Assume errors are due to C code. */
  WITH_REPORTER (0);
  WITH_PSYC_UNBOXED (kt_destr_result, dr, T_DESTR_RESULT, 0)
  int vec_len =
    basvector_len (pdata->_car);
  basvector_fill_array(pdata->_car, vec_len, array);
  /* We get args earliest lowest, so insert them in reverse order. */
  int list_len = list_length (pdata->_cdr);
  int i = vec_len + list_len - 1;
  assert (i < max_len);
  pko args;
  for (args = pdata->_cdr; args != K_NIL; args = cdr (args), i--)
    {
      array [i] = car (args);
    }  
}

/*_     , destr_result_to_vec */
SIG_CHKARRAY (destr_result_to_vec) =
{
  REF_OPER (is_destr_result),
};

DEF_SIMPLE_CFUNC (p00a1, destr_result_to_vec, T_NO_K)
{
  WITH_1_ARGS (destr_result);
  WITH_UNBOXED_UNSAFE (p_destr_result, kt_destr_result, destr_result);
  int len =
    basvector_len (p_destr_result->_car) +
    list_length (p_destr_result->_cdr);
  pko vec = mk_vector (len, K_NIL);
  WITH_UNBOXED_UNSAFE (p_vec, kt_destr_list, vec);
  destr_result_fill_array (destr_result, len, p_vec->cvec.els);
  return vec;
}

/*_    . Particular typechecks */
/*_     , Any singleton */
pko _K_ARRAY_any_singleton[] = { K_ANY, };
DEF_DESTR(_K_any_singleton,_K_ARRAY_any_singleton);
/*_     , Typespec itself */
#define K_TY_TYPESPEC K_ANY
/*_     , Destructure spec itself */
#define K_TY_DESTRSPEC K_ANY
/*_     , Top type (Always succeeds) */
RGSTR(ground, "true/o1", REF_OPER(is_any))
DEF_CFUNC(b00a1,is_any,K_ANY_SINGLETON,T_NO_K)
{ return 1; }
/*_     , true? */
/* Not entirely redundant; Used internally to check scheduled returns. */
DEF_CFUNC(b00a1,is_true,K_ANY_SINGLETON,T_NO_K)
{
  WITH_1_ARGS (p);
  return p == K_T;
}

/*_    . Internal signatures */
static int
typecheck_repeat
(klink *sc, pko argobject, pko * ar_typespec, int count, int style);
static pko
where_typemiss_repeat
(klink *sc, pko argobject, pko * ar_typespec, int count, int style);
pko
static where_typemiss_do_spec
(klink * sc, pko argobject, pko * ar_typespec, int left);
int
typecheck_by_vec (klink * sc, pko argobject, pko * ar_typespec, int left);

/*_    . Typecheck operations */
inline int
call_T_typecheck(pko T, pko obj)
{
  WITH_PSYC_UNBOXED(typep_t,T,T_TYPEP,0);
  return is_type(obj,pdata->T_tag);
}
/*_     , typecheck */
/* This is an optimization under-the-hood for running
   possibly-compound predicates.  Ultimately it will not be exposed.
   Later it may have a Kernel "safe counterpart" that is optimized to
   it when possible.

   It should not call anything that calls Kernel.  All its
   "components" should be trivpreds (xary operatives that don't use
   eval loop), satisfying can_be_trivpred, generally specified
   natively in C.  */  
/* We don't have a typecheck typecheck predicate yet, so accept
   anything for arg2. */
SIG_CHKARRAY(typecheck) = { K_ANY, K_ANY, };
DEF_SIMPLE_APPLICATIVE (bs0a2, typecheck,T_NO_K,unsafe,"type?")
{
  WITH_2_ARGS(argobject,typespec);
  assert(no_call_k(typespec));
  switch(_get_type(typespec))
    {
    case T_CFUNC:
      {
	WITH_UNBOXED_UNSAFE(pdata,kt_cfunc,typespec);
	switch(pdata->type)
	  {
	  case klink_ftype_b00a1:
	    {
	      return pdata->func.f_b00a1(argobject);
	    }
	  default:
	    errx(7, "typecheck: Object is not a typespec");
	  }
      }
      break; /* NOTREACHED */
    case T_TYPEP:
      return call_T_typecheck(typespec, argobject);
    case T_DESTRUCTURE:		/* Fallthru */
      {
	WITH_UNBOXED_UNSAFE(pdata,kt_destr_list,typespec);
	pko * ar_typespec = pdata->cvec.els;
	int left = pdata->cvec.len;
	return typecheck_by_vec (sc, argobject, ar_typespec, left);
      }
    case T_TYPECHECK:
      {
	WITH_UNBOXED_UNSAFE(pdata,kt_vector,typespec);
	pko * ar_typespec = pdata->els;
	int left = pdata->len;
	return typecheck_by_vec (sc, argobject, ar_typespec, left);
      }

    default:
      errx(7, "typecheck: Object is not a typespec");
    }
  return 0; /* NOTREACHED */
}
/*_     , typecheck_by_vec */
int
typecheck_by_vec (klink * sc, pko argobject, pko * ar_typespec, int left)
{
  int saw_optional = 0;
  for( ; left; ar_typespec++, left--)
    {
      pko tych = *ar_typespec;
      /**** Check for special keys ****/
      if(tych == REF_KEY(K_TYCH_DOT))
	{
	  if(left != 2)
	    {
	      KERNEL_ERROR_0 (sc, "typecheck: After dot there must "
			      "be exactly one typespec");
	    }
	  else
	    { return typecheck(sc, argobject, ar_typespec[1]); }
	}
      if(tych == REF_KEY(K_TYCH_OPTIONAL))
	{
	  if(saw_optional)
	    {
	      KERNEL_ERROR_0 (sc, "typecheck: Can't have two optionals");
	    }
	  else
	    {
	      saw_optional = 1;
	      continue;
	    }
	}
      if(tych == REF_KEY(K_TYCH_REPEAT))
	{
	  return
	    typecheck_repeat(sc,argobject,
			     ar_typespec + 1,
			     left - 1,
			     0);
	}
      if(tych == REF_KEY(K_TYCH_IMP_REPEAT))
	{
	  return
	    typecheck_repeat(sc,argobject,
			     ar_typespec + 1,
			     left - 1,
			     1);
	}

      /*** Manage stepping ***/
      if(!is_pair(argobject))
	{
	  if(!saw_optional)
	    { return 0; }
	  else
	    { return 1; }
	}
      else
	{
	  /* Advance */
	  pko c = pair_car(0,argobject);
	  argobject = pair_cdr(0,argobject);

	  /*** Do the check ***/
	  if (!typecheck(sc, c, tych)) { return 0; }
	}
    }
  if(argobject != K_NIL)
    { return 0; }
  return 1;  
}

/*_     , typecheck_repeat */
static int
typecheck_repeat
(klink *sc, pko argobject, pko * ar_typespec, int count, int style)
{
  int4 metrics;
  get_list_metrics_aux(argobject, metrics);
  /* Dotted lists don't satisfy repeat */
  if(!metrics[lm_num_nils]) { return 0; }
  if(metrics[lm_cyc_len])
    {
      /* STYLE may not allow cycles. */
      if(!style)
	{ return 0; }
      /* If there's a cycle and count doesn't fit into it exactly,
	 call that a mismatch. */
      if(count % metrics[lm_cyc_len])
	{ return 0; }
    }
  /* Check the car of each pair. */
  int step;
  int i;
  for(step = 0, i = 0;
      step < metrics[lm_num_pairs];
      ++step,   ++i,   argobject = pair_cdr(0,argobject))
    {
      if(i == count) { i = 0; }
      assert(is_pair(argobject));
      pko tych = ar_typespec[i];
      pko c = pair_car(0,argobject);
      if (!typecheck(sc, c, tych)) { return 0; }
    }
  return 1;
}
/*_     , where_typemiss */
/* This parallels typecheck, but where typecheck returned a boolean,
   this returns an object indicating where the type failed to match. */
SIG_CHKARRAY(where_typemiss) = { K_ANY, K_ANY, };
DEF_SIMPLE_APPLICATIVE (ps0a2, where_typemiss,T_NO_K,unsafe, "where-typemiss")
{
  /* Return a list indicating how TYPESPEC failed to match
     ARGOBJECT */
  WITH_2_ARGS(argobject,typespec);
  assert(no_call_k(typespec));
  switch(_get_type(typespec))
    {
    case T_CFUNC:
      {
	WITH_UNBOXED_UNSAFE(pdata,kt_cfunc,typespec);
	switch(pdata->type)
	  {
	  case klink_ftype_b00a1:
	    {
	      if (pdata->func.f_b00a1(argobject))
		{
		  return 0;
		}
	      else
		{ return LIST1(typespec); }
	    }
	  default:
	    errx(7, "where_typemiss: Object is not a typespec");
	    return 0;
	  }
      }
      break; /* NOTREACHED */
    case T_TYPEP:
      {
	WITH_PSYC_UNBOXED(typep_t,typespec,T_TYPEP,0);
	if (call_T_typecheck(typespec, argobject))
	  { return 0; }
	else
	  { return LIST1(mk_string(type_err_string(pdata->T_tag))); }
      }

    case T_TYPECHECK:
      {
	WITH_UNBOXED_UNSAFE(pdata,kt_destr_list,typespec);
	return where_typemiss_do_spec(sc, argobject, pdata->cvec.els, pdata->cvec.len);
      }
    case T_DESTRUCTURE:
      {
	WITH_UNBOXED_UNSAFE(pdata,kt_vector,typespec);
	return where_typemiss_do_spec(sc, argobject, pdata->els, pdata->len);
      }

    default:
      errx(7,"where_typemiss: Object is not a typespec");
      return 0;
    }
  return 0; /* NOTREACHED */
}
/*_     , where_typemiss_do_spec */
pko
where_typemiss_do_spec
(klink * sc, pko argobject, pko * ar_typespec, int left)
{
  int saw_optional = 0;
  int el_num = 0;
  for( ; left; ar_typespec++, left--)
    {
      pko tych = *ar_typespec;
      /**** Check for special keys ****/
      if(tych == REF_KEY(K_TYCH_DOT))
	{
	  if(left != 2)
	    {
	      KERNEL_ERROR_0 (sc, "where_typemiss: After dot there must "
			      "be exactly one typespec");
	    }
	  else
	    {
	      pko result =
		where_typemiss(sc, argobject, ar_typespec[1]);
	      if(result)
		{
		  return
		    LISTSTAR3(mk_integer(el_num),
			      mk_symbol("dot"),
			      result); 
		}
	      else
		{ return 0; }
	    }
	}
      if(tych == REF_KEY(K_TYCH_OPTIONAL))
	{
	  if(saw_optional)
	    {
	      KERNEL_ERROR_0 (sc, "where_typemiss: Can't have two optionals");
	    }
	  else
	    {
	      saw_optional = 1;
	      continue;
	    }
	}
      if(tych == REF_KEY(K_TYCH_REPEAT))
	{
	  pko result = 
	    where_typemiss_repeat(sc,argobject,
				  ar_typespec + 1,
				  left - 1,
				  0);
	  if(result)
	    { return LISTSTAR3(mk_integer(el_num),mk_symbol("repeat"), result); }
	  else
	    { return 0; }
	}
      if(tych == REF_KEY(K_TYCH_IMP_REPEAT))
	{
	  pko result = 
	    where_typemiss_repeat(sc,argobject,
				  ar_typespec + 1,
				  left - 1,
				  1);
	  if(result)
	    { return LISTSTAR3(mk_integer(el_num),mk_symbol("improper-repeat"),result); }
	  else
	    { return 0; }
	}

      /*** Manage stepping ***/
      if(!is_pair(argobject))
	{
	  if(!saw_optional)
	    {
	      return LIST2(mk_integer(el_num), mk_symbol("too-few"));
	    }
	  else
	    { return 0; }
	}
      else
	{
	  /* Advance */
	  pko c = pair_car(0,argobject);
	  argobject = pair_cdr(0,argobject);
	  el_num++;

	  /*** Do the check ***/
	  pko result = where_typemiss(sc, c, tych);
	  if (result)
	    { return LISTSTAR2(mk_integer(el_num),result); }
	}
    }
  if(argobject != K_NIL)
    { return LIST2(mk_integer(el_num), mk_symbol("too-many")); }
  return 0;
}

/*_     , where_typemiss_repeat */
static pko
where_typemiss_repeat
(klink *sc, pko argobject, pko * ar_typespec, int count, int style)
{
  int4 metrics;
  get_list_metrics_aux(argobject, metrics);
  /* Dotted lists don't satisfy repeat */
  if(!metrics[lm_num_nils]) { return LIST1(mk_symbol("dotted")); }
  if(metrics[lm_cyc_len])
    {
      /* STYLE may not allow cycles. */
      if(!style)
	{ return LIST1(mk_symbol("circular")); }
      /* If there's a cycle and count doesn't fit into it exactly,
	 call that a mismatch. */
      if(count % metrics[lm_cyc_len])
	{ return LIST1(mk_symbol("misaligned-end")); }
    }
  /* Check the car of each pair. */
  int step;
  int i;
  for(step = 0, i = 0;
      step < metrics[lm_num_pairs];
      ++step,   ++i,   argobject = pair_cdr(0,argobject))
    {
      if(i == count) { i = 0; }
      assert(is_pair(argobject));
      pko tych = ar_typespec[i];
      pko c = pair_car(0,argobject);
      pko result = where_typemiss(sc, c, tych);
      if (result)
	{ return LISTSTAR2(mk_integer(step),result); }      
    }
  return 0;
}

/*_    . Destructuring operations */
/*_     , destructure_by_bool */
/* Just for calling back after a freeform predicate */
SIG_CHKARRAY (destructure_by_bool) =
{
  REF_OPER (is_destr_result),
  K_ANY,
  REF_OPER (is_bool),
};
DEF_SIMPLE_CFUNC (ps0a3, destructure_by_bool, 0)
{
  WITH_3_ARGS (destr_result, argobject, satisfied);
  if (satisfied == K_T)
    {
      return
	mk_destr_result_add (destr_result, 1, &argobject);
    }
  else if (satisfied != K_F)
    {
      KERNEL_ERROR_0 (sc, "Predicate should return a boolean");
    }
  else
    {
      KERNEL_ERROR_0 (sc, "type mismatch on non-C predicate");
    }
}

/*_     , destructure_how_many */
int
destructure_how_many (pko typespec)
{
  switch (_get_type(typespec))
    {
    case T_DESTRUCTURE:
      {
	WITH_UNBOXED_UNSAFE(pdata,kt_destr_list,typespec);
	if (pdata->num_targets >= 0)
	  { return pdata->num_targets;}
	else
	  {
	    int count = 0;
	    pko * ar_typespec = pdata->cvec.els;
	    int left = pdata->cvec.len;
	    for( ; left; ar_typespec++, left--)
	      {
		pko tych = *ar_typespec;
		count += destructure_how_many (tych);
	      }
	    pdata->num_targets = count;
	    return count;
	  }
      }
    case T_KEY:
      return 0;
    default:
      return 1;
    }
}  
/*_     , destructure_make_ops */
pko
destructure_make_ops
(pko argobject, pko typespec, int saw_optional, pko provoker)
{
  return
    /* Operations to run, in reverse order. */
    LIST6(
	  /* ^V= result-so-far */
	  REF_OPER (destructure_resume),
	  /* V= (result-so-far argobject spec optional?) */
	  mk_load (LIST5 (mk_load_ix (1, 0),
			  mk_load_ix (0, 0),
			  typespec, 
			  kernel_bool (saw_optional),
			  provoker)),
	  mk_store (K_ANY, 1),
	  /* V= forced-argobject */
	  REF_OPER (force),
	  /* ^V= (argobject) */
	  mk_load (LIST1 (argobject)),
	  mk_store (K_ANY, 4)
	  /* ^V= result-so-far */
	  );
}
/*_     , destructure_make_ops_to_bool */
pko
destructure_make_ops_to_bool
(pko argobject, pko op_on_argobject)
{
  assert (is_combiner (op_on_argobject));
  return
    /* Operations to run, in reverse order. */
    LIST6(
	  /* ^V= result-so-far */
	  REF_OPER (destructure_by_bool),
	  /* V= (result-so-far bool spec) */
	  mk_load (LIST3 (mk_load_ix (1, 0),
			  argobject,
			  mk_load_ix (0, 0))),
	  mk_store (K_ANY, 1),
	  /* V= bool */
	  op_on_argobject,
	  /* ^V= (argobject) */
	  mk_load (LIST1 (argobject)),
	  mk_store (K_ANY, 4)
	  /* ^V= result-so-far */
	  );
}
/*_     , destructure */
/* Callers: past_end should point into the same array as *outarray.
   It will indicate the maximum number number of elements we may
   write.  The return value is the remainder of the outarray if
   successful, otherwise NULL.
   The meaning of extra_result depends on the return value:
   * On success, it's unused.
   * On destr_err, it will hold an error object.
   * On destr_must_call_k, it will hold a list of operations.
*/
kt_destr_outcome
destructure
(klink * sc, pko argobject, pko typespec, pko ** outarray,
 pko * past_end, pko * extra_result, int saw_optional, pko provoker)
{
  if(*outarray == past_end)
    {
      /* $$IMPROVE ME  Treat this error like other mismatches */
      KERNEL_ERROR_0 (sc, "destructure: past end of output array");
    }
  if(_get_type(typespec) == T_DESTRUCTURE)
    {
      WITH_UNBOXED_UNSAFE(pdata,kt_destr_list,typespec);
      pko * ar_typespec = pdata->cvec.els;
      int left = pdata->cvec.len;
      int el_num = 0;
      for( ; left; ar_typespec++, left--)
	{
	  pko tych = *ar_typespec;

	  /**** Check for special keys ****/
	  if(tych == REF_KEY(K_TYCH_DOT))
	    {
	      if(left != 2)
		{
		  KERNEL_ERROR_0 (sc, "destructure: After dot there must "
				  "be exactly one typespec");
		}
	      else
		{		
		  kt_destr_outcome outcome =
		    destructure (sc, argobject,
				 ar_typespec[1],
				 outarray,
				 past_end,
				 extra_result,
				 0,
				 provoker);
		  /* If there's error, contribute to describing its
		     location. */
		  if (outcome == destr_err)
		    {
		      *extra_result = 
			LISTSTAR3(mk_integer(el_num),
				  mk_symbol("dot"),
				  *extra_result);
		    }
		  return outcome;
		}
	    }
	  if(tych == REF_KEY(K_TYCH_OPTIONAL))
	    {
	      if(saw_optional)
		{
		  KERNEL_ERROR_0 (sc, "destructure: Can't have two optionals");
		}
	      else
		{
		  saw_optional = 1;
		  continue;
		}
	    }	    
	  /*** Manage stepping ***/
	  if(!is_pair(argobject))
	    {
	      if(saw_optional)
		{
		  *outarray[0] = K_INERT;
		  ++*outarray;
		}
	      else
		if (is_promise (argobject))
		  {
		    WITH_BOX_TYPE(tag,typespec);
		    pko new_typespec =
		      mk_foresliced_basvector (typespec,
					       pdata->cvec.len - left,
					       *tag);
		    *extra_result =
		      destructure_make_ops (argobject,
					    new_typespec,
					    saw_optional,
					    provoker);
		    return destr_must_call_k;
		  }
	      else
		{
		  /* $$IMPROVE ME These symbols should be made
		     only once. */
		  /* $$IMPROVE ME These location operations should be
		     encapped. */
		  *extra_result =
		    LIST2(mk_integer(el_num), mk_symbol("too-few"));
		  return destr_err;
		}
	    }
	  else
	    {
	      pko c = pair_car(0,argobject);
	      argobject = pair_cdr(0,argobject);
	      el_num++;
	      int outcome =
		destructure (sc,
			     c,
			     tych,
			     outarray,
			     past_end,
			     extra_result,
			     0,
			     provoker);
	      switch (outcome)
		{
		case destr_success:
		  /* Success keeps exploring */
		  break;
		case destr_err:
		  /* Simple error ends exploration */
		  /* Contribute to describing its location. */
		  *extra_result =
		    LISTSTAR2(mk_integer(el_num),*extra_result);
		  return destr_err;
		case destr_must_call_k:
		  /* must-call-K schedules to resume in this state,
		     then returns. */
		  {
		    WITH_BOX_TYPE(tag,typespec);
		    /* $$IMPROVE ME If length = 0, this is just
		       REF_OPER (is_null) */
		    pko new_typespec =
		      mk_foresliced_basvector (typespec,
					       pdata->cvec.len - left + 1,
					       *tag);
		    pko raw_oplist = *extra_result;
		    *extra_result =
		      LISTSTAR4 (
				 REF_OPER (destructure_resume),
				 /* ^V= (result-so-far argobject spec
				    optional?) */
				 mk_load (LIST5 (mk_load_ix (0, 0),
						 argobject,
						 new_typespec, 
						 kernel_bool (saw_optional),
						 K_NIL)),
				 mk_store (K_ANY, 1),
				 /* ^V= result-so-far */
				 raw_oplist);
		    return destr_must_call_k;
		  }
		default:
		  errx (7, "Unrecognized enumeration");
		}
	    }
	}
      if(argobject == K_NIL)
	{ return destr_success; }
      else if (is_promise (argobject))
	{
	  pko new_typespec = REF_OPER (is_null);
	  *extra_result =
	    destructure_make_ops (argobject,
				  new_typespec,
				  saw_optional,
				  provoker);
	  return destr_must_call_k;
	}
      else
	{
	  *extra_result =
	    LIST2(mk_integer(el_num), mk_symbol("too-many"));
	  return destr_err;
	}
      
    }
  else if (!no_call_k(typespec))
    {
      if (!is_combiner (typespec))
	{
	  KERNEL_ERROR_0 (sc, "spec must be a combiner");
	  /* NOTREACHED */
	}
      
      *extra_result =
	destructure_make_ops_to_bool (argobject, typespec);
      return destr_must_call_k;
    }
  else if(typecheck(sc, argobject, typespec))
    {
      *outarray[0] = argobject;
      ++*outarray;
      return destr_success;
    }
  else if (is_promise (argobject))
    {
      *extra_result =
	destructure_make_ops (argobject,
			      typespec,
			      0,
			      provoker);
      return destr_must_call_k;
    }
  else
    {
      pko result = where_typemiss(sc, argobject, typespec);
      result = result ? result : mk_string("Couldn't find the typemiss");
      *extra_result = result;
      return destr_err;
    }
}
/*_     , destructure_to_array */
void
destructure_to_array
(klink * sc,
 pko obj,		       /* Object to extract values from */
 pko type,		       /* Type spec */
 pko * array,		       /* Array to be filled */
 size_t length,		       /* Maximum length of that array */
 pko resume_op,		       /* Combiner to schedule if we resume */
 pko resume_data,	/* Extra data to the resume op */	       
 pko provoker		/* Provoker, in case of error */	       
 )
{
  if (type == K_NO_TYPE)
    { return; }
  pko * orig_array = array;
  pko extra_result = 0;
  kt_destr_outcome outcome =
    destructure (sc,
		 obj,
		 type,
		 &array,
		 array + length,
		 &extra_result,
		 0,
		 provoker);
  switch (outcome)
    {
    case destr_success:
      return;
      /* NOTREACHED */
    case destr_err:
      {
	assert (extra_result);
	_klink_error_1 (sc, "type mismatch:",
			LIST2 (provoker, extra_result));
	return;
      }      
      /* NOTREACHED */
      
    case destr_must_call_k:
      {
	/* Arrange for a resume. */
	int read_len = array - orig_array;
	pko result_so_far = mk_destr_result (read_len, orig_array, K_NIL);
	assert (is_combiner (resume_op));
	CONTIN_0_RAW (resume_op, sc);
	/* ^^^V= (final-destr_result . resume_data) */
	CONTIN_0_RAW (mk_load (LISTSTAR2 (mk_load_ix (0, 0),
					  resume_data)),
		      sc);
	CONTIN_0_RAW (mk_store (K_ANY, 1), sc);
	/* ^^^V= final-destr_result */
	schedule_rv_list (sc, extra_result);
	/* ^^^V= current-destr_result */
	/* $$ENCAPSULATE ME */
	sc->value = result_so_far;
	longjmp (sc->pseudocontinuation, 1);
      /* NOTREACHED */
	return;
      }
      /* NOTREACHED */

    default:
      errx (7, "Unrecognized enumeration");
    }  
}

/*_     , destructure_resume */
SIG_CHKARRAY (destructure_resume) =
{
  REF_OPER (is_destr_result),
  K_ANY,
  K_TY_DESTRSPEC,
  REF_OPER (is_bool),
  K_ANY,
};
DEF_SIMPLE_CFUNC (ps0a5, destructure_resume, 0)
{
  WITH_5_ARGS (destr_result, argobject, typespec, opt_p, provoker);
  const int max_args = 5;
  pko arg_array [max_args];
  pko * outarray = arg_array;
  pko extra_result = 0;
  kt_destr_outcome outcome =
    destructure (sc,
		 argobject,
		 typespec,
		 &outarray,
		 arg_array + max_args,
		 &extra_result,
		 (opt_p == K_T),
		 provoker);
  switch (outcome)
    {
    case destr_success:
      {
	int new_len = outarray - arg_array;
	return
	  mk_destr_result_add (destr_result, new_len, arg_array);
      }
      /* NOTREACHED */
    case destr_err:
      KERNEL_ERROR_1 (sc, "type mismatch:",
		      LIST2 (provoker, extra_result));
      /* NOTREACHED */
      
    case destr_must_call_k:
      {
	/* Arrange for another force+resume. This will feed whatever
	   was there before. */
	int read_len = outarray - arg_array;
	pko result_so_far =
	  mk_destr_result_add (destr_result,
			       read_len,
			       arg_array);
	schedule_rv_list (sc, extra_result);
	return result_so_far;
      }
      /* NOTREACHED */

    default:
      errx (7, "Unrecognized enumeration");
      /* NOTREACHED */
    }
}
/*_     , do-destructure */
/* We don't have a typecheck typecheck predicate yet, so accept
   anything for arg2. Really it can be what typecheck accepts or
   T_DESTRUCTURE, checked recursively.  */
SIG_CHKARRAY (do_destructure) = { K_ANY, K_ANY, };
DEF_SIMPLE_APPLICATIVE (ps0a2, do_destructure,T_NO_K,unsafe,"do-destructure")
{
  WITH_2_ARGS (argobject,typespec);
  int len = destructure_how_many (typespec);
  pko vec = mk_vector (len, K_NIL);
  WITH_UNBOXED_UNSAFE (pdata,kt_destr_list,vec);
  destructure_to_array
    (sc,
     argobject,
     typespec,
     pdata->cvec.els,
     len, 
     REF_OPER (destr_result_to_vec),
     K_NIL,
     REF_OPER (do_destructure));
  
  return vec;
}

/*_   , C functions as objects */
/*_    . Structs */
/*_     , store */
typedef struct kt_opstore
{
  pko destr;			/* Often a T_DESTRUCTURE */
  int frame_depth;
} kt_opstore;

/*_    . cfunc */
DEF_T_PRED (is_cfunc, T_CFUNC,no,"");

#if 0
/* For external use, if some code ever wants to make these objects
   dynamically. */
/* $$MAKE ME SAFE Set type-check fields */
pko
mk_cfunc (const kt_cfunc * f)
{
  typedef kt_boxed_cfunc TT;
  errx(4, "Don't use mk_cfunc yet")
  TT *pbox = GC_MALLOC (sizeof (TT));
  pbox->type = T_CFUNC;
  pbox->data = *f;
  return PTR2PKO(pbox);
}
#endif

INLINE const kt_cfunc *
get_cfunc_func (pko p)
{
  WITH_PSYC_UNBOXED(kt_cfunc,p,T_CFUNC,0)
  return pdata;
}
/*_    . cfunc_resume */
/*_     , Create */
/*_      . mk_cfunc_resume */
pko
mk_cfunc_resume (pko cfunc)
{
  ALLOC_BOX_PRESUME (kt_cfunc, T_CFUNC_RESUME);
  pbox->data = *get_cfunc_func (cfunc);
  return PTR2PKO(pbox);
}  

/*_    . Curried functions */
/*_     , About objects */
static INLINE int
is_curried (pko p)
{ return is_type (p, T_CURRIED); }     

INLINE pko
mk_curried (decurrier_f decurrier, pko args, pko next)
{
  ALLOC_BOX(pbox,T_CURRIED,kt_boxed_curried);
  pbox->data.decurrier = decurrier;
  pbox->data.args      = args;
  pbox->data.next      = next;
  pbox->data.argcheck  = 0;
  return PTR2PKO(pbox);
}
/*_     , Operations */
/*_      . call_curried */
pko
call_curried(klink * sc, pko curried, pko value)
{
  WITH_PSYC_UNBOXED(kt_curried,curried,T_CURRIED,sc);

  /* First schedule the next one if there is any */
  if(pdata->next)
    {
      klink_push_cont(sc, pdata->next);
    }
  
  /* Then call the decurrier with the data field and the value,
     returning its result. */
  return pdata->decurrier (sc, pdata->args, value);
}  

/*_    . Chains */
/*_     , Struct */
typedef kt_vector kt_chain;

/*_     , Creating */
/*_      . Statically */
#define SIG_CHAIN(C_NAME) pko CHAIN_NAME(C_NAME)[]
#define DEF_CHAIN(NAME, ARRAY_NAME)			\
  DEF_VEC(T_CHAIN | T_IMMUTABLE, NAME, ARRAY_NAME)

#define DEF_SIMPLE_CHAIN(C_NAME)			\
  RGSTR(all-builtins,"C-" #C_NAME, REF_OPER (C_NAME))	\
  DEF_CHAIN(OPER(C_NAME), CHAIN_NAME(C_NAME))


/*_     , Operations */
void
schedule_chain(klink * sc, const kt_vector * chain)
{
  _kt_spagstack dump = sc->dump;
  int i;
  for(i = chain->len - 1; i >= 0; i--)
    {
      pko comb = chain->els[i];
      /* If frame_depth is unassigned, assign it. */
      if(_get_type(comb) == T_STORE)
	{
	  WITH_UNBOXED_UNSAFE( pdata, kt_opstore, comb );
	  if(pdata->frame_depth < 0)
	    { pdata->frame_depth = chain->len - 1 - i; }
	}
      /* Push it as a combiner */
      dump = klink_push_cont_aux(dump, comb, sc->envir);
    }
  sc->dump = dump;
}

/*_      . eval_chain */
pko
eval_chain( klink * sc, pko functor, pko value )
{
  WITH_PSYC_UNBOXED( kt_vector, functor, T_CHAIN, 0 );
  schedule_chain( sc, pdata);
  return value;
}
/*_      . schedule_rv_list */
void
schedule_rv_list (klink * sc, pko list)
{
  WITH_REPORTER (sc);
  _kt_spagstack dump = sc->dump;
  for(; list != K_NIL; list = cdr (list))
    {
      pko comb = car (list);
      /* $$PUNT If frame_depth is unassigned, assign it. */

      /* Push it as a combiner */
      dump = klink_push_cont_aux(dump, comb, sc->envir);
    }
  sc->dump = dump;
}
/*_    . No-trace */
/*_     , Create */
inline static pko
mk_notrace( pko combiner )
{
  ALLOC_BOX_PRESUME( pko, T_NOTRACE );
  *pdata = combiner;
  return PTR2PKO(pbox);
}

/*_     , Parts */
inline static pko
notrace_comb( pko p )
{
  WITH_PSYC_UNBOXED( pko, p, T_NOTRACE, 0 );
  return *pdata;
}
/*_    . Store */
/*_     , Create */
/*_      . statically */
#define STORE_DEF(DATA)				\
  { T_STORE | T_IMMUTABLE, { DATA, -1, }, }

#define ANON_STORE(DATA)			\
  ANON_REF (kt_opstore, STORE_DEF(DATA))

/*_      . dynamically */
pko
mk_store (pko data, int depth)
{
  ALLOC_BOX_PRESUME(kt_opstore, T_STORE | T_IMMUTABLE);
  pdata->destr       = data;
  pdata->frame_depth = depth;
  return PTR2PKO(pbox);
}  

/*_    . Load */
/*_     , Struct */
typedef pko kt_opload;

/*_     , Create */
/*_      . statically */
#define LOAD_DEF( DATA )			\
  { T_LOAD | T_IMMUTABLE, DATA, }

#define ANON_LOAD( DATA )			\
  ANON_REF( pko, LOAD_DEF( DATA ))

#define ANON_LOAD_IX( X, Y )		 \
  ANON_PAIR(ANON_REF(num, INT_DEF( X )), \
	    ANON_REF(num, INT_DEF( Y )))
/*_      . dynamically */
/*_       , mk_load_ix */
pko
mk_load_ix (int x, int y)
{
  return cons (mk_integer (x), mk_integer (y));
}  
/*_       , mk_load */
pko
mk_load (pko data)
{
  ALLOC_BOX_PRESUME(kt_opload, T_LOAD | T_IMMUTABLE);
  *pdata = data;
  return PTR2PKO(pbox);
}

/*_   , pairs proper */
/*_    . Type */
DEF_T_PRED (is_pair, T_PAIR,ground, "pair?/o1");

/*_    . Create */
SIG_CHKARRAY(Xcons) = { K_ANY, K_ANY, };
DEF_SIMPLE_DESTR(Xcons);
DEF_APPLICATIVE_W_DESTR(p00a2,mk_pair, REF_DESTR(Xcons),T_NO_K,ground, "cons")
{
  WITH_2_ARGS(a,b);
  return cons (a, b);
}

DEF_APPLICATIVE_W_DESTR(p00a2,mk_mutable_pair, REF_DESTR(Xcons),T_NO_K,ground, "mcons")
{
  WITH_2_ARGS(a,b);
  return mcons (a, b);
}

/*_    . Parts and operations */

SIG_CHKARRAY(pair_cxr) = { REF_OPER(is_pair), };
DEF_SIMPLE_DESTR(pair_cxr);
DEF_APPLICATIVE_W_DESTR(ps0a1,pair_car, REF_DESTR(pair_cxr),T_NO_K,ground, "car")
{
  WITH_1_ARGS(p);
  return v2car(sc,T_PAIR,p);
}

DEF_APPLICATIVE_W_DESTR(ps0a1,pair_cdr, REF_DESTR(pair_cxr),T_NO_K,ground, "cdr")
{
  WITH_1_ARGS(p);
  return v2cdr(sc,T_PAIR,p);
}

SIG_CHKARRAY(pair_set_cxr) = { REF_OPER(is_pair),  K_ANY, };
DEF_SIMPLE_DESTR(pair_set_cxr);
DEF_APPLICATIVE_W_DESTR(ps0a2,set_car, REF_DESTR(pair_set_cxr),T_NO_K,ground, "set-car!")
{
  WITH_2_ARGS(p,q);
  v2set_car(sc,T_PAIR,p,q);
  return K_INERT;
}

DEF_APPLICATIVE_W_DESTR(ps0a2,set_cdr, REF_DESTR(pair_set_cxr),T_NO_K,ground, "set-cdr!")
{
  WITH_2_ARGS(p,q);
  v2set_cdr(sc,T_PAIR,p,q);
  return K_INERT;
}
/*_     , Normal (one arg) */
/*_     , Values as pairs */
DEF_CFUNC_RAW(OPER (valcar), ps0a1, pair_car, REF_OPER (is_pair), T_NO_K);
DEF_CFUNC_RAW(OPER (valcdr), ps0a1, pair_cdr, REF_OPER (is_pair), T_NO_K);

/*_   , Strings */
/*_    . Type */
DEF_T_PRED (is_string, T_STRING,ground,"string?/o1");
/*_    . Create */

INTERFACE INLINE pko
mk_string (const char *str)
{
  return mk_bastring (T_STRING, str, strlen (str), 0);
}

INTERFACE INLINE pko
mk_counted_string (const char *str, int len)
{
  return mk_bastring (T_STRING, str, len, 0);
}

INTERFACE INLINE pko
mk_empty_string (int len, char fill)
{
  return mk_bastring (T_STRING, 0, len, fill);
}
/*_    . Create static */
/* $$WRITE ME  As for k_print_terminate_list macros */

/*_    . Accessors */
INTERFACE INLINE char *
string_value (pko p)
{
  return bastring_value(0,T_STRING,p);
}

INTERFACE INLINE int
string_len (pko p)
{
  return bastring_len(0,T_STRING,p);
}

/*_   , Symbols */
/*_    . Type */
DEF_T_PRED(is_symbol, T_SYMBOL,ground,"symbol?/o1");
/*_    . Create */
static pko
mk_symbol_obj (const char *name)
{
  return mk_bastring (T_SYMBOL | T_IMMUTABLE, name, strlen (name), 0);
}

/* We want symbol objects to be unique per name, so check an oblist of
   unique symbols. */
INTERFACE pko
mk_symbol (const char *name)
{
  /* first check oblist */
  pko x = oblist_find_by_name (name);
  if (x != K_NIL)
    {
      return x;
    }
  else
    {
      x = oblist_add_by_name (name);
      return x;
    }
}
/*_    . oblist implementation */
/*_     , Global object */
static pko oblist = 0;
/*_     , Oblist as hash table */
#ifndef USE_OBJECT_LIST

static int hash_fn (const char *key, int table_size);

static pko
oblist_initial_value ()
{
  return mk_vector (461, K_NIL);
}

/* returns the new symbol */
static pko
oblist_add_by_name (const char *name)
{
  pko x = mk_symbol_obj (name);
  int location = hash_fn (name, vector_len (oblist));
  set_vector_elem (oblist, location,
		   cons (x, vector_elem (oblist, location)));
  return x;
}

static INLINE pko
oblist_find_by_name (const char *name)
{
  int location;
  pko x;
  char *s;
  WITH_REPORTER(0);

  location = hash_fn (name, vector_len (oblist));
  for (x = vector_elem (oblist, location); x != K_NIL; x = cdr (x))
    {
      s = symname (0,car (x));
      /* case-insensitive, per R5RS section 2. */
      if (stricmp (name, s) == 0)
	{
	  return car (x);
	}
    }
  return K_NIL;
}

static pko
oblist_all_symbols (void)
{
  int i;
  pko x;
  pko ob_list = K_NIL;

  for (i = 0; i < vector_len (oblist); i++)
    {
      for (x = vector_elem (oblist, i); x != K_NIL; x = cdr (x))
	{
	  ob_list = mcons (x, ob_list);
	}
    }
  return ob_list;
}

/*_     , Oblist as list */
#else

static pko
oblist_initial_value ()
{
  return K_NIL;
}

static INLINE pko
oblist_find_by_name (const char *name)
{
  pko x;
  char *s;
  WITH_REPORTER(0);
  for (x = oblist; x != K_NIL; x = cdr (x))
    {
      s = symname (0,car (x));
      /* case-insensitive, per R5RS section 2. */
      if (stricmp (name, s) == 0)
	{
	  return car (x);
	}
    }
  return K_NIL;
}

/* returns the new symbol */
static pko
oblist_add_by_name (const char *name)
{
  pko x = mk_symbol_obj (name);
  oblist = cons (x, oblist);
  return x;
}

static pko
oblist_all_symbols (void)
{
  return oblist;
}

#endif


/*_    . Parts and operations */
SIG_CHKARRAY(string_to_symbol) = { REF_OPER(is_string), };
DEF_SIMPLE_APPLICATIVE(ps0a1,string_to_symbol,T_NO_K,ground, "string->symbol")
{
  return mk_symbol(string_value(arg1));
}

INTERFACE INLINE char *
symname (sc_or_null sc, pko p)
{
  return bastring_value (sc,T_SYMBOL, p);
}


/*_   , Vectors */

/*_    . Type */
DEF_T_PRED (is_vector, T_VECTOR,unsafe,"vector?/o1");

/*_    . Create */
/*_     , mk_vector (T_ level) */
INTERFACE static pko
mk_vector (int len, pko fill)
{ return mk_filled_basvector(len, fill, T_VECTOR); }

/*_     , k_mk_vector (K level) */
/* $$RETHINK ME  This may not be wanted. */
SIG_CHKARRAY(k_mk_vector) = { REF_OPER(is_integer), REF_KEY(K_TYCH_OPTIONAL), K_ANY, };
DEF_SIMPLE_APPLICATIVE (ps0a2, k_mk_vector,T_NO_K,unsafe,"make-vector")
{
  WITH_2_ARGS(k_len, fill);

  int len = ivalue (k_len);
  if (fill == K_INERT)
    { fill = K_NIL; }
  return mk_vector (len, fill);
}

/*_     , vector */
/* K_ANY instead of REF_OPER(is_finite_list) because
   mk_basvector_w_args checks list-ness internally */  
DEF_APPLICATIVE_W_DESTR(ps0a1, vector, K_ANY,T_NO_K,unsafe,"vector")
{
  WITH_1_ARGS(p);
  return mk_basvector_w_args(sc,p,T_VECTOR);
}

/*_    . Operations (T_ level) */
/*_     , fill_vector */

INTERFACE static void
fill_vector (pko vec, pko obj)
{
  assert(_get_type(vec) == T_VECTOR);
  unsafe_basvector_fill(vec,obj);
}

/*_    . Parts of vectors (T_ level) */

INTERFACE static int
vector_len (pko vec)
{
  assert(_get_type(vec) == T_VECTOR);
  return basvector_len(vec);
}

INTERFACE static pko
vector_elem (pko vec, int ielem)
{
  assert(_get_type(vec) == T_VECTOR);
  return basvector_elem(vec, ielem);
}

INTERFACE static void
set_vector_elem (pko vec, int ielem, pko a)
{
  assert(_get_type(vec) == T_VECTOR);
  basvector_set_elem(vec, ielem, a);
  return;
}

/*_   , Promises */
/* T_PROMISE is essentially a handle, pointing to a pair of either
   (expression env) or (value #f).  We use #f, not nil, because nil is
   a possible environment. */

/*_    . Create */
/*_     , $lazy */
RGSTR(ground,"$lazy", REF_OPER(mk_promise_lazy))
DEF_CFUNC(ps0a1, mk_promise_lazy, K_ANY_SINGLETON, T_NO_K)
{
  WITH_1_ARGS(p);
  pko guts = mcons(p, mcons(sc->envir, mk_continuation(sc->dump)));
  return v2cons (T_PROMISE, guts, K_NIL);
}
/*_     , memoize */
/* $$CHECK ME Is K_ANY correct?  Or K_ANY_SINGLETON?  */
DEF_APPLICATIVE_W_DESTR(p00a1,mk_promise_memo,K_ANY,T_NO_K,ground,"memoize")
{
  WITH_1_ARGS(p);
  pko guts = mcons(p, K_F);
  return v2cons (T_PROMISE, guts, K_NIL);
}
/*_    . Type */

DEF_T_PRED (is_promise,T_PROMISE,ground,"promise?/o1");
/*_    . Helpers */
/*_     , promise_schedule_eval */
inline pko
promise_schedule_eval(klink * sc, pko p)
{
  WITH_REPORTER(sc);  
  pko guts = unsafe_v2car(p);
  pko env     = car(cdr(guts));
  pko dynxtnt = cdr(cdr(guts));
  /* Arrange to eval the expression and pass the result to
     handle_promise_result */
  CONTIN_1R(dcrry_2ALLVLL,handle_promise_result,sc,p);
  /* $$ENCAP ME  This deals with continuation guts, so should be
     encapped.  As a special continuation-maker? */ 
  _kt_spagstack new_dump =
    special_dynxtnt (cont_dump(dynxtnt), sc->dump, env);
  sc->dump = new_dump;
  CONTIN_2(dcrry_2dotALL, kernel_eval, sc, car(guts), env);
  return K_INERT;
}  
/*_     , handle_promise_result */
SIG_CHKARRAY(handle_promise_result) = { REF_OPER(is_promise), K_ANY };
DEF_SIMPLE_CFUNC(ps0a2,handle_promise_result,0)
{
  /* guts are only made by C code so if they're wrong it's a C
     error */
  WITH_REPORTER(0);  		
  WITH_2_ARGS(p,value);
  pko guts = unsafe_v2car(p);

  /* if p already has a result, return it */
  if(cdr(guts) == K_F)
    { return car(guts); }
  /* If value is again a promise, set this promise's guts to that
     promise's guts and force it again, which will force both (This is
     why we need promises to be 2-layer) */
  else if(is_promise(value))
    {
      unsafe_v2set_car (p, unsafe_v2car(value));
      return promise_schedule_eval(sc, p);
    }
  /* Otherwise set the value and return it. */
  else
    {
      unsafe_v2set_car (guts, value);
      unsafe_v2set_cdr (guts, K_F);
      return value;
    }
}
/*_    . Operations */
/*_     , force */
DEF_APPLICATIVE_W_DESTR (ps0a1, force, K_ANY_SINGLETON,T_NO_K,ground,"force")
{
  /* guts are only made by this C code here, so if they're wrong it's
     a C error */
  WITH_REPORTER(0);  		
  WITH_1_ARGS(p);
  if(!is_promise(p))
    { return p; }

  pko guts = unsafe_v2car(p);
  if(cdr(guts) == K_F)
    { return car(guts); }
  else
    { return promise_schedule_eval(sc,p); }
}

/*_   , Ports */
/*_    . Creating */

/* $$IMPROVE ME Just directly contain the port structure.  Possibly
   split port into several T_ types.  */
static pko
mk_port (port * pt)
{
  ALLOC_BOX_PRESUME (port *, T_PORT);
  pbox->data = pt;
  return PTR2PKO(pbox);
}

static port *
port_rep_from_filename (const char *fn, int prop)
{
  FILE *f;
  char *rw;
  port *pt;
  if (prop == (port_input | port_output))
    {
      rw = "a+";
    }
  else if (prop == port_output)
    {
      rw = "w";
    }
  else
    {
      rw = "r";
    }
  f = fopen (fn, rw);
  if (f == 0)
    {
      return 0;
    }
  pt = port_rep_from_file (f, prop);
  pt->rep.stdio.closeit = 1;

#if SHOW_ERROR_LINE
  if (fn)
    { pt->rep.stdio.filename = store_string (strlen (fn), fn, 0); }

  pt->rep.stdio.curr_line = 0;
#endif
  return pt;
}

static pko
port_from_filename (const char *fn, int prop)
{
  port *pt;
  pt = port_rep_from_filename (fn, prop);
  if (pt == 0)
    {
      return K_NIL;
    }
  return mk_port (pt);
}

static port *
port_rep_from_file (FILE * f, int prop)
{
  port *pt;
  pt = (port *) GC_MALLOC_ATOMIC (sizeof *pt);
  if (pt == NULL)
    {
      return NULL;
    }
  /* Don't care what goes in these but GC really wants to provide it
     so here are dummy objects to put it in. */
  GC_finalization_proc ofn;
  GC_PTR               ocd;
  GC_register_finalizer(pt, port_finalize_file, 0, &ofn, &ocd);
  pt->kind = port_file | prop;
  pt->rep.stdio.file = f;
  pt->rep.stdio.closeit = 0;
  return pt;
}

static pko
port_from_file (FILE * f, int prop)
{
  port *pt;
  pt = port_rep_from_file (f, prop);
  if (pt == 0)
    {
      return K_NIL;
    }
  return mk_port (pt);
}

static port *
port_rep_from_string (char *start, char *past_the_end, int prop)
{
  port *pt;
  pt = (port *) GC_MALLOC_ATOMIC (sizeof (port));
  if (pt == 0)
    {
      return 0;
    }
  pt->kind = port_string | prop;
  pt->rep.string.start = start;
  pt->rep.string.curr = start;
  pt->rep.string.past_the_end = past_the_end;
  return pt;
}

static pko
port_from_string (char *start, char *past_the_end, int prop)
{
  port *pt;
  pt = port_rep_from_string (start, past_the_end, prop);
  if (pt == 0)
    {
      return K_NIL;
    }
  return mk_port (pt);
}

#define BLOCK_SIZE 256

static int
realloc_port_string (port * p)
{
  /* $$IMPROVE ME  Just use REALLOC. */
  char *start = p->rep.string.start;
  size_t new_size = p->rep.string.past_the_end - start + 1 + BLOCK_SIZE;
  char *str = GC_MALLOC_ATOMIC (new_size);
  if (str)
    {
      memset (str, ' ', new_size - 1);
      str[new_size - 1] = '\0';
      strcpy (str, start);
      p->rep.string.start = str;
      p->rep.string.past_the_end = str + new_size - 1;
      p->rep.string.curr -= start - str;
      return 1;
    }
  else
    {
      return 0;
    }
}


static port *
port_rep_from_scratch (void)
{
  port *pt;
  char *start;
  pt = (port *) GC_MALLOC_ATOMIC (sizeof (port));
  if (pt == 0)
    {
      return 0;
    }
  start = GC_MALLOC_ATOMIC (BLOCK_SIZE);
  if (start == 0)
    {
      return 0;
    }
  memset (start, ' ', BLOCK_SIZE - 1);
  start[BLOCK_SIZE - 1] = '\0';
  pt->kind = port_string | port_output | port_srfi6;
  pt->rep.string.start = start;
  pt->rep.string.curr = start;
  pt->rep.string.past_the_end = start + BLOCK_SIZE - 1;
  return pt;
}

static pko
port_from_scratch (void)
{
  port *pt;
  pt = port_rep_from_scratch ();
  if (pt == 0)
    {
      return K_NIL;
    }
  return mk_port (pt);
}
/*_     , Interface */
/*_      . open-input-file */
SIG_CHKARRAY(k_open_input_file) =
  { REF_OPER(is_string), };
DEF_SIMPLE_APPLICATIVE(ps0a1,k_open_input_file,0,ground, "open-input-file")
{
  WITH_1_ARGS(filename);
  return port_from_filename (string_value(filename), port_file | port_input);
}


/*_    . Testing */

DEF_T_PRED (is_port, T_PORT,ground,"port?/o1");

DEF_SIMPLE_PRED (is_inport,T_NO_K,ground,"input-port?/o1")
{
  WITH_1_ARGS(p);
  return is_port (p) && portvalue (p)->kind & port_input;
}

DEF_SIMPLE_PRED (is_outport,T_NO_K,ground,"output-port?/o1")
{
  WITH_1_ARGS(p);
  return is_port (p) && portvalue (p)->kind & port_output;
}

/*_    . Values */
INLINE port *
portvalue (pko p)
{
  WITH_PSYC_UNBOXED(port *,p,T_PORT,0);
  return *pdata;
}

INLINE void
set_portvalue (pko p, port * newport)
{
  assert_mutable(0,p);
  WITH_PSYC_UNBOXED(port *,p,T_PORT,0);
  *pdata = newport;
  return;
}

/*_    . reading from ports */
static int
inchar (port *pt)
{
  int c;

  if (pt->kind & port_saw_EOF)
    { return EOF; }
  c = basic_inchar (pt);
  if (c == EOF)
    { pt->kind |= port_saw_EOF; }
#if SHOW_ERROR_LINE
  else if (c == '\n')
    {
      if (pt->kind & port_file)
	{ pt->rep.stdio.curr_line++; }      
    }
#endif
  
  return c;
}

static int
basic_inchar (port * pt)
{
  if (pt->kind & port_file)
    {
      return fgetc (pt->rep.stdio.file);
    }
  else
    {
      if (*pt->rep.string.curr == 0 ||
	  pt->rep.string.curr == pt->rep.string.past_the_end)
	{
	  return EOF;
	}
      else
	{
	  return *pt->rep.string.curr++;
	}
    }
}

/* back character to input buffer */
static void
backchar (port * pt, int c)
{
  if (c == EOF)
    { return; }

  if (pt->kind & port_file)
    {
      ungetc (c, pt->rep.stdio.file);
#if SHOW_ERROR_LINE
      if (c == '\n')
	{
	  pt->rep.stdio.curr_line--;
	}
#endif
    }
  else
    {
      if (pt->rep.string.curr != pt->rep.string.start)
	{
	  --pt->rep.string.curr;
	}
    }
}

/*_     , Interface */

/*_      . (get-char textual-input-port) */
SIG_CHKARRAY(get_char) = { REF_OPER(is_inport), };
DEF_SIMPLE_APPLICATIVE(p00a1,get_char,T_NO_K,ground, "get-char")
{
  WITH_1_ARGS(port);
  assert(is_inport(port));
  int c = inchar(portvalue(port));
  if(c == EOF)
    { return K_EOF; }
  else
    { return mk_character(c); }
}

/*_    . Finalization */
static void
port_finalize_file(GC_PTR obj, GC_PTR client_data)
{
  port *pt = obj;
  if ((pt->kind & port_file) && pt->rep.stdio.closeit)
    { port_close_port (pt, port_input | port_output); }
}  

static void
port_close (pko p, int flag)
{
  assert(is_port(p));
  port_close_port(portvalue (p), flag);
}

static void
port_close_port (port * pt, int flag)
{
  pt->kind &= ~flag;
  if ((pt->kind & (port_input | port_output)) == 0)
    {
      if (pt->kind & port_file)
	{
#if SHOW_ERROR_LINE
	  /* Cleanup is here so (close-*-port) functions could work too */
	  pt->rep.stdio.curr_line = 0;

#endif

	  fclose (pt->rep.stdio.file);
	}
      pt->kind = port_free;
    }
}


/*_   , Encapsulation type */

SIG_CHKARRAY(is_encap) = { REF_OPER(is_key), K_ANY };
DEF_SIMPLE_CFUNC(b00a2, is_encap,T_NO_K)
{
  WITH_2_ARGS(type, p);
  if (is_type (p, T_ENCAP))
    {
      WITH_UNBOXED_UNSAFE(pdata,kt_encap,p);
      return (pdata->type == type);
    }
  else
    {
      return 0;
    }
}

/* NOT directly part of the interface. */
SIG_CHKARRAY(unencap) = { REF_OPER(is_key),  K_ANY};
DEF_SIMPLE_CFUNC(ps0a2, unencap,T_NO_K)
{
  WITH_2_ARGS(type, p);
  if (is_encap (type, p))
    {
      WITH_UNBOXED_UNSAFE(pdata,kt_encap,p);
      return pdata->value;
    }
  else
    {
      /* We have no type-name to give to the error message. */
      KERNEL_ERROR_0 (sc, "unencap: wrong type");
    }
}

/* NOT directly part of the interface. */
SIG_CHKARRAY(mk_encap) = { REF_OPER(is_key),  K_ANY};
DEF_SIMPLE_CFUNC(p00a2, mk_encap,T_NO_K)
{
  WITH_2_ARGS(type, value);
  ALLOC_BOX_PRESUME (kt_encap, T_ENCAP);
  pbox->data.type = type;
  pbox->data.value = value;
  return PTR2PKO(pbox);
}

DEF_APPLICATIVE_W_DESTR (p00a0, mk_encapsulation_type, K_NO_TYPE,T_NO_K,ground, "make-encapsulation-type/raw")
{
  /* A unique cell representing a type */
  pko type = mk_void();
  /* $$IMPROVE ME make typespecs for the curried objs.  trivpred is
     effectively that spec object.  */
  pko e   = wrap (mk_curried (dcrry_2ALLV01, type, REF_OPER (mk_encap)));
  pko trivpred = mk_curried (dcrry_2ALLV01, type, REF_OPER (is_encap));
  pko d   = wrap (mk_curried (dcrry_2ALLV01, type, REF_OPER (unencap))); 
  return LIST3 (e, trivpred, d);
}
/*_   , Listloop types */
/*_    . Forward declarations */
struct kt_listloop;
/*_    . Enumerations */
/*_     , Next-style */
/* How to turn the current list into current value and next list. */
typedef enum
{
  lls_1list,
  lls_many,
  lls_neighbors,
  lls_max,
} kt_loopstyle_step;
typedef enum
{
  lls_combiner,
  lls_count,
  lls_top_count,
  lls_stop_on,
  lls_num_args,
} kt_loopstyle_argix;

/*_    . Function signatures. */
typedef pko (* kt_listloop_mk_val)(pko value,  struct kt_listloop * pll);
/*_    . Struct */
typedef struct kt_listloop_style
{
  pko                combiner;	/* Default combiner or NULL. */
  int                collect_p;	/* Whether to collect a (reversed)
				   list of the returns. */
  kt_loopstyle_step  step;
  kt_listloop_mk_val mk_val; /* From returned value+state -> passed value. */
  pko                destructurer; /* A destructurer contents */
  /* Selection of args.  Each entry correspond to one arg in "full
     args", and indexes something in the array of actual args that the
     destructurer retrieves. */
  int                arg_select[lls_num_args]; 
} kt_listloop_style;
typedef struct kt_listloop
{
  pko combiner; 		/* The combiner to use repeatedly. */
  pko list;			/* The list to loop over */
  int top_length;		/* Length of top element, for lls_many. */
  int countdown;		/* Num elements left, or negative if unused. */
  int countup;			/* Upwards count from 0. */
  pko stop_on;			/* Stop if return value is this. Can
				   be 0 for unused. */
  kt_listloop_style * style;	/* Non-NULL pointer to style. */
} kt_listloop;
/*_     , Internal signatures */
pko
listloop_aux (klink * sc,
	      kt_listloop_style * style_v,
	      pko list,
	      pko style_args[lls_num_args]);
FORWARD_DECL_CFUNC (static, ps0a3, listloop_resume);

/*_    . Creating */
/*_     , Listloop styles */
/* Unused */
pko
mk_listloop_style
(pko combiner,
 int collect_p,
 kt_loopstyle_step step,
 kt_listloop_mk_val mk_val)
{
  ALLOC_BOX_PRESUME(kt_listloop_style,T_LISTLOOP_STYLE);
  pdata->combiner   = combiner;
  pdata->collect_p  = collect_p;
  pdata->step       = step;
  pdata->mk_val     = mk_val;
  return PTR2PKO(pbox);
}
/*_     , Listloops */
pko
mk_listloop
(pko combiner,
 pko list,
 int top_length,
 int count,
 pko stop_on,
 kt_listloop_style * style)
{
  ALLOC_BOX_PRESUME(kt_listloop,T_LISTLOOP);
  pdata->combiner   = combiner;
  pdata->list       = list;
  pdata->top_length = top_length;
  pdata->countdown  = count;
  pdata->countup    = -1;
  pdata->stop_on    = stop_on;
  pdata->style      = style;
  return PTR2PKO(pbox);
}
/*_     , Copying */
pko
copy_listloop(const kt_listloop * orig)
{
  ALLOC_BOX_PRESUME(kt_listloop,T_LISTLOOP);
  memcpy (pdata, orig, sizeof(kt_listloop));
  return PTR2PKO(pbox);
}
/*_    . Testing */
/* Unused so far */
DEF_T_PRED(is_listloop,       T_LISTLOOP,       no, "");
DEF_T_PRED(is_listloop_style, T_LISTLOOP_STYLE, no, "");
/*_    . Val-makers */
/*_    . Pre-existing style objects */
/*_     , listloop-style-sequence */
RGSTR(simple,"listloop-style-sequence",REF_OBJ(sequence_style))
static BOX_OF(kt_listloop_style) sequence_style =
{
  T_LISTLOOP_STYLE,
  {
    REF_OPER(kernel_eval),
    0,
    lls_1list,
    0,
    K_NO_TYPE,			/* No args contemplated */
    { [0 ... lls_num_args - 1] = -1, }
  }
};
/*_     , listloop-style-neighbors */
RGSTR(simple,"listloop-style-neighbors",REF_OBJ(neighbor_style))
SIG_CHKARRAY(neighbor_style) =
  {
    REF_OPER(is_integer),
  };
DEF_SIMPLE_DESTR(neighbor_style);
static BOX_OF(kt_listloop_style) neighbor_style =
{
  T_LISTLOOP_STYLE,
  {
    REF_OPER(val2val),
    1,
    lls_neighbors,
    0,
    REF_DESTR(neighbor_style),
    /* See http://gcc.gnu.org/onlinedocs/gcc/Designated-Inits.html. */
    { [0 ... lls_num_args - 1] = -1, [lls_count] = 0, },
  }
};
/*_    . Operations */
/*_     , listloop */
/* Create a listloop object. */
/* $$IMPROVE ME This may become what style operative T_ type calls.
   Rename it eval_listloop_style. */ 
SIG_CHKARRAY(listloop) =
{
  REF_OPER(is_listloop_style),
  REF_OPER(is_countable_list),
  REF_KEY(K_TYCH_DOT),
  K_ANY,
};
  
DEF_SIMPLE_APPLICATIVE(ps0a3, listloop,0,ground, "listloop")
{
  WITH_3_ARGS(style, list, args);

  WITH_UNBOXED_UNSAFE(style_v,kt_listloop_style, style);
  pko style_args[lls_num_args];
  /* Destructure the args by style */
  destructure_to_array(sc,
		       args,
		       style_v->destructurer,
		       style_args,
		       lls_num_args,
		       REF_OPER (listloop_resume),
		       LIST2 (style, list),
		       style);
  return listloop_aux (sc, style_v, list, style_args);
}
/*_     , listloop_resume */
SIG_CHKARRAY (listloop_resume) =
{
  REF_OPER (is_destr_result),
  REF_OPER(is_listloop_style),
  REF_OPER(is_countable_list),
};
DEF_SIMPLE_CFUNC(ps0a3, listloop_resume, 0)
{
  WITH_3_ARGS (destr_result, style, list);
  pko style_args[lls_num_args];
  destr_result_fill_array (destr_result, lls_num_args, style_args);
  WITH_UNBOXED_UNSAFE(style_v,kt_listloop_style, style);
  return listloop_aux (sc, style_v, list, style_args);
}
/*_     , listloop_aux */
pko
listloop_aux
(klink * sc, kt_listloop_style * style_v, pko list, pko style_args[lls_num_args])
{
  /*** Get the actual arg objects ***/
#define GET_OBJ(_INDEX) \
  ((style_v->arg_select[_INDEX] < 0) ? K_INERT : style_args[style_v->arg_select[_INDEX]])

  pko count      = GET_OBJ(lls_count);
  pko combiner   = GET_OBJ(lls_combiner);
  pko top_length = GET_OBJ(lls_top_count);
#undef GET_OBJ

  /*** Extract values from the objects, using defaults as needed ***/
  int countv      = (count      == K_INERT) ? -1L : ivalue(count);
  int top_lengthv = (top_length == K_INERT) ? 1 : ivalue(top_length);
  if(combiner == K_INERT)
    {
      combiner = style_v->combiner;
    }

  /*** Make the loop object itself ***/
  pko ll = mk_listloop( combiner, list, top_lengthv, countv, 0, style_v );
  return ll;
}
/*_     , Evaluating one iteration */
pko
eval_listloop(klink * sc, pko functor, pko value)
{
  WITH_REPORTER(sc);
  WITH_PSYC_UNBOXED(kt_listloop, functor, T_LISTLOOP, sc);

  /*** Test whether done, maybe return current value. ***/
  /* If we're not checking, value will be NULL so this won't
     trigger.  pdata->countup is 0 for the first element. */
  if((pdata->countup >= 0) && (value == pdata->stop_on))
    {
      /* $$IMPROVE ME  This will ct an "abnormal return" value from
	 this and the other data.  */
      return value;
    }
  /* If we're not counting down, value will be negative so this won't
     trigger.  */
  if(pdata->countdown == 0)
    {
      return value;
    }
  /* And if we run out of elements, we have to stop regardless. */
  if(pdata->list == K_NIL)
    {
      /* $$IMPROVE ME Error if we're counting down (ie, if count
	 is positive). */
      return value;
    }
      
  /*** Step list, getting new value ***/
  pko new_list, new_value;
  
  switch(pdata->style->step)
    {
    case lls_1list:
      new_list  = cdr( pdata->list );
      /* We assume the common case of val as list. */
      new_value = LIST1(car( pdata->list ));
      break;

    case lls_neighbors:
      /* $$IMPROVE ME Also test that next item (new_list) is non-empty */
      new_list  = cdr( pdata->list );
      new_value = LIST2(car( pdata->list ), car(new_list));
      break;
    case lls_many:
      new_list  = k_counted_map_cdr(sc, pdata->top_length, pdata->list, T_PAIR);
      new_value = k_counted_map_car(sc, pdata->top_length, pdata->list, T_PAIR);
      break;
    default:
      KERNEL_ERROR_0(sc,"I know nut-ting about that case");
    }

  /* Convert it if applicable. */
  if(pdata->style->mk_val)
    {
      new_value = pdata->style->mk_val(new_value, pdata);
    }

  /*** Arrange a new iteration. ***/
  /* We don't have to re-setup the final chain, if any, because it's
     still there from the earlier call.  Just the combiner (if any)
     and a fresh listloop operative.  */
  pko new_listloop = copy_listloop(pdata);
  {
    WITH_UNBOXED_UNSAFE(new_pdata,kt_listloop,new_listloop);
    new_pdata->list = new_list;
    if(new_pdata->countdown > 0)
      { new_pdata->countdown--; }
    new_pdata->countup++;
  }

  if(pdata->style->collect_p)
    {
      CONTIN_0_RAW (mk_curried(dcrry_NVLLdotALL, value, new_listloop), sc);
    }
  else
    {
      CONTIN_0_RAW(new_listloop, sc);
    }

  CONTIN_0_RAW(pdata->combiner, sc);
  return new_value;
}

/*_  . Handling lists */
/*_   , list* */
DEF_APPLICATIVE_W_DESTR (ps0a1, list_star, REF_OPER(is_finite_list),T_NO_K,ground, "list*")
{
  return v2list_star(sc, arg1, T_PAIR);
}
/*_   , reverse */
SIG_CHKARRAY(reverse) = { REF_OPER(is_finite_list), };
DEF_SIMPLE_APPLICATIVE (ps0a1, reverse,T_NO_K,ground, "reverse")
{
  WITH_1_ARGS(a);
  return v2reverse(a,T_PAIR);
}
/*_    . reverse list -- in-place */
/* Don't just use unsafe_v2reverse_in_place, it checks nothing.  This
   may be reserved for optimization only. */ 

/*_    . append list -- produce new list */
/* $$IMPROVE ME  This defines append/2 but we'll want append/N.  Do
   that in init. */
SIG_CHKARRAY(append) = { REF_OPER(is_finite_list), K_ANY, };
DEF_SIMPLE_APPLICATIVE (ps0a2, append,T_NO_K,simple, "append")
{
  WITH_2_ARGS(a,b);
  return v2append(sc,a,b,T_PAIR);
}
/*_   , is_finite_list */
DEF_SIMPLE_PRED (is_finite_list,T_NO_K,ground, "finite-list?/o1")
{
  WITH_1_ARGS(p);
  int4 metrics;
  get_list_metrics_aux(p, metrics);
  return (metrics[lm_num_nils] == 1);
}
/*_   , is_countable_list */
DEF_SIMPLE_PRED (is_countable_list,T_NO_K,ground, "countable-list?/o1")
{
  WITH_1_ARGS(p);
  int4 metrics;
  get_list_metrics_aux(p, metrics);
  return (metrics[lm_num_nils] || metrics[lm_cyc_len]);
}
/*_   , list_length */
/* Result is:
   proper list: length
   circular list: -1
   not even a pair: -2
   dotted list: -2 minus length before dot

   The extra meanings will change since callers can use
   get_list_metrics_aux now.  Return lm_acyc_len even for dotted
   lists, return positive infinity for circular lists.
*/
/* $$OBSOLESCENT */
int
list_length (pko p)
{
  int4 metrics;
  get_list_metrics_aux(p, metrics);
  /* A proper list */
  if(metrics[lm_num_nils] == 1)
    { return metrics[lm_acyc_len]; }
  /* A circular list */
  /* $$IMPROVE ME Return +oo.  First make a type and object for +oo  */
  if(metrics[lm_cyc_len] != 0)
    { return -1; }
  /* $$IMPROVE ME Return lm_acyc_len again, merge with the other
     case. */
  /* Otherwise it's dotted */
  return 2 - metrics[lm_acyc_len];
}
/*_   , list_length_k */
DEF_APPLICATIVE_W_DESTR(p00a1, list_length_k, K_ANY_SINGLETON,T_NO_K,ground, "length")
{
  WITH_1_ARGS(p);
  return mk_integer(list_length(p));
}

/*_   , get_list_metrics */
DEF_APPLICATIVE_W_DESTR(p00a1, get_list_metrics, K_ANY_SINGLETON,T_NO_K,ground, "get-list-metrics")
{
  WITH_1_ARGS(p);
  int4 metrics;
  get_list_metrics_aux(p, metrics);
  return LIST4(mk_integer(metrics[0]),
	       mk_integer(metrics[1]),
	       mk_integer(metrics[2]),
	       mk_integer(metrics[3]));
}
/*_   , get_list_metrics_aux */
/* RESULTS must be an int4 (an array of 4 integers).  get_list_metrics_aux
   will fill it with (See enum lm_index):
   
   * the number of pairs in a
   * the number of nil objects in a
   * the acyclic prefix length of a
   * the cycle length of a
*/

/* $$IMPROVE ME Take a flag so we can skip work such as finding
   prefix-length when we don't need to do it.  This will cause some
   result positions to be interpreted differently: when it's cycling,
   lm_acyc_len and lm_num_pairs may both overshoot (but never
   undershoot).
*/

void
get_list_metrics_aux (pko a, int4 presults)
{
  int * results = presults;	/* Make it easier to index. */
  int steps = 0;
  int power = 1;
  int loop_len = 1;
  pko slow, fast;
  WITH_REPORTER(0);
  
  /* Use Brent's Algorithm, but we have to check for nil and non-pair
     too, so I rearranged the loop.  We also count steps, because in
     some cases we use number of steps directly. */
  slow = fast = a;
  while (1)
    {
      if (fast == K_NIL)
	{
	  results[lm_num_pairs] = steps;
	  results[lm_num_nils]  = 1;
	  results[lm_acyc_len]  = steps;
	  results[lm_cyc_len]   = 0;
	  return;
	}
      if (!is_pair (fast))
	{
	  results[lm_num_pairs] = steps;
	  results[lm_num_nils]  = 0;
	  results[lm_acyc_len]  = steps;
	  results[lm_cyc_len]   = 0;
	  return;
	}
      fast = cdr (fast);
      if (fast == slow)
	{
	  /* The fast cursor has caught up with the slow cursor so the
	     structure is circular and loop_len is the cycle length.
	     We still need to find prefix length.
	  */
	  int prefix_len = 0;
	  int i = 0;
	  /* Restart the turtle from the beginning */
	  slow = a;
	  /* Restart the hare from position LOOP_LEN */
	  for(i = 0, fast = a; i < loop_len; i++)
	    { fast = cdr (fast); }
	  /* Since hare has exactly a loop_len head start, when it
	     goes around the loop exactly once it will be in the same
	     position as turtle, so turtle will have only walked the
	     acyclic prefix. */
	  while(fast != slow)
	    {
	      fast = cdr (fast);
	      slow = cdr (slow);
	      prefix_len++;
	    }
	  
	  results[lm_num_pairs] = prefix_len + loop_len;
	  results[lm_num_nils]  = 0;
	  results[lm_acyc_len]  = prefix_len;
	  results[lm_cyc_len]   = loop_len;
	  return;
	}
      if(power == loop_len)
	{
	  /* Re-plant the slow cursor */
	  slow = fast;
	  loop_len = 0;
	  power *= 2;
	}
      ++loop_len;
      ++steps;
    }
}
/*_  . Handling trees */
/*_   , copy_es_immutable */
DEF_APPLICATIVE_W_DESTR (ps0a1, copy_es_immutable, K_ANY_SINGLETON,T_NO_K,ground, "copy-es-immutable")
{
  WITH_1_ARGS(object);
  WITH_REPORTER(sc);
  if (is_pair (object))
    {
      /* If it's already immutable, can we assume it's immutable
       * all the way down and just return it? */
      return cons
	(copy_es_immutable (sc, car (object)),
	 copy_es_immutable (sc, cdr (object)));
    }
  else
    {
      return object;
    }
}
/*_   , Get tree cycles */
/*_    . Structs */
/*_     , kt_recurrence_table */
/* Really just a specialized resizeable lookup table from object to
   count.  Internals may change.  */ 
/* $$IMPROVE ME Look up more efficiently.  Current GC is not
   compacting, so we can hash or sort addresses meaningfully.  */
typedef struct
{
  pko * objs;
  int     * counts;
  int table_size;
  int alloced_size;
}
kt_recurrence_table;
/*_     , recur_entry */
typedef struct
{
  /* $$IMPROVE ME  These two fields may become one enumerated field */
  int count;
  int seen_in_walk;
  int index_in_walk;
} recur_entry;
/*_     , kt_recur_tracker */
typedef struct
{
  pko     * objs;
  recur_entry * entries;
  int table_size;
  int current_index;
} kt_recur_tracker;
/*_    . is_recurrence_table */
DEF_T_PRED(is_recurrence_table,T_RECURRENCES,ground, "recurrence-table?/o1");

/*_    . is_recur_tracker */
DEF_SIMPLE_PRED(is_recur_tracker,T_NO_K,ground, "recur-tracker?/o1")
{
  WITH_1_ARGS(p);
  return (p == K_NIL) || is_type (p, T_RECUR_TRACKER);
}
/*_    . recurrences_to_recur_tracker */
SIG_CHKARRAY(recurrences_to_recur_tracker) =
{ REF_OPER(is_recurrence_table), };
DEF_SIMPLE_APPLICATIVE(p00a1,recurrences_to_recur_tracker,T_NO_K,ground, "recurrences->tracker")
{
  WITH_1_ARGS(recurrences);
  assert_type(0,recurrences,T_RECURRENCES);			
 
  WITH_UNBOXED_UNSAFE(ptable, kt_recurrence_table,recurrences);
  /* $$IMPROVE ME Scan for counts > 1, and if there are none,
     return K_NIL. */
  if(ptable->table_size == 0)
    { return K_NIL; }
  {
    ALLOC_BOX_PRESUME(kt_recur_tracker,T_RECUR_TRACKER);
    /* $$MAKE ME SAFE This assumes that sharing is OK, recurrences
       won't mutate the LUT.  When we have COW or similar, make it
       safe.  At least check for immutability.  */
    pdata->objs       = ptable->objs;
    pdata->table_size = ptable->table_size;
    pdata->current_index = 0;
    pdata->entries =
      GC_MALLOC_ATOMIC(sizeof(recur_entry) * ptable->table_size);
    int i;
    for(i = 0; i < ptable->table_size; i++)
      {
	recur_entry * p_entry = &pdata->entries[i];
	p_entry->count = ptable->counts[i];
	p_entry->index_in_walk = 0;
	p_entry->seen_in_walk  = 0;
      }
    return PTR2PKO(pbox);
  }
}
/*_    . recurrences_list_objects */
/* $$WRITE ME  Get a list of all objects and their recurrence counts */
/*_    . objtable_get_index */
int
objtable_get_index
(pko * objs, int table_size, pko obj)
{
  int i;
  for(i = 0; i < table_size; i++)
    {
      if(obj == objs[i])
	{ return i; }
    }
  return -1;
}
/*_    . recurrences_get_seen_count */
/* Return the number of times OBJ has been seen before.  If "add" is
   non-zero, increment the count too (but return its previous
   value).  */
int
recurrences_get_seen_count
(kt_recurrence_table * p_cycles_data, pko obj, int add)
{
  int index = objtable_get_index(p_cycles_data->objs,
				 p_cycles_data->table_size,
				 obj);
  if(index >= 0)
    {
      int count = p_cycles_data->counts[index];
      /* Maybe record another sighting of this object. */
      if(add)
	{ p_cycles_data->counts[index]++; }
      /* We've found our return value. */
      return count;
    }

  /* We only get here if search didn't find anything. */
  /* Make sure we have enough space for this object. */
  if(add)
    {
      if(p_cycles_data->table_size == p_cycles_data->alloced_size)
	{
	  p_cycles_data->alloced_size *= 2;
	  p_cycles_data->counts = GC_REALLOC(p_cycles_data->counts, sizeof(int) * p_cycles_data->alloced_size);
	  p_cycles_data->objs = GC_REALLOC(p_cycles_data->objs, sizeof(pko) * p_cycles_data->alloced_size);
	}
      int index = p_cycles_data->table_size;
      /* Record what it was */
      p_cycles_data->objs[index] = obj;
      /* We have now seen it once. */
      p_cycles_data->counts[index] = 1;
      p_cycles_data->table_size++;
    }
  return 0;
}
/*_    . recurrences_get_object_count */
/* Given an object, list its count */
SIG_CHKARRAY(recurrences_get_object_count) =
{ REF_OPER(is_recurrence_table), K_ANY, };
DEF_SIMPLE_APPLICATIVE(p00a2, recurrences_get_object_count,T_NO_K,ground, "recurrences-get-object-count")
{
  WITH_2_ARGS(table, obj);
  WITH_PSYC_UNBOXED(kt_recurrence_table,table, T_RECURRENCES, 0);
  int seen_count = recurrences_get_seen_count(pdata, obj, 0);
  return mk_integer(seen_count);
}
/*_    . init_recurrence_table */
void
init_recurrence_table(kt_recurrence_table * p_cycles_data, int initial_size)
{
  p_cycles_data->objs   = initial_size ?
    GC_MALLOC(sizeof(pko) * initial_size) : 0;
  p_cycles_data->counts = initial_size ?
    GC_MALLOC(sizeof(int) * initial_size)     : 0;
  p_cycles_data->alloced_size = initial_size;
  p_cycles_data->table_size = 0;
}
/*_    . trace_tree_cycles */
static void
trace_tree_cycles
(pko tree, kt_recurrence_table * p_cycles_data)
{
  /* Special case for the "empty container", not because it's just a
     key but because "exploring" it does nothing. */
  if (tree == K_NIL)
    { return; }
  /* Maybe skip this object entirely */
  /* $$IMPROVE ME Parameterize this */
  switch(_get_type(tree))
    {
    case T_SYMBOL:
    case T_NUMBER:
      return;
    default:
      break;
    }
  if(recurrences_get_seen_count(p_cycles_data,tree, 1) != 0)
    { return; }

  /* Switch on tree type */
  switch(_get_type(tree))
    {
    case T_PAIR:
      {
#define _EXPLORE_FUNC(X) trace_tree_cycles(X, p_cycles_data)
	EXPLORE_v2(tree);
#undef _EXPLORE_FUNC
	break;
      }
    default:
      break;
      /* Done this exploration */
    }
  return;
}

/*_    . get_recurrences */
SIG_CHKARRAY(get_recurrences) = { K_ANY, };
DEF_SIMPLE_APPLICATIVE (ps0a1, get_recurrences,T_NO_K,ground, "get-recurrences")
{
  WITH_1_ARGS(tree);
  /* No reason to even start exploring non-containers */
  /* $$IMPROVE ME  Allow containers other than pairs */
  int explore_p = (_get_type(tree) == T_PAIR);
  ALLOC_BOX_PRESUME(kt_recurrence_table, T_RECURRENCES);
  init_recurrence_table(pdata, explore_p ? 8 : 0);
  if(explore_p)
    { trace_tree_cycles(tree,pdata); }
  return PTR2PKO(pbox);
}

/*_  . Reading */

/*_   , Making result objects */

/* make symbol or number atom from string */
static pko
mk_atom (klink * sc, char *q)
{
  char c, *p;
  int has_dec_point = 0;
  int has_fp_exp = 0;

#if USE_COLON_HOOK
  if ((p = strstr (q, "::")) != 0)
    {
      *p = 0;
      return mcons (sc->COLON_HOOK,
		   mcons (mcons (sc->QUOTE,
			       mcons (mk_atom (sc, p + 2), K_NIL)),
			 mcons (mk_symbol (strlwr (q)), K_NIL)));
    }
#endif

  p = q;
  c = *p++;
  if ((c == '+') || (c == '-'))
    {
      c = *p++;
      if (c == '.')
	{
	  has_dec_point = 1;
	  c = *p++;
	}
      if (!isdigit (c))
	{
	  return (mk_symbol (strlwr (q)));
	}
    }
  else if (c == '.')
    {
      has_dec_point = 1;
      c = *p++;
      if (!isdigit (c))
	{
	  return (mk_symbol (strlwr (q)));
	}
    }
  else if (!isdigit (c))
    {
      return (mk_symbol (strlwr (q)));
    }

  for (; (c = *p) != 0; ++p)
    {
      if (!isdigit (c))
	{
	  if (c == '.')
	    {
	      if (!has_dec_point)
		{
		  has_dec_point = 1;
		  continue;
		}
	    }
	  else if ((c == 'e') || (c == 'E'))
	    {
	      if (!has_fp_exp)
		{
		  has_dec_point = 1;	/* decimal point illegal
					   from now on */
		  p++;
		  if ((*p == '-') || (*p == '+') || isdigit (*p))
		    {
		      continue;
		    }
		}
	    }
	  return (mk_symbol (strlwr (q)));
	}
    }
  if (has_dec_point)
    {
      return mk_real (atof (q));
    }
  return (mk_integer (atol (q)));
}

/* make constant */
static pko
mk_sharp_const (char *name)
{
  long x;
  char tmp[STRBUFFSIZE];

  if (!strcmp (name, "t"))
    return (K_T);
  else if (!strcmp (name, "f"))
    return (K_F);
  else if (!strcmp (name, "ignore"))
    return (K_IGNORE);
  else if (!strcmp (name, "inert"))
    return (K_INERT);
  else if (*name == 'o')
    {				/* #o (octal) */
      snprintf (tmp, STRBUFFSIZE, "0%s", name + 1);
      sscanf (tmp, "%lo", &x);
      return (mk_integer (x));
    }
  else if (*name == 'd')
    {				/* #d (decimal) */
      sscanf (name + 1, "%ld", &x);
      return (mk_integer (x));
    }
  else if (*name == 'x')
    {				/* #x (hex) */
      snprintf (tmp, STRBUFFSIZE, "0x%s", name + 1);
      sscanf (tmp, "%lx", &x);
      return (mk_integer (x));
    }
  else if (*name == 'b')
    {				/* #b (binary) */
      x = binary_decode (name + 1);
      return (mk_integer (x));
    }
  else if (*name == '\\')
    {				/* #\w (character) */
      int c = 0;
      if (stricmp (name + 1, "space") == 0)
	{
	  c = ' ';
	}
      else if (stricmp (name + 1, "newline") == 0)
	{
	  c = '\n';
	}
      else if (stricmp (name + 1, "return") == 0)
	{
	  c = '\r';
	}
      else if (stricmp (name + 1, "tab") == 0)
	{
	  c = '\t';
	}
      else if (name[1] == 'x' && name[2] != 0)
	{
	  int c1 = 0;
	  if (sscanf (name + 2, "%x", &c1) == 1 && c1 < UCHAR_MAX)
	    {
	      c = c1;
	    }
	  else
	    {
	      return K_NIL;
	    }
#if USE_ASCII_NAMES
	}
      else if (is_ascii_name (name + 1, &c))
	{
	  /* nothing */
#endif
	}
      else if (name[2] == 0)
	{
	  c = name[1];
	}
      else
	{
	  return K_NIL;
	}
      return mk_character (c);
    }
  else
    return (K_NIL);
}

/*_   , Reading strings */
/* read characters up to delimiter, but cater to character constants */
static char *
readstr_upto (klink * sc, char *delim)
{
  port * pt = portvalue (klink_find_dyn_binding(sc,K_INPORT));

  char *p = sc->strbuff;

  while ((p - sc->strbuff < sizeof (sc->strbuff)) &&
	 !is_one_of (delim, (*p++ = inchar (pt))));

  if (p == sc->strbuff + 2 && p[-2] == '\\')
    {
      *p = 0;
    }
  else
    {
      backchar (pt, p[-1]);
      *--p = '\0';
    }
  return sc->strbuff;
}

/* skip white characters */
static INLINE int
skipspace (klink * sc)
{
  port * pt = portvalue (klink_find_dyn_binding(sc,K_INPORT));
  int c = 0;

  do
    { c = inchar (pt); }
  while (isspace (c));
  if (c != EOF)
    {
      backchar (pt, c);
      return 1;
    }
  else
    { return EOF; }
}

/*_   , Utilities */
/* check c is in chars */
static INLINE int
is_one_of (char *s, int c)
{
  if (c == EOF)
    return 1;
  while (*s)
    if (*s++ == c)
      return (1);
  return (0);
}

/*_   , Reading expressions */
/* read string expression "xxx...xxx" */
static pko
readstrexp (klink * sc)
{
  port * pt = portvalue (klink_find_dyn_binding(sc,K_INPORT));
  char *p = sc->strbuff;
  int c;
  int c1 = 0;
  enum
  { st_ok, st_bsl, st_x1, st_x2, st_oct1, st_oct2 } state = st_ok;

  for (;;)
    {
      c = inchar (pt);
      if (c == EOF || p - sc->strbuff > sizeof (sc->strbuff) - 1)
	{
	  return K_F;
	}
      switch (state)
	{
	case st_ok:
	  switch (c)
	    {
	    case '\\':
	      state = st_bsl;
	      break;
	    case '"':
	      *p = 0;
	      return mk_counted_string (sc->strbuff, p - sc->strbuff);
	    default:
	      *p++ = c;
	      break;
	    }
	  break;
	case st_bsl:
	  switch (c)
	    {
	    case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	      state = st_oct1;
	      c1 = c - '0';
	      break;
	    case 'x':
	    case 'X':
	      state = st_x1;
	      c1 = 0;
	      break;
	    case 'n':
	      *p++ = '\n';
	      state = st_ok;
	      break;
	    case 't':
	      *p++ = '\t';
	      state = st_ok;
	      break;
	    case 'r':
	      *p++ = '\r';
	      state = st_ok;
	      break;
	    case '"':
	      *p++ = '"';
	      state = st_ok;
	      break;
	    default:
	      *p++ = c;
	      state = st_ok;
	      break;
	    }
	  break;
	case st_x1:
	case st_x2:
	  c = toupper (c);
	  if (c >= '0' && c <= 'F')
	    {
	      if (c <= '9')
		{
		  c1 = (c1 << 4) + c - '0';
		}
	      else
		{
		  c1 = (c1 << 4) + c - 'A' + 10;
		}
	      if (state == st_x1)
		{
		  state = st_x2;
		}
	      else
		{
		  *p++ = c1;
		  state = st_ok;
		}
	    }
	  else
	    {
	      return K_F;
	    }
	  break;
	case st_oct1:
	case st_oct2:
	  if (c < '0' || c > '7')
	    {
	      *p++ = c1;
	      backchar (pt, c);
	      state = st_ok;
	    }
	  else
	    {
	      if (state == st_oct2 && c1 >= 32)
		return K_F;

	      c1 = (c1 << 3) + (c - '0');

	      if (state == st_oct1)
		state = st_oct2;
	      else
		{
		  *p++ = c1;
		  state = st_ok;
		}
	    }
	  break;

	}
    }
}


/* get token */
static int
token (klink * sc)
{
  port * pt = portvalue (klink_find_dyn_binding(sc,K_INPORT));
  int c;
  c = skipspace (sc);
  if (c == EOF)
    {
      return (TOK_EOF);
    }
  switch (c = inchar (pt))
    {
    case EOF:
      return (TOK_EOF);
    case '(':
      return (TOK_LPAREN);
    case ')':
      return (TOK_RPAREN);
    case '.':
      c = inchar (pt);
      if (is_one_of (" \n\t", c))
	{
	  return (TOK_DOT);
	}
      else
	{
	  backchar (pt, c);
	  backchar (pt, '.');
	  return TOK_ATOM;
	}
    case '\'':
      return (TOK_QUOTE);
    case ';':
      while ((c = inchar (pt)) != '\n' && c != EOF)
	{}

      if (c == EOF)
	{
	  return (TOK_EOF);
	}
      else
	{
	  return (token (sc));
	}
    case '"':
      return (TOK_DQUOTE);
    case '`':
      return (TOK_BQUOTE);
    case ',':
      if ((c = inchar (pt)) == '@')
	{
	  return (TOK_ATMARK);
	}
      else
	{
	  backchar (pt, c);
	  return (TOK_COMMA);
	}
    case '#':
      c = inchar (pt);
      if (c == '(')
	{
	  return (TOK_VEC);
	}
      else if (c == '!')
	{
	  while ((c = inchar (pt)) != '\n' && c != EOF)
	    {}

	  if (c == EOF)
	    {
	      return (TOK_EOF);
	    }
	  else
	    {
	      return (token (sc));
	    }
	}
      else
	{
	  backchar (pt, c);
	  /* $$UNHACKIFY ME!  This is a horrible hack. */
	  if (is_one_of (" itfodxb\\", c))
	    {
	      return TOK_SHARP_CONST;
	    }
	  else
	    {
	      return (TOK_SHARP);
	    }
	}
    default:
      backchar (pt, c);
      return (TOK_ATOM);
    }
}
/*_   , Nesting check */
/*_    . create_nesting_check */
void create_nesting_check(klink * sc)
{ klink_push_dyn_binding(sc,K_NEST_DEPTH,mk_integer(0)); }  
/*_    . nest_depth_ok_p */
int nest_depth_ok_p(klink * sc)
{
  pko nesting =
    klink_find_dyn_binding(sc,K_NEST_DEPTH);
  if(!nesting)
    { return 1; }
  return ivalue(nesting) == 0;
}
/*_    . change_nesting_depth */
void change_nesting_depth(klink * sc, signed int change)
{
  pko nesting =
    klink_find_dyn_binding(sc,K_NEST_DEPTH);
  add_to_ivalue(nesting,change);
}
/*_   , C-style entry points */

/*_    . kernel_read_internal */
/* The only reason that this is separate from kernel_read_sexp is that
   it gets a token, which kernel_read_sexp does almost always, except
   once when a caller tricks it with TOK_LPAREN, and once when
   kernel_read_list effectively puts back a token it didn't decode.  */
static
DEF_APPLICATIVE_W_DESTR (ps0a0, kernel_read_internal, K_NO_TYPE,0,ground, "read")
{
  token_t tok = token (sc);
  if (tok == TOK_EOF)
    {
      return K_EOF;
    }
  sc->tok = tok;
  create_nesting_check(sc);
  return kernel_read_sexp (sc);
}

/*_    . kernel_read_sexp */
DEF_CFUNC (ps0a0, kernel_read_sexp, K_NO_TYPE,0)
{
  switch (sc->tok)
    {
    case TOK_EOF:
      return K_EOF;
      /* NOTREACHED */
    case TOK_VEC:
      CONTIN_0 (vector, sc);

      /* fall through */
    case TOK_LPAREN:
      sc->tok = token (sc);
      if (sc->tok == TOK_RPAREN)
	{
	  return K_NIL;
	}
      else if (sc->tok == TOK_DOT)
	{
	  KERNEL_ERROR_0 (sc, "syntax error: illegal dot expression");
	}
      else
	{
	  change_nesting_depth(sc, 1);
	  CONTIN_1 (dcrry_2A01VLL, kernel_read_list, sc, K_NIL);
	  CONTIN_0 (kernel_read_sexp, sc);
	  return K_INERT;
	}
    case TOK_QUOTE:
      {
	pko pquote = REF_OPER(arg1);
	CONTIN_1 (dcrry_2A01VLL, val2val, sc, pquote);
      }
      sc->tok = token (sc);
      CONTIN_0 (kernel_read_sexp, sc);
      return K_INERT;

    case TOK_BQUOTE:
      sc->tok = token (sc);
      if (sc->tok == TOK_VEC)
	{
	  /* $$CLEAN ME Do this more cleanly than by changing tokens
	     to trick it.  Maybe factor the TOK_LPAREN treatment so we
	     can schedule it. */
	  klink_push_cont (sc, REF_OPER (kernel_treat_qquoted_vec));
	  sc->tok = TOK_LPAREN;
	  /* $$CLEANUP Seems like this could be combined with the part
	     afterwards */
	  CONTIN_0 (kernel_read_sexp, sc);
	  return K_INERT;
	}
      else
	{
	  /* Punt for now: Give quoted symbols rather than actual
	     operators. ,Similarly sc->UNQUOTE, sc->UNQUOTESP */
	  CONTIN_1 (dcrry_2A01VLL, val2val, sc, sc->QQUOTE);
	}

      CONTIN_0 (kernel_read_sexp, sc);
      return K_INERT;

    case TOK_COMMA:
      CONTIN_1 (dcrry_2A01VLL, val2val, sc, sc->UNQUOTE);
      sc->tok = token (sc);
      CONTIN_0 (kernel_read_sexp, sc);
      return K_INERT;
    case TOK_ATMARK:
      CONTIN_1 (dcrry_2A01VLL, val2val, sc, sc->UNQUOTESP);
      sc->tok = token (sc);
      CONTIN_0 (kernel_read_sexp, sc);
      return K_INERT;
    case TOK_ATOM:
      return mk_atom (sc, readstr_upto (sc, "();\t\n\r "));
    case TOK_DQUOTE:
      {
	pko x = readstrexp (sc);
	if (x == K_F)
	  {
	    KERNEL_ERROR_0 (sc, "Error reading string");
	  }
	setimmutable (x);
	return x;
      }
    case TOK_SHARP:
      {
	pko sharp_hook = sc->SHARP_HOOK;
	pko f =
	  is_symbol(sharp_hook)
	  ? find_slot_in_env (sc->envir, sharp_hook, 1)
	  : K_NIL;
	if (f == 0)
	  {
	    KERNEL_ERROR_0 (sc, "undefined sharp expression");
	  }
	else
	  {
	    pko form = mcons (slot_value_in_env (f), K_NIL);
	    return kernel_eval (sc, form, sc->envir);
	  }
      }
    case TOK_SHARP_CONST:
      {
	pko x = mk_sharp_const (readstr_upto (sc, "();\t\n\r "));
	if (x == K_NIL)
	  {
	    KERNEL_ERROR_0 (sc, "undefined sharp expression");
	  }
	else
	  {
	    return x;
	  }
      }
    default:
      KERNEL_ERROR_0 (sc, "syntax error: illegal token");
    }
}

/*_    . Read list */
/* $$IMPROVE ME  Use currying ops instead of accumulating by hand */
SIG_CHKARRAY(kernel_read_list) = { REF_OPER(is_finite_list),  K_ANY, };
DEF_SIMPLE_CFUNC (ps0a2, kernel_read_list,0)
{
  WITH_2_ARGS (old_accum,value);
  pko accum = mcons (value, old_accum);
  port * pt = portvalue (klink_find_dyn_binding(sc,K_INPORT));
  sc->tok = token (sc);
  if (sc->tok == TOK_EOF)
    {
      return (K_EOF);
    }
  else if (sc->tok == TOK_RPAREN)
    {
      /* $$RECONSIDER ME Why is this done?  To accept CR from user? */
      int c = inchar (pt);
      if (c != '\n')
	{
	  backchar (pt, c);
	}
      change_nesting_depth(sc, -1);
      return (unsafe_v2reverse_in_place (K_NIL, accum));
    }
  else if (sc->tok == TOK_DOT)
    {
      CONTIN_1 (dcrry_2A01VLL, kernel_treat_dotted_list, sc, accum);
      sc->tok = token (sc);
      CONTIN_0 (kernel_read_sexp, sc);
      return K_INERT;
    }
  else
    {
      CONTIN_1 (dcrry_2A01VLL, kernel_read_list, sc, accum);
      CONTIN_0 (kernel_read_sexp, sc);
      return K_INERT;
    }
}

/*_    . Treat end of dotted list */
static
DEF_CFUNC (ps0a2, kernel_treat_dotted_list, REF_DESTR(kernel_read_list),T_NO_K)
{
  WITH_2_ARGS(args,value);

  if (token (sc) != TOK_RPAREN)
    {
      KERNEL_ERROR_0 (sc, "syntax error: illegal dot expression");
    }
  else
    {
      change_nesting_depth(sc, -1);
      return (unsafe_v2reverse_in_place (value, args));
    }
}

/*_    . Treat quasiquoted vector */
static
DEF_CFUNC (ps0a1, kernel_treat_qquoted_vec, K_ANY,T_NO_K)
{
  pko value = arg1;
  /* $$IMPROVE ME Include vector applicative directly, not by applying
     symbol.  This does need to apply, though, so that backquote (now
     seeing a list) can be run on "value" first*/
  return (mcons (mk_symbol ("apply"),
		mcons (mk_symbol ("vector"),
		      mcons (mcons (sc->QQUOTE, mcons (value, K_NIL)),
			    K_NIL))));
}
/*_   , Loading files */
/*_    . load_from_port */
/* $$RETHINK ME  This soon need no longer be a cfunc */
SIG_CHKARRAY(load_from_port) = { REF_OPER(is_inport), REF_OPER(is_environment)};
DEF_SIMPLE_CFUNC(ps0a2,load_from_port,0)
{
  WITH_2_ARGS(inport,env);
  assert (is_port(inport));
  assert (is_environment(env));
  /* Print that we're loading (If there's an outport, and we may want
     to add a verbosity condition based on a dynamic variable) */
  pko the_outport = klink_find_dyn_binding(sc,K_OUTPORT);
  if(the_outport && (the_outport != K_NIL))
    {
      port * pt = portvalue(inport);
      if(pt->kind & port_file)
	{
	  const char *fname = pt->rep.stdio.filename;
	  if (!fname)
	    { fname = "<unknown>"; }
	  putstr(sc,"Loading ");
	  putstr(sc,fname);
	  putstr(sc,"\n");
	}
    }

  /* We will do the evals in ENV */
  sc->envir = env;
  klink_push_dyn_binding(sc,K_INPORT,inport);
  return kernel_rel(sc);   
}
/*_    . load */
/* $$OBSOLETE */
SIG_CHKARRAY(k_load_file) = { REF_OPER(is_string), };
DEF_SIMPLE_APPLICATIVE(ps0a1,k_load_file,0,ground, "load")
{
  WITH_1_ARGS(filename_ob);
  const char * filename = string_value(filename_ob);
  pko p = port_from_filename (filename, port_file | port_input);
  if (p == K_NIL)
    {
      KERNEL_ERROR_1(sc,"unable to open", filename_ob);
    }

  return load_from_port(sc,p,sc->envir); 
}
/*_    . get-module-from-port */
SIG_CHKARRAY(k_get_mod_fm_port) =
  { REF_OPER(is_port), REF_KEY(K_TYCH_OPTIONAL), REF_OPER(is_environment), };
DEF_SIMPLE_APPLICATIVE(ps0a2,k_get_mod_fm_port,0,ground, "get-module-from-port")
{
  WITH_2_ARGS(port, params);
  pko env = mk_std_environment();
  if(params != K_INERT)
    {
      assert(is_environment(params));
      kernel_define (env, mk_symbol ("module-parameters"), params);
    }
  /* Ultimately return that environment. */
  CONTIN_1R(dcrry_NdotALL,val2val,sc,env);
  return load_from_port(sc, port,env);
}

/*_  . Printing */
/*_   , Writing chars */
INTERFACE void
putstr (klink * sc, const char *s)
{
  pko the_outport = klink_find_dyn_binding(sc,K_OUTPORT);
  port *pt = portvalue (the_outport); /* $$MAKE ME SAFER - check for K_NIL */

  if (pt->kind & port_file)
    {
      fputs (s, pt->rep.stdio.file);
    }
  else
    {
      for (; *s; s++)
	{
	  if (pt->rep.string.curr != pt->rep.string.past_the_end)
	    {
	      *pt->rep.string.curr++ = *s;
	    }
	  else if (pt->kind & port_srfi6 && realloc_port_string (pt))
	    {
	      *pt->rep.string.curr++ = *s;
	    }
	}
    }
}

static void
putchars (klink * sc, const char *s, int len)
{
  pko the_outport = klink_find_dyn_binding(sc,K_OUTPORT);
  port *pt = portvalue (the_outport); /* $$MAKE ME SAFER - check for K_NIL */

  if (pt->kind & port_file)
    {
      fwrite (s, 1, len, pt->rep.stdio.file);
    }
  else
    {
      for (; len; len--)
	{
	  if (pt->rep.string.curr != pt->rep.string.past_the_end)
	    {
	      *pt->rep.string.curr++ = *s++;
	    }
	  else if (pt->kind & port_srfi6 && realloc_port_string (pt))
	    {
	      *pt->rep.string.curr++ = *s++;
	    }
	}
    }
}

INTERFACE void
putcharacter (klink * sc, int c)
{
  pko the_outport = klink_find_dyn_binding(sc,K_OUTPORT);
  port *pt = portvalue (the_outport); /* $$MAKE ME SAFER - check for K_NIL */

  if (pt->kind & port_file)
    {
      fputc (c, pt->rep.stdio.file);
    }
  else
    {
      if (pt->rep.string.curr != pt->rep.string.past_the_end)
	{
	  *pt->rep.string.curr++ = c;
	}
      else if (pt->kind & port_srfi6 && realloc_port_string (pt))
	{
	  *pt->rep.string.curr++ = c;
	}
    }
}

#define   ok_abbrev(x)   (is_pair(x) && cdr(x) == K_NIL)

static void
printslashstring (klink * sc, char *p, int len)
{
  int i;
  unsigned char *s = (unsigned char *) p;
  putcharacter (sc, '"');
  for (i = 0; i < len; i++)
    {
      if (*s == 0xff || *s == '"' || *s < ' ' || *s == '\\')
	{
	  putcharacter (sc, '\\');
	  switch (*s)
	    {
	    case '"':
	      putcharacter (sc, '"');
	      break;
	    case '\n':
	      putcharacter (sc, 'n');
	      break;
	    case '\t':
	      putcharacter (sc, 't');
	      break;
	    case '\r':
	      putcharacter (sc, 'r');
	      break;
	    case '\\':
	      putcharacter (sc, '\\');
	      break;
	    default:
	      {
		int d = *s / 16;
		putcharacter (sc, 'x');
		if (d < 10)
		  {
		    putcharacter (sc, d + '0');
		  }
		else
		  {
		    putcharacter (sc, d - 10 + 'A');
		  }
		d = *s % 16;
		if (d < 10)
		  {
		    putcharacter (sc, d + '0');
		  }
		else
		  {
		    putcharacter (sc, d - 10 + 'A');
		  }
	      }
	    }
	}
      else
	{
	  putcharacter (sc, *s);
	}
      s++;
    }
  putcharacter (sc, '"');
}

/*_   , Printing atoms */
static void
printatom (klink * sc, pko l)
{
  char *p;
  int len;
  atom2str (sc, l, &p, &len);
  putchars (sc, p, len);
}


/* Uses internal buffer unless string pointer is already available */
static void
atom2str (klink * sc, pko l, char **pp, int *plen)
{
  WITH_REPORTER(sc);
  char *p;
  pko p_escapes = klink_find_dyn_binding(sc,K_PRINT_FLAG);
  int escapes = (p_escapes == K_T) ? 1 : 0;

  if (l == K_NIL)
    {
      p = "()";
    }
  else if (l == K_T)
    {
      p = "#t";
    }
  else if (l == K_F)
    {
      p = "#f";
    }
  else if (l == K_INERT)
    {
      p = "#inert";
    }
  else if (l == K_IGNORE)
    {
      p = "#ignore";
    }
  else if (l == K_EOF)
    {
      p = "#<EOF>";
    }
  else if (is_port (l))
    {
      p = sc->strbuff;
      snprintf (p, STRBUFFSIZE, "#<PORT>");
    }
  else if (is_number (l))
    {
      p = sc->strbuff;
      if (num_is_integer (l))
	{
	  snprintf (p, STRBUFFSIZE, "%ld", ivalue (l));
	}
      else
	{
	  snprintf (p, STRBUFFSIZE, "%.10g", rvalue (l));
	}
    }
  else if (is_string (l))
    {
      if (!escapes)
	{
	  p = string_value (l);
	}
      else
	{			/* Hack, uses the fact that printing is needed */
	  *pp = sc->strbuff;
	  *plen = 0;
	  printslashstring (sc, string_value (l), string_len (l));
	  return;
	}
    }
  else if (is_character (l))
    {
      int c = charvalue (l);
      p = sc->strbuff;
      if (!escapes)
	{
	  p[0] = c;
	  p[1] = 0;
	}
      else
	{
	  switch (c)
	    {
	    case ' ':
	      snprintf (p, STRBUFFSIZE, "#\\space");
	      break;
	    case '\n':
	      snprintf (p, STRBUFFSIZE, "#\\newline");
	      break;
	    case '\r':
	      snprintf (p, STRBUFFSIZE, "#\\return");
	      break;
	    case '\t':
	      snprintf (p, STRBUFFSIZE, "#\\tab");
	      break;
	    default:
#if USE_ASCII_NAMES
	      if (c == 127)
		{
		  snprintf (p, STRBUFFSIZE, "#\\del");
		  break;
		}
	      else if (c < 32)
		{
		  snprintf (p, STRBUFFSIZE, "#\\%s", charnames[c]);
		  break;
		}
#else
	      if (c < 32)
		{
		  snprintf (p, STRBUFFSIZE, "#\\x%x", c);
		  break;
		  break;
		}
#endif
	      snprintf (p, STRBUFFSIZE, "#\\%c", c);
	      break;
	      break;
	    }
	}
    }
  else if (is_symbol (l))
    {
      p = symname (sc,l);
    }


  else if (is_environment (l))
    {
      p = "#<ENVIRONMENT>";
    }
  else if (is_continuation (l))
    {
      p = "#<CONTINUATION>";
    }
  else if (is_operative (l)
	   /* $$TRANSITIONAL  When these can be launched by
	      themselves, this check will be folded into is_operative */
	   || is_type (l, T_DESTRUCTURE)
	   || is_type (l, T_TYPECHECK)
	   || is_type (l, T_TYPEP))
    {
      /* $$TRANSITIONAL  This logic will move, probably into
	 k_print_special_and_balk_p, and become more general. */
      pko slot =
	print_lookup_unwraps ?
	reverse_find_slot_in_env_aux(print_lookup_unwraps,l) :
	0;
      if(slot)
	{
	  p = sc->strbuff;
	  snprintf (p, STRBUFFSIZE, ",(unwrap #,%s)", symname(0, car(slot)));
	}
      else
	{
	  pko slot =
	    print_lookup_to_xary ?
	    reverse_find_slot_in_env_aux(print_lookup_to_xary,l) :
	    0;
	  if(slot)
	    {
	      /* We don't say it's the tree-ary version, because the
		 tree-ary conversion is not exposed. */
	      p = symname(0, car(slot));
	    }
	  else
	    {
	      pko slot =
		all_builtins_env ?
		reverse_find_slot_in_env_aux(all_builtins_env, l) :
		0;
	      if(slot)
		{
		  p = symname(0, car(slot));
		}
	      else
		{ p = "#<OPERATIVE>"; }}
	}
    }
  else if (is_promise (l))
    {
      p = "#<PROMISE>";
    }
  else if (is_applicative (l))
    {
      p = "#<APPLICATIVE>";
    }
  else if (is_type (l, T_ENCAP))
    {
      p = "#<ENCAPSULATION>";
    }
  else if (is_type (l, T_KEY))
    {
      p = "#<KEY>";
    }
  else if (is_type (l, T_RECUR_TRACKER))
    {
      p = "#<RECURRENCE TRACKER>";
    }
  else if (is_type (l, T_RECURRENCES))
    {
      p = "#<RECURRENCE TABLE>";
    }
  else
    {
      p = sc->strbuff;
      snprintf (p, STRBUFFSIZE, "#<ERROR %d>", _get_type(l));
    }
  *pp = p;
  *plen = strlen (p);
}

/*_   , C-style entry points */
/*_    . Print sexp */
/*_     , kernel_print_sexp */
SIG_CHKARRAY(kernel_print_sexp) =
{ K_ANY, REF_KEY(K_TYCH_OPTIONAL), REF_OPER(is_environment), };
static
DEF_SIMPLE_CFUNC (ps0a2, kernel_print_sexp,0)
{
  WITH_2_ARGS(sexp, lookup_env);
  pko recurrences = get_recurrences(sc, sexp);
  pko tracker = recurrences_to_recur_tracker(recurrences);
  /* $$IMPROVE ME Default to an environment that knows sharp
     constants */
  return kernel_print_sexp_aux
    (sc, sexp,
     tracker,
     ((lookup_env == K_INERT) ? ground_env : lookup_env));
}
/*_     , k_print_special_and_balk_p */
/* Possibly print a replacement or prefix.  Return 1 if we should now
   skip printing sexp (Because it's shared), 0 otherwise. */
static int
k_print_special_and_balk_p
(klink * sc, pko tracker, pko lookup_env, pko sexp)
{
  WITH_REPORTER(0);
  /* If this object is directly known to printer, print its symbol. */
  if(lookup_env != K_NIL)
    {
      pko slot = reverse_find_slot_in_env_aux(lookup_env,sexp);
      if(slot)
	{
	  putstr (sc, "#,");	/* Reader is to convert the symbol */
	  printatom (sc, car(slot));
	  return 1;
	}
    }
  if(tracker == K_NIL)
    { return 0; }

  /* $$IMPROVE ME Parameterize this and share that parameterization
     with get_recurrences */
  switch(_get_type(sexp))
    {
    case T_SYMBOL:
    case T_NUMBER:
      return 0;
    default:
      break;
    }

  WITH_PSYC_UNBOXED(kt_recur_tracker,tracker, T_RECUR_TRACKER, sc);
  int index = objtable_get_index(pdata->objs,pdata->table_size,sexp);
  if(index < 0) { return 0; }
  recur_entry * slot = &pdata->entries[index];
  if(slot->count <= 1) { return 0; }

  if(slot->seen_in_walk)
    {
      char *p = sc->strbuff;
      snprintf (p, STRBUFFSIZE, "#%d", slot->index_in_walk);
      putchars (sc, p, strlen (p));
      return 1;  	/* Skip printing the object */
    }
  else
    {
      slot->seen_in_walk = 1;
      slot->index_in_walk = pdata->current_index;
      pdata->current_index++;
      char *p = sc->strbuff;
      snprintf (p, STRBUFFSIZE, "#%d=", slot->index_in_walk);
      putchars (sc, p, strlen (p));
      return 0;		/* Still should print the object */
    }
}
/*_     , kernel_print_sexp_aux */
SIG_CHKARRAY(kernel_print_sexp_aux) =
{ K_ANY, REF_OPER(is_recur_tracker), REF_OPER(is_environment), };
static
DEF_SIMPLE_CFUNC (ps0a3, kernel_print_sexp_aux,0)
{
  WITH_3_ARGS(sexp, recur_tracker, lookup_env);
  WITH_REPORTER(0);
  if(k_print_special_and_balk_p(sc, recur_tracker, lookup_env, sexp))
    { return K_INERT; }
  if (is_vector (sexp))
    {
      putstr (sc, "#(");
      CONTIN_4 (dcrry_4dotALL, kernel_print_vec_from, sc, sexp,
		mk_integer (0), recur_tracker, lookup_env);
      return K_INERT;
    }
  else if (!is_pair (sexp))
    {
      printatom (sc, sexp);
      return K_INERT;
    }
  /* $$FIX ME Recognize quote etc.

     That is hard since the quote operative is not currently defined
     as such and we no longer have syntax.
   */
  else if (car (sexp) == sc->QUOTE && ok_abbrev (cdr (sexp)))
    {
      putstr (sc, "'");
      return kernel_print_sexp_aux (sc, cadr (sexp), recur_tracker, lookup_env);
    }
  else if (car (sexp) == sc->QQUOTE && ok_abbrev (cdr (sexp)))
    {
      putstr (sc, "`");
      return kernel_print_sexp_aux (sc, cadr (sexp), recur_tracker, lookup_env);
    }
  else if (car (sexp) == sc->UNQUOTE && ok_abbrev (cdr (sexp)))
    {
      putstr (sc, ",");
      return kernel_print_sexp_aux (sc, cadr (sexp), recur_tracker, lookup_env);
    }
  else if (car (sexp) == sc->UNQUOTESP && ok_abbrev (cdr (sexp)))
    {
      putstr (sc, ",@");
      return kernel_print_sexp_aux (sc, cadr (sexp), recur_tracker, lookup_env);
    }
  else
    {
      putstr (sc, "(");
      CONTIN_0_RAW(REF_OBJ(k_print_terminate_list), sc);
      CONTIN_3 (dcrry_2dotALL, kernel_print_list, sc, cdr (sexp), recur_tracker, lookup_env);
      return kernel_print_sexp_aux (sc, car (sexp), recur_tracker, lookup_env);
    }
}
/*_     , print_value */
DEF_BOXED_CURRIED(print_value,
		  dcrry_1VLL,
		  REF_KEY(K_NIL),
		  REF_OPER (kernel_print_sexp));
/*_    . k_print_string */
SIG_CHKARRAY(k_print_string) = { REF_OPER(is_string), };
static
DEF_SIMPLE_CFUNC (ps0a1, k_print_string,T_NO_K)
{
  WITH_1_ARGS(str);
  putstr (sc, string_value(str));
  return K_INERT;  
}
/*_    . k_print_terminate_list */
/* $$RETHINK ME  This may be the long way to do it. */
static
BOX_OF(kt_string) _k_string_rpar =
{ T_STRING | T_IMMUTABLE,
  { ")", sizeof(")"), },
};
static
BOX_OF(kt_vec2) _k_list_string_rpar =
{ T_PAIR | T_IMMUTABLE,
  { REF_OBJ(_k_string_rpar), REF_KEY(K_NIL)}
};
static
DEF_BOXED_CURRIED(k_print_terminate_list,
		  dcrry_1dotALL,
		  REF_OBJ(_k_list_string_rpar),
		  REF_OPER(k_print_string));
/*_    . k_newline */
RGSTR(ground, "newline", REF_OBJ(k_newline))
static
BOX_OF(kt_string) _k_string_newline =
{ T_STRING | T_IMMUTABLE,
  { "\n", sizeof("\n"), }, };
static
BOX_OF(kt_vec2) _k_list_string_newline =
{ T_PAIR | T_IMMUTABLE,
  { REF_OBJ(_k_string_newline), REF_KEY(K_NIL)}
};
static
DEF_BOXED_CURRIED(k_newline,
		  dcrry_1dotALL,
		  REF_OBJ(_k_list_string_newline),
		  REF_OPER(k_print_string));

/*_    .  kernel_print_list */
static
DEF_CFUNC (ps0a3, kernel_print_list, REF_DESTR(kernel_print_sexp_aux),0)
{
  WITH_REPORTER(0);
  WITH_3_ARGS(sexp, recur_tracker, lookup_env);
  if(is_pair (sexp))  { putstr (sc, " "); }
  else if (sexp != K_NIL) { putstr (sc, " . "); }
  else { }
  
  if(k_print_special_and_balk_p(sc, recur_tracker, lookup_env, sexp))
    { return K_INERT; }
  if (is_pair (sexp))
    {
      CONTIN_3 (dcrry_3dotALL, kernel_print_list, sc, cdr (sexp), recur_tracker, lookup_env);
      return kernel_print_sexp_aux (sc, car (sexp), recur_tracker, lookup_env);
    }
  if (is_vector (sexp))
    {
      /* $$RETHINK ME What does this even print? */
      CONTIN_3 (dcrry_3dotALL, kernel_print_list, sc, K_NIL, recur_tracker, lookup_env);
      return kernel_print_sexp_aux (sc, sexp, recur_tracker, lookup_env);
    }

  if (sexp != K_NIL)
    {
      printatom (sc, sexp);
    }
  return K_INERT;
}


/*_    .  kernel_print_vec_from */
SIG_CHKARRAY(kernel_print_vec_from) =
{ K_ANY,
  REF_OPER(is_integer),
  REF_OPER(is_recur_tracker),
  REF_OPER(is_environment), };
DEF_SIMPLE_CFUNC (ps0a4, kernel_print_vec_from,0)
{
  WITH_4_ARGS(vec,k_i, recur_tracker, lookup_env);
  int i = ivalue (k_i);
  int len = vector_len (vec);
  if (i == len)
    {
      putstr (sc, ")");
      return K_INERT;
    }
  else
    {
      pko elem = vector_elem (vec, i);
      set_ivalue (k_i, i + 1);
      CONTIN_4 (dcrry_4dotALL, kernel_print_vec_from, sc, vec, arg2, recur_tracker, lookup_env);  
      putstr (sc, " ");
      return kernel_print_sexp_aux (sc, elem, recur_tracker, lookup_env);
    }
}
/*_   , Kernel entry points */
/*_    . write */
DEF_APPLICATIVE_W_DESTR(ps0a1,k_write,K_ANY_SINGLETON,0,ground, "write")
{
  WITH_1_ARGS(p);
  klink_push_dyn_binding(sc,K_PRINT_FLAG,K_T);
  return kernel_print_sexp(sc,p,K_INERT);
}

/*_    . display */
DEF_APPLICATIVE_W_DESTR(ps0a1,k_display,K_ANY_SINGLETON,0,ground, "display")
{
  WITH_1_ARGS(p);
  klink_push_dyn_binding(sc,K_PRINT_FLAG,K_F);
  return kernel_print_sexp(sc,p,K_INERT);
}

/*_   , Tracing */
/*_    . tracing_say */
/* $$TRANSITIONAL  Until we have actual trace hook */
SIG_CHKARRAY(tracing_say) = { REF_OPER(is_string),  K_ANY, };
DEF_SIMPLE_CFUNC (ps0a2, tracing_say,T_NO_K)
{
  WITH_2_ARGS(k_string, value);
  if (sc->tracing)
    {
      putstr (sc, string_value(k_string));
    }
  return value;
}


/*_  . Equivalence */
/*_   , Equivalence of atoms */
SIG_CHKARRAY(eqv) = { K_ANY, K_ANY, };
DEF_SIMPLE_APPLICATIVE(b00a2,eqv,T_NO_K,simple,"equal?/2-atom-atom")
{
  WITH_2_ARGS(a,b);

  if (is_string (a))
    {
      if (is_string (b))
	{
	  const char * a_str = string_value (a);
	  const char * b_str = string_value (b);
	  if (a_str == b_str) { return 1; }
	  return !strcmp(a_str, b_str);
	}
      else
	{ return (0); }
    }
  else if (is_number (a))
    {
      if (is_number (b))
	{
	  if (num_is_integer (a) == num_is_integer (b))
	    return num_eq (nvalue (a), nvalue (b));
	}
      return (0);
    }
  else if (is_character (a))
    {
      if (is_character (b))
	return charvalue (a) == charvalue (b);
      else
	return (0);
    }
  else if (is_port (a))
    {
      if (is_port (b))
	return a == b;
      else
	return (0);
    }
  else
    {
      return (a == b);
    }
}
/*_   , Equivalence of containers */

/*_  . Hash function */
#if !defined(USE_ALIST_ENV) || !defined(USE_OBJECT_LIST)

static int
hash_fn (const char *key, int table_size)
{
  unsigned int hashed = 0;
  const char *c;
  int bits_per_int = sizeof (unsigned int) * 8;

  for (c = key; *c; c++)
    {
      /* letters have about 5 bits in them */
      hashed = (hashed << 5) | (hashed >> (bits_per_int - 5));
      hashed ^= *c;
    }
  return hashed % table_size;
}
#endif

/* Quick and dirty hash function for pointers */
static int
ptr_hash_fn(void * ptr, int table_size)
{ return (long)ptr % table_size; }

/*_  . binder/accessor maker */
pko make_keyed_variable(pko gen_binder, pko gen_accessor)
{
  /* Make a unique key object */
  pko key = mk_void();
  pko binder = wrap (mk_curried
			 (dcrry_3A01dotVLL,
			  LIST1(key),
			  gen_binder));
  pko accessor = wrap (mk_curried
			   (dcrry_1A01,
			    LIST1(key),
			    gen_accessor));
  /* Curry and wrap the two things. */
  return LIST2 (binder, accessor);
}

/*_  . Environment implementation */
/*_   , New-style environment objects */

/*_    . Types */

/*   T_ENV_FRAME is a vec2 used as a pair in the env tree.  It
     indicates a frame boundary. 
*/
/*   T_ENV_PAIR is another vec2 used as a pair in the env tree.  It
     indicates no frame boundary.
*/

/* Other types are (hackishly) still shared with the vanilla types:

   A vector is interpeted as a hash table vector that is "as if" it
   were a list of T_ENV_PAIR.  Each element is an alist of bindings.
   It can only hold symbol bindings, not keyed bindings, because we
   can't hash keyed bindings.

   A pair is interpreted as a binding of something and value.  That
   something can be either a symbol or a key (void object).  It is
   held directly by an T_ENV_FRAME or T_ENV_PAIR (or "as if", by the
   alists of a hash table vector).

 */

/*_    . Object functions */

DEF_T_PRED (is_environment, T_ENV_FRAME,ground,"environment?/o1");

/*_   , New environment implementation */

#ifndef USE_ALIST_ENV
static pko
find_slot_in_env_vector (pko eobj, pko hdl)
{
  for (; eobj != K_NIL; eobj = unsafe_v2cdr (eobj))
    {
      assert (is_pair (eobj));
      pko slot = unsafe_v2car (eobj);
      assert (is_pair (slot));
      if (unsafe_v2car (slot) == hdl)
	{
	  return slot;
	}
    }
  return 0;
}

static pko
reverse_find_slot_in_env_vector (pko eobj, pko value)
{
  for (; eobj != K_NIL; eobj = unsafe_v2cdr (eobj))
    {
      assert (is_pair (eobj));
      pko slot = unsafe_v2car (eobj);
      assert (is_pair (slot));
      if (unsafe_v2cdr (slot) == value)
	{
	  return slot;
	}
    }
  return 0;
}
#endif

/*
 * If we're using vectors, each frame of the environment may be a hash
 * table: a vector of alists hashed by variable name.  In practice, we
 * use a vector only for the initial frame; subsequent frames are too
 * small and transient for the lookup speed to out-weigh the cost of
 * making a new vector.
 */
static INLINE pko
make_new_frame(pko old_env)
{
  pko new_frame;
#ifndef USE_ALIST_ENV
  /* $$IMPROVE ME  Make a better test for whether to make vector. */
  /* The interaction-environment has about 300 variables in it. */
  if (old_env == K_NIL)
    {
      new_frame = mk_vector (461, K_NIL);
    }
  else
#endif  
    {
      new_frame = K_NIL;
    }

  return v2cons (T_ENV_FRAME, new_frame, old_env);
}

static INLINE void
new_slot_spec_in_env (pko env, pko variable, pko value)
{
  assert(is_environment(env));
  assert(is_symbol(variable));
  pko slot = mcons (variable, value);
  pko car_env = unsafe_v2car (env);
#ifndef USE_ALIST_ENV
  if (is_vector (car_env))
    {
      int location = hash_fn (symname (0,variable), vector_len (car_env));

      set_vector_elem (car_env, location,
		       cons (slot,
			     vector_elem (car_env, location)));
    }
  else
#endif
    {
      pko new_list = v2cons (T_ENV_PAIR, slot, car_env);
      unsafe_v2set_car (env, new_list);
    }
}

enum env_frame_search_restriction
{
  env_fsr_all,
  env_fsr_only_coming_frame,
  env_fsr_only_this_frame,
};

/* This explores a tree of bindings, punctuated by frames past which
   we sometimes don't search. */
static pko
find_slot_in_env_aux (pko eobj, pko hdl, int restr)
{
  if(eobj == K_NIL)
    { return 0; }
  _kt_tag type = _get_type (eobj);
  switch(type)
    {
      /* We have a slot (Which for now is just a pair) */
    case T_PAIR:
      if(unsafe_v2car (eobj) == hdl)
	{ return eobj; }
      else
	{ return 0; }
#ifndef USE_ALIST_ENV
    case T_VECTOR:
      {
	/* Only for symbols. */
	if(!is_symbol (hdl)) { return 0; }
	int location = hash_fn (symname (0,hdl), vector_len (eobj));
	pko el = vector_elem (eobj, location);
	return find_slot_in_env_vector (el, hdl);
      }      
#endif
      /* We have some sort of env pair */
    case T_ENV_FRAME:
      /* Check whether we should keep looking. */
      switch(restr)
	{
	case env_fsr_all:
	  break;
	case env_fsr_only_coming_frame:
	  restr = env_fsr_only_this_frame;
	  break;
	case env_fsr_only_this_frame:
	  return 0; 
	default:
	  errx (3,
	       "find_slot_in_env_aux: Bad restriction enum: %d", restr);  
	}
      /* Fallthru */
    case T_ENV_PAIR:
      {
	/* Explore car before cdr */
	pko found = find_slot_in_env_aux (unsafe_v2car (eobj), hdl, restr);
	if(found) { return found; }
	return find_slot_in_env_aux (unsafe_v2cdr (eobj),hdl,restr);
      }
    default:
      /* No other type should be found */
      errx (3,
	   "find_slot_in_env_aux: Bad type: %d", type);  
      return 0;			/* NOTREACHED */
    }
}

static pko
find_slot_in_env (pko env, pko hdl, int all)
{
  assert(is_environment(env));
  enum env_frame_search_restriction restr =
    all ? env_fsr_all : env_fsr_only_coming_frame;
  return find_slot_in_env_aux(env,hdl,restr);
}
/*_   , Reverse find-slot */
/*_    . env_confirm_slot */
static int
env_confirm_slot(pko env, pko slot)
{
  assert(is_pair(slot));
  return
    (find_slot_in_env_aux(env,unsafe_v2car(slot),env_fsr_all) == slot);
}  
/*_    . reverse_find_slot_in_env_aux2 */
static pko
reverse_find_slot_in_env_aux2(pko env, pko eobj, pko value)
{
  if(eobj == K_NIL)
    { return 0; }
  _kt_tag type = _get_type (eobj);
  switch(type)
    {
      /* We have a slot (Which for now is just a pair) */
    case T_PAIR:
      if((unsafe_v2cdr (eobj) == value)
	 && env_confirm_slot(env, eobj))
	{ return eobj; }
      else
	{ return 0; }
#ifndef USE_ALIST_ENV
    case T_VECTOR:
      {
	/* $$IMPROVE ME Create a reverse-lookup vector if we come here
	   and there is none.  */
	int i;
	for(i = 0; i < vector_len (eobj); ++i)
	  {
	    pko slot = reverse_find_slot_in_env_vector(vector_elem (eobj, i), value);
	    if(slot &&
	       env_confirm_slot(env, slot))
	      { return slot; }
	  }
	return 0;
      }      
#endif
      /* We have some sort of env pair */
    case T_ENV_FRAME:
      /* Fallthru */
    case T_ENV_PAIR:
      {
	/* Explore car before cdr */
	pko found =
	  reverse_find_slot_in_env_aux2 (env, unsafe_v2car (eobj), value);
	if(found && env_confirm_slot(env, found))
	  { return found; }
	found =
	  reverse_find_slot_in_env_aux2 (env, unsafe_v2cdr (eobj), value);
	if(found && env_confirm_slot(env, found))
	  { return found; }
	return 0;
      }
    default:
      /* No other type should be found */
      errx (3,
	   "reverse_find_slot_in_env_aux2: Bad type: %d", type);  
      return 0;			/* NOTREACHED */
    }
}

/*_    . reverse_find_slot_in_env_aux */
static pko
reverse_find_slot_in_env_aux (pko env, pko value)
{
  assert(is_environment(env));
  return reverse_find_slot_in_env_aux2(env, env, value);
}
  
/*_    . Entry point */
/* Exposed for testing */
/* NB, args are in different order than in the helpers */
SIG_CHKARRAY(reverse_find_slot_in_env) =
{ K_ANY, REF_OPER(is_environment), };
DEF_SIMPLE_APPLICATIVE (ps0a2, reverse_find_slot_in_env,T_NO_K,unsafe, "reverse-lookup")
{
  WITH_2_ARGS(value,env);
  WITH_REPORTER(0);
  pko slot = reverse_find_slot_in_env_aux(env, value);
  if(slot) { return car(slot); }
  else
    {
      KERNEL_ERROR_0(sc, "reverse_find_slot_in_env: No match");
    }
}

/*_    . reverse-binds?/2 */
/* $$IMPROVE ME  Maybe combine these */
DEF_APPLICATIVE_W_DESTR(b00a2,reverse_binds_p,
			REF_DESTR(reverse_find_slot_in_env),
			T_NO_K,simple,"reverse-binds?/2")
{
  WITH_2_ARGS(value,env);
  return reverse_find_slot_in_env_aux(env, value) ? 1 : 0;
}
/*_   , Shared functions */

static INLINE void
new_frame_in_env (klink * sc, pko old_env)
{
  sc->envir = make_new_frame (old_env);
}

static INLINE void
set_slot_in_env (pko slot, pko value)
{
  assert (is_pair (slot));
  set_cdr (0, slot, value);
}

static INLINE pko
slot_value_in_env (pko slot)
{
  WITH_REPORTER(0);
  assert (is_pair (slot));
  return cdr (slot);
}

/*_   , Keyed static bindings */
/*_    . Support */
/*_     , Making them */
/* Make a new frame containing just the one keyed static variable. */
static INLINE pko
env_plus_keyed_var (pko key, pko value, pko old_env)
{
  pko slot = cons (key, value);
  return v2cons (T_ENV_FRAME, slot, old_env);
}
/*_     , Finding them */
/* find_slot_in_env works for this too. */
/*_    . Interface */
/*_     , Binder */
SIG_CHKARRAY(klink_ksb_binder) =
{ REF_OPER(is_key),  K_ANY, REF_OPER(is_environment), };
DEF_SIMPLE_CFUNC(ps0a3,klink_ksb_binder,T_NO_K)
{
  WITH_3_ARGS(key, value, env);
  /* Check that env is in fact a environment. */
  if(!is_environment(env))
    {
      KERNEL_ERROR_1(sc,
		     "klink_ksb_binder: Arg 2 must be an environment: ",
		     env);
    }
  /* Return a new environment with just that binding. */
  return env_plus_keyed_var(key, value, env);
}

/*_     , Accessor */
SIG_CHKARRAY(klink_ksb_accessor) =
{ REF_OPER(is_key), };
DEF_SIMPLE_CFUNC(ps0a1,klink_ksb_accessor,T_NO_K)
{
  WITH_1_ARGS(key);
  pko value = find_slot_in_env(sc->envir,key,1);
  if(!value)
    {
      KERNEL_ERROR_0(sc, "klink_ksb_accessor: No binding found");
    }
  
  return slot_value_in_env (value);
}

/*_     , make_keyed_static_variable */
RGSTR(ground, "make-keyed-static-variable", REF_OPER(make_keyed_static_variable))
DEF_CFUNC(p00a0, make_keyed_static_variable,K_NO_TYPE,T_NO_K)
{
  return make_keyed_variable(
			     REF_OPER(klink_ksb_binder),
			     REF_OPER (klink_ksb_accessor));
}
/*_   , Building environments */
/* Argobject is checked internally, so K_ANY */
DEF_APPLICATIVE_W_DESTR(ps0a1,make_environment,K_ANY,T_NO_K,ground, "make-environment")
{
  WITH_1_ARGS(parents);
  /* $$IMPROVE ME Factor this so we only call get_list_metrics_aux
     once on this object. */
  int4 metrics;
  get_list_metrics_aux(parents, metrics);
  pko typecheck = REF_OPER(is_environment);
  /* This will reject dotted lists */
  if(!typecheck_repeat(sc,parents,&typecheck,1,1))
    {
      KERNEL_ERROR_0 (sc, "make_environment: argobject must be a list of environments");
    }

  /* Collect the parent environments. */
  int i;
  pko rv_par_list = K_NIL;
  for(i = 0; i < metrics[lm_num_pairs]; ++i, parents = pair_cdr(0, parents))
    {
      pko pare = pair_car(0, parents);
      rv_par_list = v2cons (T_ENV_PAIR, pare, rv_par_list);
    }
  
  /* Reverse the list in place. */
  pko par_list;

  par_list = unsafe_v2reverse_in_place(K_NIL, rv_par_list);

  /* $$IMPROVE ME Check for redundant environments and skip them.
     Check only *previous* environments, because we still need to
     search correctly.  When recurrences walks environments too, we
     can use that to find them. */
  /* $$IMPROVE ME Add to environment information to block rechecks. */

  /* Return a new environment with all of those as parents. */
  return make_new_frame(par_list);
}
/*_   , bindsp_1 */
RGSTR(simple,"$binds?/2", REF_OPER(bindsp_1))
SIG_CHKARRAY(bindsp_1) =
{ REF_OPER(is_environment), REF_OPER(is_symbol), };
DEF_SIMPLE_CFUNC(bs0a2,bindsp_1,T_NO_K)
{
  WITH_2_ARGS(env, sym);
  return find_slot_in_env(env, sym, 1) ? 1 : 0;
}
/*_   , find-binding */
DEF_APPLICATIVE_W_DESTR(ps0a2,find_binding,REF_DESTR(bindsp_1),T_NO_K,ground,"find-binding")
{
  WITH_2_ARGS(env, sym);
  pko binding = find_slot_in_env(env, sym, 1);
  if(binding)
    {
      return cons(K_T,slot_value_in_env (binding));
    }
  else
    {
      return cons(K_F,K_INERT);
    }
}

/*_  . Stack */
/*_   , Enumerations */
enum klink_stack_cell_types
{
  ksct_invalid,
  ksct_frame,
  ksct_binding,
  ksct_entry_guards,
  ksct_exit_guards,
  ksct_profile,
  ksct_args,
  ksct_arg_barrier,		/* Barrier to propagating pseudo-env. */
};
/*_   , Structs */

struct dump_stack_frame
{
  pko envir;
  pko ff;
};
struct stack_binding
{
  pko key;
  pko value;
};

struct stack_guards
{
  pko guards;
  pko envir;
};

struct stack_profiling
{
  pko ff;
  int initial_count;
  int returned_p;
};

struct stack_arg
{
  pko vec;
  int frame_depth;
};

typedef struct dump_stack_frame_cell
{
  enum klink_stack_cell_types type;
  _kt_spagstack next;
  union
  {
    struct dump_stack_frame frame;
    struct stack_binding    binding;
    struct stack_guards     guards;
    struct stack_profiling  profiling;
    struct stack_arg        pseudoenv;
  } data;
} dump_stack_frame_cell;

/*_   , Initialize */

static INLINE void
dump_stack_initialize (klink * sc)
{
  sc->dump = 0;
}

static INLINE int
stack_empty (klink * sc)
{ return sc->dump == 0; }

/*_   , Frames */
static int
klink_pop_cont (klink * sc)
{
  _kt_spagstack rv_pseudoenvs = 0;
  
  /* Always return frame, which sc->dump will be set to. */
  /* for(frame = sc->dump; frame != 0; frame = frame->next) */
  while(1)
    {
      if (sc->dump == 0)
	{
	  return 0;
	}
      else
	{
	  const _kt_spagstack frame = sc->dump;
	  if(frame->type == ksct_frame)
	    {
	      const struct dump_stack_frame *pdata = &frame->data.frame;
	      sc->next_func    = pdata->ff;
	      sc->envir        = pdata->envir;

	      _kt_spagstack final_frame = frame->next;

	      /* Add the collected pseudo-env elements */
	      while(rv_pseudoenvs)
		{
		  _kt_spagstack el = rv_pseudoenvs;
		  _kt_spagstack new_top = rv_pseudoenvs->next;
		  el->next = final_frame;
		  final_frame = el;
		  rv_pseudoenvs = new_top;
		}
	      sc->dump = final_frame;
	      return 1;
	    }
#ifdef PROFILING
	  else
	  if(frame->type == ksct_profile)
	    {
	      struct stack_profiling * pdata = &frame->data.profiling;
	      k_profiling_done_frame(sc,pdata);
	      sc->dump = frame->next;
	    }
#endif
	  else if( frame->type == ksct_args )
	    {
	      struct stack_arg * old_pe = &frame->data.pseudoenv;
	      if(old_pe->frame_depth > 0)
		{
		  /* Make a copy, to be re-added lower down */
		  _kt_spagstack new_pseudoenv =
		    (_kt_spagstack)
		    GC_MALLOC (sizeof (dump_stack_frame_cell));
		  struct stack_arg * new_pe = &new_pseudoenv->data.pseudoenv;
		  new_pe->vec         = old_pe->vec;
		  new_pe->frame_depth = old_pe->frame_depth - 1;
	      
		  new_pseudoenv->type = ksct_args;
		  new_pseudoenv->next = rv_pseudoenvs;
		  rv_pseudoenvs = new_pseudoenv;
		}

	      sc->dump = frame->next;
	    }
	  else if( frame->type == ksct_arg_barrier )
	    {
	      errx( 0, "Not allowed");
	      rv_pseudoenvs = 0;
	      sc->dump = frame->next;
	    }
	  else
	    {
	      sc->dump = frame->next;
	    }
	}
    }
}

static _kt_spagstack 
klink_push_cont_aux
(_kt_spagstack old_frame, pko ff, pko env)
{
  _kt_spagstack frame =
    (_kt_spagstack)
    GC_MALLOC (sizeof (dump_stack_frame_cell));
  struct dump_stack_frame * pdata = &frame->data.frame;
  pdata->ff           = ff;
  pdata->envir        = env;
  
  frame->type = ksct_frame;
  frame->next = old_frame;
  return frame;
}

/* $$MOVE ME */
static void
klink_push_cont (klink * sc, pko ff)
{ sc->dump = klink_push_cont_aux(sc->dump, ff, sc->envir); }

/*_   , Dynamic bindings */

/* We do not pop dynamic bindings, only frames. */
/* We deal with dynamic bindings in the context of the interpreter so
   that in the future we can cache them. */
static void
klink_push_dyn_binding (klink * sc, pko key, pko value)
{
  _kt_spagstack frame =
    (_kt_spagstack)
    GC_MALLOC (sizeof (dump_stack_frame_cell));
  struct stack_binding *pdata = &frame->data.binding;

  pdata->key           = key;
  pdata->value        = value;
  
  frame->type = ksct_binding;
  frame->next = sc->dump;
  sc->dump = frame;
}


static pko
klink_find_dyn_binding(klink * sc, pko key)
{
  _kt_spagstack frame = sc->dump;
  while(1)
    {
      if (frame == 0)
	{
	  return 0;
	}
      else
	{
	  if(frame->type == ksct_binding)
	    {
	      const struct stack_binding *pdata = &frame->data.binding;
	      if(pdata->key == key)
		{ return pdata->value; }
	    }
	  frame = frame->next;
	}
    }
}
/*_   , Guards */
/*_    . klink_push_guards */
static _kt_spagstack 
klink_push_guards
(_kt_spagstack old_frame, pko guards, pko envir, int exit) 
{
  _kt_spagstack frame =
    (_kt_spagstack)
    GC_MALLOC (sizeof (dump_stack_frame_cell));
  struct stack_guards * pdata = &frame->data.guards;
  pdata->guards = guards;
  pdata->envir  = envir;
  
  frame->type = exit ? ksct_exit_guards : ksct_entry_guards;
  frame->next = old_frame;
  return frame;
}
/*_    . get_guards_lo1st */
/* Get a list of guard entries, root-most on top. */
static pko
get_guards_lo1st(_kt_spagstack frame)
{
  pko list = K_NIL;
  for(; frame != 0; frame = frame->next)
    {
      if((frame->type == ksct_entry_guards) ||
	 (frame->type == ksct_exit_guards))
	{
	  list = cons(mk_continuation(frame), list);
	}
    }
  
  return list;
}  
/*_   , Args */
/*_    . Misc */
/*_     , set_nth_arg */
#if 0
/* Set the nth arg */
/* Unused, probably for a while, probably will never be used in this
   form. */
int
set_nth_arg(klink * sc, int n, pko value)
{
  _kt_spagstack frame = sc->dump;
  int i = 0;
  for(frame = sc->dump; frame != 0; frame = frame->next)
    {
      if(frame->type == ksct_args)
	{
	  if( i == n )
	    {
	      frame->data.arg = value;
	      return 1;
	    }
	  else
	    { i++; }
	}
    }
  /* If we got here we never encountered the target. */
  return 0;
}
#endif
/*_    . Store from value */
/*_     , push_arg_raw */
_kt_spagstack
push_arg_raw(_kt_spagstack old_frame, pko value, int frame_depth)
{
  _kt_spagstack frame =
    (_kt_spagstack)
    GC_MALLOC (sizeof (dump_stack_frame_cell));

  frame->data.pseudoenv.vec = value;
  frame->data.pseudoenv.frame_depth = frame_depth;
  frame->type = ksct_args;
  frame->next = old_frame;
  return frame;
}
/*_     , k_do_store */
/* T_STORE */
pko
k_do_store(klink * sc, pko functor, pko value)
{
  WITH_PSYC_UNBOXED( kt_opstore, functor, T_STORE, sc );
  /* $$MAKE ME SAFE do_destructure must be safe vs resuming.  Error if
     not T_NO_K.  Don't try to maybe resume, because so far we never
     have to do that.
   */
  pko vec = do_destructure( sc, value, pdata->destr );
  /* Push that as arg */
  sc->dump = push_arg_raw (sc->dump, vec, pdata->frame_depth);
  return K_INERT;
}
/*_    . Load to value */
/*_     , get_nth_arg */
pko
get_nth_arg( _kt_spagstack frame, int n )
{
  int i = 0;
  for(; frame != 0; frame = frame->next)
    {
      if(frame->type == ksct_args)
	{
	  if( i == n )
	    { return frame->data.pseudoenv.vec; }
	  else
	    { i++; }
	}
    }
  /* If we got here we never encountered the target. */
  return 0;
}

/*_     , k_load_recurse */
/* $$IMPROVE ME Add a shortcut for accessing value without ever
   storing it. */
pko
k_load_recurse( _kt_spagstack frame, pko tree )
{
  if(_get_type( tree) == T_PAIR)
    {
      WITH_PSYC_UNBOXED( kt_vec2, tree, T_PAIR, 0 );
      if( is_integer( pdata->_car ) && is_integer( pdata->_cdr ))
	{
	  /* Pair of integers: Look up that item, look up secondary
	     index, return it */
	  const int n = ivalue( pdata->_car );
	  const int m = ivalue( pdata->_cdr );
	  pko vec = get_nth_arg( frame, n );
	  assert( vec );
	  assert( is_vector( vec ));
	  pko value = basvector_elem( vec, m );
	  assert( value );
	  return value;
	}
      else
	{
	  /* Pair, not integers: Explore car and cdr, return cons of them. */
	  return cons(
		      k_load_recurse( frame, pdata->_car ),
		      k_load_recurse( frame, pdata->_cdr ));
	}
    }
  else
    {
      /* Anything else: Return it literally. */
      return tree;
    }
}

/*_     , k_do_load */
/* T_LOAD C-destructures as a singleton.  It will contain a tree */
/* This may largely take over for decurriers. */
pko
k_do_load(klink * sc, pko functor, pko value)
{
  WITH_PSYC_UNBOXED( pko, functor, T_LOAD, sc );
  return k_load_recurse( sc->dump, *pdata );
}

/*_   , Stack ancestry */
/*_    . frame_is_ancestor_of */
int frame_is_ancestor_of(_kt_spagstack frame, _kt_spagstack other)
{
  /* Walk from other towards root.  Return 1 if we ever encounter
     frame, otherwise 0. */
  for(; other != 0; other = other->next)
    {
      if(other == frame)
	{ return 1; }
    }
  return 0;
}
/*_    . special_dynxtnt */
/* Make a child of dynamic extent OUTER that evals with dynamic
   environment ENVIR continues normally to PROX_DEST. */ 
_kt_spagstack special_dynxtnt
(_kt_spagstack outer, _kt_spagstack prox_dest, pko envir)
{
  return
    klink_push_cont_aux(outer, 
			mk_curried(dcrry_2A01VLL,
				   LIST1(mk_continuation(prox_dest)),
				   REF_OPER(invoke_continuation)),
			envir);
}
/*_    . curr_frame_depth */
int curr_frame_depth(_kt_spagstack frame)
{
  /* Walk towards root, counting. */
  int count = 0;
  for(; frame != 0; frame = frame->next, count++)
    { }
  return count;
}
/*_   , Continuations */
/*_    . Struct */
typedef struct
{
  _kt_spagstack frame;
}
  continuation_t;

/*_    . Type */
DEF_T_PRED (is_continuation, T_CONTINUATION,ground, "continuation?/o1");
/*_    . Create */
static pko
mk_continuation (_kt_spagstack frame)
{
  ALLOC_BOX_PRESUME (continuation_t, T_CONTINUATION);
  pdata->frame        = frame;
  return PTR2PKO(pbox);
}
/*_    . Parts */
static _kt_spagstack
cont_dump (pko p)
{
  WITH_PSYC_UNBOXED(continuation_t,p,T_CONTINUATION,0);
  return pdata->frame;
}

/*_    . Continuations WRT interpreter */
/*_     , current_continuation */
static pko
current_continuation (klink * sc)
{
  return mk_continuation (sc->dump);
}
/*_    . Operations */
/*_     , invoke_continuation */
/* DOES NOT RETURN */
/* Control is resumed at _klink_cycle */

/* Static and not directly available to Kernel, it's the eventual
   target of continuation_to_applicative. */
SIG_CHKARRAY(invoke_continuation) =
{ REF_OPER(is_continuation), K_ANY, };
DEF_SIMPLE_CFUNC(vs0a2,invoke_continuation,T_NO_K)
{
  WITH_2_ARGS (p, value);
  assert(is_continuation(p));
  if(p)
    { sc->dump = cont_dump (p); }
  sc->value = value;
  longjmp (sc->pseudocontinuation, 1);
}
/*_     , add_guard */
/* Add the appropriate guard, if any, and return the new proximate
   destination. */
_kt_spagstack
add_guard
(_kt_spagstack prox_dest, _kt_spagstack to_contain,
 pko guard_list, pko envir, _kt_spagstack outer) 
{
  WITH_REPORTER(0);
  pko x;
  for(x = guard_list; x != K_NIL; x = cdr(x))
    {
      pko selector = car(car(x));
      assert(is_continuation(selector));
      if(frame_is_ancestor_of(cont_dump(selector), to_contain))
	{
	  /* Call has to take place in the dynamic extent of the
	     next frame around this set of guards, so that the
	     interceptor has access to dynamic bindings, but then
	     control has to continue normally to the next guard or
	     finally to the destination.
		 
	     So we extend the next frame with a call to
	     invoke_continuation, currying the next destination in the
	     chain.  That does not check guards, so in effect it
	     continues normally.  Then we extend that with a call to
	     the interceptor, currying an continuation->applicative of
	     the guards' outer continuation.
	     
	     NB, continuation->applicative is correct.  It would be
	     wrong to shortcircuit it.  Although there are no guards
	     between there and the outer continuation, the
	     continuation we pass might be called from another dynamic
	     context.  But it needs to be unwrapped.
	  */
	  pko wrapped_interceptor = cadr(car(x));
	  assert(is_applicative(wrapped_interceptor));
	  pko interceptor = unwrap(0,wrapped_interceptor);
	  assert(is_operative(interceptor));

	  _kt_spagstack med_frame =
	    special_dynxtnt(outer, prox_dest, envir);
	  prox_dest =
	    klink_push_cont_aux(med_frame,
				mk_curried(dcrry_2VLLdotALL,
					   LIST1(continuation_to_applicative(mk_continuation(outer))),
					   interceptor),
				envir);	      

	  /* We use only the first match so end the loop. */
	  break;
	}
    }
  return prox_dest;
}
/*_     , add_guard_chain */
_kt_spagstack
add_guard_chain
(_kt_spagstack prox_dest, pko guard_frame_list, _kt_spagstack to_contain, int exit) 
{
  WITH_REPORTER(0);
  const enum klink_stack_cell_types tag
    = exit ? ksct_exit_guards : ksct_entry_guards ;
  for( ; guard_frame_list != K_NIL ; guard_frame_list = cdr(guard_frame_list))
    {
      _kt_spagstack guard_frame = cont_dump(car(guard_frame_list));
      if(guard_frame->type == tag)
	{
	  struct stack_guards * pguards = &guard_frame->data.guards;
	  prox_dest =
	    add_guard(prox_dest,
		      to_contain,
		      pguards->guards,
		      pguards->envir,
		      exit ? guard_frame->next : guard_frame);
	}
    }
  return prox_dest;
}
/*_     , continue_abnormally */
/*** Arrange to "walk" from current continuation to c, passing control
     thru appropriate guards.  ***/ 
SIG_CHKARRAY(continue_abnormally) =
{ REF_OPER(is_continuation), K_ANY, };
/* I don't give this T_NO_K even though technically it longjmps
   rather than pushing into the eval loop.  In the future we may
   distinguish those two cases. */
DEF_SIMPLE_CFUNC(ps0a2,continue_abnormally,0)
{
  WITH_2_ARGS(c,value);
  WITH_REPORTER(0);
  _kt_spagstack source      = sc->dump;
  _kt_spagstack destination = cont_dump (c);

  /*** Find the guard frames on the intermediate path. ***/

  /* Control is exiting our current frame, so collect guards from
     there towards root.  What we get is lowest first. */
  pko exiting_lo1st = get_guards_lo1st(source);
  /* Control is entering c's frame, so collect guards from there
     towards root.  Again it's lowest first.  */
  pko entering_lo1st = get_guards_lo1st(destination);
  
  /* Remove identical entries from the top, thus removing any merged
     part. */
  while((exiting_lo1st != K_NIL) &&
	(entering_lo1st != K_NIL) &&
	(cont_dump(car(exiting_lo1st)) == cont_dump(car(entering_lo1st))))
    {
      exiting_lo1st  = cdr(exiting_lo1st);
      entering_lo1st = cdr(entering_lo1st);
    }

  

  /*** Construct a string of calls to the appropriate guards, ending
       at destination.  We collect in the reverse of the order that
       they will be run, so collect from "entering" first, from
       highest to lowest, then collect from "exiting", from lowest to
       highest. ***/
  
  _kt_spagstack prox_dest = destination;

  pko entering_hi1st = reverse(sc, entering_lo1st);
  prox_dest = add_guard_chain(prox_dest, entering_hi1st, destination, 0);
  prox_dest = add_guard_chain(prox_dest,  exiting_lo1st, source,      1);

  invoke_continuation(sc, mk_continuation(prox_dest), value);
  return value;			/* NOTREACHED */
}

/*_    . Interface */
/*_     , call_cc */
SIG_CHKARRAY(call_cc) = { REF_OPER(is_combiner), };
DEF_SIMPLE_APPLICATIVE(ps0a1,call_cc,0,ground, "call/cc")
{
  WITH_1_ARGS(combiner);
  pko cc = current_continuation(sc);
  return kernel_eval_aux(sc,combiner,LIST1(cc),sc->envir);
}
/*_     , extend-continuation */
/*_      . extend_continuation_aux */
pko
extend_continuation_aux(_kt_spagstack old_frame, pko a, pko env)
{
  _kt_spagstack frame = klink_push_cont_aux(old_frame, a, env);
  return mk_continuation(frame);
}  
/*_      . extend_continuation */
SIG_CHKARRAY(extend_continuation) =
{ REF_OPER(is_continuation),
  REF_OPER(is_applicative),
  REF_KEY(K_TYCH_OPTIONAL),
  REF_OPER(is_environment),
};
DEF_SIMPLE_APPLICATIVE(ps0a3, extend_continuation,T_NO_K,ground, "extend-continuation")
{
  WITH_3_ARGS(c, a, env);
  assert(is_applicative(a));
  if(env == K_INERT) { env = make_new_frame(K_NIL); }
  return extend_continuation_aux(cont_dump(c), unwrap(sc,a), env);
}
/*_     , continuation->applicative */
SIG_CHKARRAY(continuation_to_applicative) = { REF_OPER(is_continuation), };
DEF_SIMPLE_APPLICATIVE(p00a1,continuation_to_applicative,T_NO_K,ground, "continuation->applicative")
{
  WITH_1_ARGS(c);
  return
    wrap(mk_curried (dcrry_2A01VLL, LIST1(c), REF_OPER(continue_abnormally)));
}

/*_     , guard-continuation */
/* Each guard list is repeat (list continuation applicative) */
/* We'd like to spec that applicative take 2 args, a continuation and
   a value, and be wrapped exactly once. */ 
SIG_CHKARRAY(guard_continuation) =
{ K_ANY, REF_OPER(is_continuation), K_ANY, };
DEF_SIMPLE_APPLICATIVE(ps0a3,guard_continuation,T_NO_K,ground, "guard-continuation")
{
  WITH_3_ARGS(entry_guards, c, exit_guards);
  /* The spec wants an outer continuation to keeps sets of guards from
     being mixed together if there are two calls to guard_continuation
     with the same c.  But that happens naturally here, so it seems
     unneeded.  */

  /* $$IMPROVE ME Copy the es of both lists of guards. */
  _kt_spagstack frame = cont_dump(c);
  if(entry_guards != K_NIL)
    {
      frame = klink_push_guards(frame, entry_guards, sc->envir, 0);
    }
  if(exit_guards != K_NIL)
    {
      frame = klink_push_guards(frame, exit_guards, sc->envir, 1);
    }
    
  pko inner_cont = mk_continuation(frame);
  return inner_cont;
}

/*_     , guard-dynamic-extent */
SIG_CHKARRAY(guard_dynamic_extent) =
  {
    REF_OPER(is_finite_list),
    REF_OPER(is_applicative),
    REF_OPER(is_finite_list),
  };
  /* DOES NOT RETURN */
DEF_SIMPLE_APPLICATIVE(ps0a3,guard_dynamic_extent,0,ground, "guard-dynamic-extent")
{
  WITH_3_ARGS(entry,app,exit);
  pko cont = guard_continuation(sc,entry,current_continuation(sc),exit);
  pko cont2 = extend_continuation(sc,cont, app, sc->envir);
  /* Skip directly into the new continuation, don't invoke the
     guards */
  invoke_continuation(sc,cont2, K_NIL);
  /* NOTREACHED */
  return 0;
}

/*_   , Keyed dynamic bindings */
/*_    . klink_kdb_binder */
SIG_CHKARRAY(klink_kdb_binder) =
{ REF_OPER(is_key), K_ANY, REF_OPER(is_combiner), };
DEF_SIMPLE_CFUNC(ps0a3,klink_kdb_binder,T_NO_K)
{
  WITH_3_ARGS(key, value, combiner);
  /* Check that combiner is in fact a combiner. */
  if(!is_combiner(combiner))
    {
      KERNEL_ERROR_1(sc,
		     "klink_kdb_binder: Arg 2 must be a combiner: ",
		     combiner);
    }
  /* Push the new binding. */
  klink_push_dyn_binding(sc, key, value);
  /* $$IMPROVE ME In general, should can control calling better than
     this.  Possibly do this thru invoke_continuation, except we're
     not arbitrarily changing continuations.  */
  /* $$IMPROVE ME Want a better way to control what environment to
     push in.  In fact, that's much like a dynamic variable. */
  /* $$IMPROVE ME Want a better and cheaper way to make empty
     environments. The vector thing should be controlled by a hint. */ 
  /* Make an empty static environment */
  new_frame_in_env(sc,K_NIL);
  /* Push combiner in that environment. */
  klink_push_cont(sc,combiner);
  /* And call it with no operands. */
  return K_NIL;
}
/* Combines with data to become "an applicative that takes two
  arguments, the second of which must be a oper. It calls its
  second argument with no operands (nil operand tree) in a fresh empty
  environment, and returns the result." */
/*_    . klink_kdb_accessor */
SIG_CHKARRAY(klink_kdb_accessor) =
{ REF_OPER(is_key), };
DEF_SIMPLE_CFUNC(ps0a1,klink_kdb_accessor,T_NO_K)
{
  WITH_1_ARGS(key);
  pko value = klink_find_dyn_binding(sc,key);
  if(!value)
    {
      KERNEL_ERROR_0(sc, "klink_kdb_accessor: No binding found");
    }
  return value;
}
/* Combines with data to become "an applicative that takes zero
  arguments. If the call to a occurs within the dynamic extent of a
  call to b, then a returns the value of the first argument passed to
  b in the smallest enclosing dynamic extent of a call to b. If the
  call to a is not within the dynamic extent of any call to b, an
  error is signaled."
 */
/*_    . make_keyed_dynamic_variable */
RGSTR(ground, "make-keyed-dynamic-variable", REF_OPER(make_keyed_dynamic_variable))

DEF_CFUNC(p00a0, make_keyed_dynamic_variable,K_NO_TYPE,T_NO_K)
{
  return make_keyed_variable(
			     REF_OPER(klink_kdb_binder),
			     REF_OPER (klink_kdb_accessor));
}
/*_   , Profiling */
#ifdef PROFILING
/*_    . Structs */
typedef struct profiling_data
{
  int  num_calls;
  long num_evalloops;
} profiling_data;
typedef struct
{
  pko     * objs;
  profiling_data * entries;
  int table_size;
  int alloced_size;
} kt_profile_table;
/*_    . Current data */
/* This may be moved to per interpreter, or even more fine-grained. */
/* This may not always be the way we get elapsed counts. */
static long k_profiling_count = 0;
static int k_profiling_p = 0;	/* Are we profiling now? */
/* If we are profiling, init this if it's not initted */
static kt_profile_table k_profiling_table = { 0 };
/*_    . Dealing with table (All will be shared with other lookup tables) */
/*_     , Init */
void
init_profile_table(kt_profile_table * p_table, int initial_size)
{
  p_table->objs   = initial_size ?
    GC_MALLOC(sizeof(pko) * initial_size) : 0;
  p_table->entries = initial_size ?
    GC_MALLOC(sizeof(profiling_data) * initial_size)     : 0;
  p_table->alloced_size = initial_size;
  p_table->table_size = 0;
}  
/*_     , Increase its size */
void
enlarge_profile_table(kt_profile_table * p_table)
{
  if(p_table->table_size == p_table->alloced_size)
    {
      p_table->alloced_size *= 2;
      p_table->entries = GC_REALLOC(p_table->entries, sizeof(profiling_data) * p_table->alloced_size);
      p_table->objs = GC_REALLOC(p_table->objs, sizeof(pko) * p_table->alloced_size);
    }


}  
/*_     , Searching in it */
/* Use objtable_get_index */
/*_    . On the stack */
static struct stack_profiling *
klink_find_profile_in_frame (_kt_spagstack frame, pko ff)
{
  for( ;
      (frame != 0) && (frame->type != ksct_frame) ;
      frame = frame->next)
    {
      if(frame->type == ksct_profile)
	{
	  struct stack_profiling *pdata = &frame->data.profiling;
	  if(pdata->ff == ff) { return pdata; }
	}      
    }
  return 0; 
}
/*_    . Profile collection operations */
/*_     , When eval loop steps */
void
k_profiling_step(void)
{ k_profiling_count++; }  
/*_     , When we begin executing a frame */
/* Push a stack_profiling cell onto the frame.  */

void
k_profiling_new_frame(klink * sc, pko ff)
{
  if(!k_profiling_p) { return; }
  if(!is_operative(ff)) { return; }
  /* Do this only if ff is interesting (which for the moment means
     that it can be found in ground environment). */
  if(!reverse_binds_p(ff, ground_env)           &&
     !reverse_binds_p(ff, print_lookup_unwraps) &&
     !reverse_binds_p(ff, print_lookup_to_xary))
    { return; }
  struct stack_profiling * found_profile =
    klink_find_profile_in_frame (sc->dump, ff);
  /* If the same combiner is already being profiled in this frame,
     don't add another copy.  */
  if(found_profile)
    {
      /* $$IMPROVE ME Count tail calls */
    }
  else
    {
      /* Push a profiling frame */
      _kt_spagstack old_frame = sc->dump;
      _kt_spagstack frame =
	(_kt_spagstack)
	GC_MALLOC (sizeof (dump_stack_frame_cell));
      struct stack_profiling * pdata = &frame->data.profiling;
      pdata->ff            = ff;
      pdata->initial_count = k_profiling_count;
      pdata->returned_p    = 0;
      frame->type = ksct_profile;
      frame->next = old_frame;
      sc->dump = frame;
    }
}

/*_     , When we pop a stack_profiling cell */
void
k_profiling_done_frame(klink * sc, struct stack_profiling * profile)
{
  if(!k_profiling_p) { return; }
  profiling_data * pdata = 0;
  pko ff = profile->ff;

  /* This stack_profiling cell is popped past but it might be used
     again if we re-enter, so mark it accordingly. */
  profile->returned_p = 1;
  if(k_profiling_table.alloced_size == 0)
    { init_profile_table(&k_profiling_table, 8); }
  else
    {
      int index = objtable_get_index(k_profiling_table.objs, k_profiling_table.table_size, ff);
      if(index >= 0)
	{ pdata = &k_profiling_table.entries[index]; }
    }

  /* Create it if needed */
  if(!pdata)
    {
      /* Increase size as needed */
      enlarge_profile_table(&k_profiling_table);
      /* Add entry */
      const int index = k_profiling_table.table_size;
      k_profiling_table.objs[index] = ff;
      k_profiling_table.table_size++;
      pdata = &k_profiling_table.entries[index];
      /* Initialize it here */
      pdata->num_calls     = 0;
      pdata->num_evalloops = 0;
    }
  
  /* Add to its counts: Num calls.  Num eval-loops taken. */
  pdata->num_calls++;
  pdata->num_evalloops += k_profiling_count - profile->initial_count;
}
/*_    . Interface */
/*_     , Turn profiling on */
/* Maybe better as a command-line switch or binder. */
SIG_CHKARRAY(profiling) = { REF_OPER(is_integer), };
DEF_SIMPLE_APPLICATIVE (ps0a1, profiling,T_NO_K,ground, "profiling")
{
  WITH_1_ARGS(profile_p);
  int pr = k_profiling_p;
  k_profiling_p = ivalue (profile_p);
  return mk_integer (pr);
}

/*_     , Dumping profiling data */
/* Return a list of the profiled combiners. */
DEF_APPLICATIVE_W_DESTR(ps0a0,get_profiling_data,K_NO_TYPE,T_NO_K,ground,"get-profiling-data")
{
  int index;
  pko result_list = K_NIL;
  for(index = 0; index < k_profiling_table.table_size; index++)
    {
      pko ff = k_profiling_table.objs[index];
      profiling_data * pdata = &k_profiling_table.entries[index];
      
      /* Element format: (object num-calls num-evalloops) */
      result_list = cons(
			 LIST3(ff,
			       mk_integer(pdata->num_calls),
			       mk_integer(pdata->num_evalloops)),
			 result_list);
    }
  /* Don't care about order so no need to reverse the list. */
  return result_list;
}
/*_    . Reset profiling data */
/*_   , Alternative definitions for no profiling */
#else
#define k_profiling_step()
#define k_profiling_new_frame(DUMMY, DUMMY2)
#endif
/*_  . Error handling */
/*_   , _klink_error_1 */
static void
_klink_error_1 (klink * sc, const char *s, pko a)
{
#if SHOW_ERROR_LINE
  const char *str = s;
  char sbuf[STRBUFFSIZE];
  pko the_inport = klink_find_dyn_binding(sc,K_INPORT);
  if (the_inport && (the_inport != K_NIL))
    {
      port * pt = portvalue(the_inport);
      /* Make sure error is not in REPL */
      if((pt->kind & port_file) && (pt->rep.stdio.file != stdin))
	{
	  /* Count is 0-based but print it 1-based. */
	  int ln = pt->rep.stdio.curr_line + 1;
	  const char *fname = pt->rep.stdio.filename;

	  if (!fname)
	    { fname = "<unknown>"; }

	  snprintf (sbuf, STRBUFFSIZE, "(%s : %i) %s", fname, ln, s);

	  str = (const char *) sbuf;
	}
    }
#else
  const char *str = s;
#endif
  {
    pko err_arg;
    pko err_string = mk_string (str);
    if (a != 0)
      {
	err_arg = mcons (a, K_NIL);
      }
    else
      {
	err_arg = K_NIL;
      }
    err_arg = mcons (err_string, err_arg);
    invoke_continuation (sc, sc->error_continuation, err_arg);
  }
  /* NOTREACHED */
  return;
}

/*_   , Default cheap error handlers */
/*_    .  kernel_err */
DEF_CFUNC (ps0a1, kernel_err, K_ANY,0)
{
  WITH_REPORTER(0);
  if(arg1 == K_NIL)
    {
      putstr (sc, "Error with no arguments.  I know nut-ting!");
      return K_INERT;
    }
  if(!is_finite_list(arg1))
    {
      putstr (sc, "kernel_err: arg must be a finite list");
      return K_INERT;
    }

  assert(is_pair(arg1));
  int got_string = is_string (car (arg1));
  pko args_x = got_string ? cdr (arg1) : arg1;
  const char *message = got_string ? string_value (car (arg1)) : " -- ";

  putstr (sc, "Error: ");
  putstr (sc, message);
  return kernel_err_x (sc, args_x);
}

/*_    . kernel_err_x */
DEF_CFUNC (ps0a1, kernel_err_x, K_ANY_SINGLETON,0)
{
  WITH_1_ARGS(args);
  WITH_REPORTER(0);
  putstr (sc, " ");
  if (args != K_NIL)
    {
      assert(is_pair(args));
      CONTIN_1 (dcrry_1dotALL, kernel_err_x, sc, cdr (args));
      klink_push_dyn_binding(sc,K_PRINT_FLAG,K_T);
      CONTIN_1 (dcrry_1dotALL, kernel_print_sexp, sc, car (args));
      return K_INERT;
    }
  else
    {
      putstr (sc, "\n");
      return K_INERT;
    }
}
/*_    . kernel_err_return */
DEF_CFUNC(ps0a1,kernel_err_return, K_ANY,0)
{
  /* This should not set sc->done, because when it's called it still
     must print the error, which may require more eval loops.  */
  sc->retcode = 1;
  return kernel_err(sc, arg1);
}
/*_   , Interface */
/*_    . error */
DEF_APPLICATIVE_W_DESTR(ps0a1,error,K_ANY,0,ground,"error")
{
  WITH_1_ARGS(err_arg);
  invoke_continuation (sc, sc->error_continuation, err_arg);
  return 0;			/* NOTREACHED */
}
/*_    . error-descriptor? */
/* $$WRITE ME TO replace the punted version */

/*_  . Support for calling C functions */

/*_   , klink_call_cfunc_aux */
static pko
klink_call_cfunc_aux (klink * sc, const kt_cfunc * p_cfunc, pko * arg_array)
{
  switch (p_cfunc->type)
    {
      /* For these macros, the arglist is parenthesized so is
	 usable. */

      /* ***************************************** */
      /* For function types returning bool as int (bXXaX) */
#define CASE_CFUNCTYPE_bX(SUFFIX,ARGLIST)			\
      case klink_ftype_##SUFFIX:				\
	return kernel_bool(p_cfunc->func.f_##SUFFIX ARGLIST)
      
      CASE_CFUNCTYPE_bX (b00a1, (arg_array[0]));
      CASE_CFUNCTYPE_bX (b00a2, (arg_array[0], arg_array[1]));
      CASE_CFUNCTYPE_bX (bs0a2, (sc, arg_array[0], arg_array[1]));

#undef CASE_CFUNCTYPE_bX


      /* ***************************************** */
      /* For function types returning pko (pXXaX) */
#define CASE_CFUNCTYPE_pX(SUFFIX,ARGLIST)	\
      case klink_ftype_##SUFFIX:		\
	return p_cfunc->func.f_##SUFFIX ARGLIST

      CASE_CFUNCTYPE_pX (p00a0, ());
      CASE_CFUNCTYPE_pX (p00a1, (arg_array[0]));
      CASE_CFUNCTYPE_pX (p00a2, (arg_array[0], arg_array[1]));
      CASE_CFUNCTYPE_pX (p00a3, (arg_array[0], arg_array[1], arg_array[2]));

      CASE_CFUNCTYPE_pX (ps0a0, (sc));
      CASE_CFUNCTYPE_pX (ps0a1, (sc, arg_array[0]));
      CASE_CFUNCTYPE_pX (ps0a2, (sc, arg_array[0], arg_array[1]));
      CASE_CFUNCTYPE_pX (ps0a3, (sc, arg_array[0], arg_array[1], arg_array[2]));
      CASE_CFUNCTYPE_pX (ps0a4, (sc, arg_array[0], arg_array[1],
				 arg_array[2], arg_array[3]));
      CASE_CFUNCTYPE_pX (ps0a5, (sc, arg_array[0], arg_array[1], arg_array[2], arg_array[3], arg_array[4]));

#undef CASE_CFUNCTYPE_pX


      /* ***************************************** */
      /* For function types returning void (vXXaX) */
#define CASE_CFUNCTYPE_vX(SUFFIX,ARGLIST)	\
      case klink_ftype_##SUFFIX:		\
	p_cfunc->func.f_##SUFFIX ARGLIST;	\
      return K_INERT

      CASE_CFUNCTYPE_vX (vs0a2, (sc, arg_array[0], arg_array[1]));
      CASE_CFUNCTYPE_vX (vs0a3, (sc, arg_array[0], arg_array[1], arg_array[2]));

#undef CASE_CFUNCTYPE_vX

    default:
      KERNEL_ERROR_0 (sc,
		      "kernel_call: About that function type, I know nut-ting!");
    }
}
/*_   , klink_call_cfunc */
static pko
klink_call_cfunc (klink * sc, pko functor, pko env, pko args)
{
  const kt_cfunc * p_cfunc = get_cfunc_func (functor);
  assert(p_cfunc->argcheck);
  const int max_args = destructure_how_many (p_cfunc->argcheck);
  pko arg_array[max_args];
  destructure_to_array(sc,args,
		       p_cfunc->argcheck,
		       arg_array,
		       max_args,
		       REF_OPER (k_resume_to_cfunc),
		       functor,
		       functor);
  return klink_call_cfunc_aux (sc, p_cfunc, arg_array);
}
/*_   , k_resume_to_cfunc */
SIG_CHKARRAY (k_resume_to_cfunc) =
{
  REF_OPER (is_destr_result),
  REF_KEY (K_TYCH_DOT),
  REF_OPER (is_cfunc),
};
DEF_SIMPLE_CFUNC (ps0a2, k_resume_to_cfunc, 0)
{
  WITH_2_ARGS (destr_result, functor);
  assert_type (0, functor, T_CFUNC);
  const int max_args = 5;
  pko arg_array[max_args];
  destr_result_fill_array (destr_result, max_args, arg_array);
  return klink_call_cfunc_aux (sc, get_cfunc_func (functor), arg_array);
}
/*_  . Some decurriers */
static pko
dcrry_2A01VLL (klink * sc, pko args, pko value)
{
  WITH_REPORTER(sc);
  return LIST2(car (args), value);
}
static pko dcrry_3A01dotVLL (klink * sc, pko args, pko value)
{
  WITH_REPORTER(sc);
  return cons (car (args), value);
}
static pko
dcrry_2CA01VLLA02 (klink * sc, pko args, pko value)
{
  WITH_REPORTER(sc);
  return LIST2( cons (car (args), value), cadr (args));
}
/* May not be needed */
static pko
dcrry_3A01A02VLL (klink * sc, pko args, pko value)
{
  WITH_REPORTER(sc);
  return LIST3(car (args), cadr (args), value);  
}
static pko
dcrry_2ALLVLL (klink * sc, pko args, pko value)
{
  return LIST2(args, value);  
}
static pko dcrry_2ALLV01     (klink * sc, pko args, pko value)
{
  WITH_REPORTER(sc);
  return LIST2(args, car (value));  
}

static pko
dcrry_NCVLLA01dotAX1 (klink * sc, pko args, pko value)
{
  WITH_REPORTER(sc);
  return cons(cons (value, car (args)), cdr (args));
}
static pko dcrry_NdotALL (klink * sc, pko args, pko value)
{ return args; }

static pko dcrry_1ALL (klink * sc, pko args, pko value)
{ return cons( args, K_NIL ); }

static pko dcrry_5ALLdotVLL (klink * sc, pko args, pko value)
{ return cons (args, value); }

static pko dcrry_NVLLdotALL (klink * sc, pko args, pko value)
{ return cons (value, args); }

static pko
dcrry_1VLL (klink * sc, pko args, pko value)
{ return LIST1 (value); }

/*_  . Defining */
/*_   , Internal functions */
/*_    . kernel_define_tree_aux */
kt_destr_outcome
kernel_define_tree_aux
(klink * sc, pko value, pko formal, pko env, pko * extra_result)
{
  WITH_REPORTER(0);
  if (is_pair (formal))
    {
      if (is_pair (value))
	{
	  kt_destr_outcome outcome =
	    kernel_define_tree_aux (sc, car (value), car (formal), env,
				    extra_result);
	  switch (outcome)
	    {
	    case destr_success:
	      /* $$IMPROVE ME On error, give a more accurate position. */
	      return
		kernel_define_tree_aux (sc, cdr (value), cdr (formal), env,
					extra_result);	      
	    case destr_err:
	      return destr_err;
	    case destr_must_call_k:
	      /* $$IMPROVE ME Also schedule to resume the cdr */
	      /* Operations to run, in reverse order. */
	      *extra_result =
		LISTSTAR3(
		      /* ^V= #inert */
		      REF_OPER (kernel_define_tree),
		      /* V= (value formal env) */
		      mk_load (LIST3 (cdr (value),
				      cdr (formal),
				      env)),
		      *extra_result);	      
	      return destr_must_call_k;
	    default:
	      errx (7, "Unrecognized enumeration");
	    }
	}
      if (is_promise (value))
	{
	  /* Operations to run, in reverse order. */
	  *extra_result =
	    LIST5(
		  /* ^V= #inert */
		  REF_OPER (kernel_define_tree),
		  /* V= (forced-value formal env) */
		  mk_load (LIST3 (mk_load_ix (0, 0),
				  formal, 
				  env)),
		  mk_store (K_ANY, 1),
		  /* V= forced-argobject */
		  REF_OPER (force),
		  /* ^V= (value) */
		  mk_load (LIST1 (value)));
	  return destr_must_call_k;
	}
      else
	{
	  _klink_error_1 (sc,
			   "kernel_define_tree: value must be a pair: ", value);
	  return destr_err;  		/* NOTREACHED */
	}
    }
  /* We can encounter NIL at the end of a non-dotted list, so mustn't
     try to bind it, and value list must end here too. */
  else if (formal == K_NIL)
    {
      if(value != K_NIL)
	{
	  _klink_error_1 (sc,
			   "kernel_define_tree: too many args: ", value);
	  return destr_err;  		/* NOTREACHED */
	}
      return destr_success;
    }
  /* If formal is #ignore, don't try to bind it, do nothing. */
  else if (formal == K_IGNORE)
    {
      return destr_success;
    }
  /* If it's a symbol, bind it.  Even a promise is bound thus. */
  else if (is_symbol (formal))
    {
      kernel_define (env, formal, value);
      return destr_success;
    }
  else
    {
      _klink_error_1 (sc,
		      "kernel_define_tree: can't bind to: ", formal);
      return destr_err;		/* NOTREACHED */
    }
}
/*_    . kernel_define_tree */
/* This can no longer be assumed to be T_NO_K, in case promises must
   be forced. */
SIG_CHKARRAY(kernel_define_tree) =
{ K_ANY,  K_ANY, REF_OPER(is_environment), };
DEF_SIMPLE_CFUNC(vs0a3,kernel_define_tree,0)
{
  WITH_3_ARGS(value, formal, env);
  pko extra_result;
  kt_destr_outcome outcome =
    kernel_define_tree_aux(sc, value, formal, env, &extra_result);
  switch (outcome)
    {
    case destr_success:
      break;
    case destr_err:
      /* Later this may raise the error */
      return;
    case destr_must_call_k:
      schedule_rv_list (sc, extra_result);
      return;
    default:
      errx (7, "Unrecognized enumeration");
    }
}
/*_    . kernel_define */
SIG_CHKARRAY(kernel_define) =
{
  REF_OPER(is_environment),
  REF_OPER(is_symbol),
  K_ANY,
};
DEF_SIMPLE_CFUNC(p00a3,kernel_define,T_NO_K)
{
  WITH_3_ARGS(env, symbol, value);
  assert(is_symbol(symbol));
  pko x = find_slot_in_env (env, symbol, 0);
  if (x != 0)
    {
      set_slot_in_env (x, value);
    }
  else
    {
      new_slot_spec_in_env (env, symbol, value);
    }
  return K_INERT;
}
void klink_define (klink * sc, pko symbol, pko value)
{ kernel_define(sc->envir,symbol,value); }  

/*_   , Supporting kernel registerables */
/*_    . eval_define */
RGSTR(ground, "$define!", REF_OPER(eval_define))
SIG_CHKARRAY(eval_define) =
{ K_ANY, K_ANY, };
DEF_SIMPLE_CFUNC(ps0a2,eval_define,0)
{
  pko env = sc->envir;
  WITH_2_ARGS(formal, expr);
  CONTIN_2(dcrry_3VLLdotALL,kernel_define_tree,sc,formal,env);
  /* Using args functionality:
     BEFORE:
     make 2 new slots
     put formal in 2,
     put env in 3,

     RUN, in reverse order 
     kernel_define_tree (CONTIN_0)
     make arglist from 3 args ($$WRITE MY SUPPORT) - or from 2 args and value.
     (The 2 slots will go here)
     put return value in new slot ($$WRITE MY SUPPORT)
     kernel_eval


     Possibly "make arglist" will be an array of integers, -1 meaning
     the current value.  And on its own it could do decurrying.
   */
  return kernel_eval(sc,expr,env);
}
/*_    . set */
RGSTR(ground, "$set!", REF_OPER(set))
SIG_CHKARRAY(set) =
{ K_ANY, K_ANY, K_ANY, };
DEF_SIMPLE_CFUNC(ps0a3,set,0)
{
  pko env = sc->envir;
  WITH_3_ARGS(env_expr, formal, expr);
  /* Using args functionality:

     RUN, in reverse order 
     kernel_define_tree (CONTIN_0)
     make arglist from 3 args - or from 2 args and value.
     put return value in new slot
     kernel_eval
     make arglist from 1 arg
     env_expr in slot
     formal in slot
     put return value in new slot
     kernel_eval 
     expr (Passed directly)
     
   */
  
  CONTIN_0(kernel_define_tree,sc);
  return
    kernel_mapeval(sc, K_NIL,
		   LIST3(expr,
			 LIST2(REF_OPER (arg1), formal),
			 env_expr),
		   env);
}

/*_  . Misc Kernel functions */
/*_   , tracing */

SIG_CHKARRAY(tracing) = { REF_OPER(is_integer), };
DEF_SIMPLE_APPLICATIVE (ps0a1, tracing,T_NO_K,ground, "tracing")
{
  WITH_1_ARGS(trace_p);
  int tr = sc->tracing;
  sc->tracing = ivalue (trace_p);
  return mk_integer (tr);
}

/*_   , new_tracing */

SIG_CHKARRAY(new_tracing) = { REF_OPER(is_integer), };
DEF_SIMPLE_APPLICATIVE (ps0a1, new_tracing,T_NO_K,ground, "new-tracing")
{
  WITH_1_ARGS(trace_p);
  int tr = sc->new_tracing;
  sc->new_tracing = ivalue (trace_p);
  return mk_integer (tr);
}


/*_   , get-current-environment */
DEF_APPLICATIVE_W_DESTR (ps0a0, get_current_environment, K_NO_TYPE,T_NO_K,ground, "get-current-environment")
{ return sc->envir; }

/*_   , arg1, $quote, list */
DEF_APPLICATIVE_W_DESTR (ps0a1, arg1, K_ANY_SINGLETON,T_NO_K,ground, "identity")
{
  WITH_1_ARGS(p);
  return p;
}
/* Same, unwrapped */
RGSTR(ground, "$quote",   REF_OPER(arg1))

/*_   , val2val */
RGSTR(ground, "list", REF_APPL(val2val))
/* The underlying C function here is "arg1", but it's called with
   the whole argobject as arg1 */
/* K_ANY instead of REF_OPER(is_finite_list) because we deliberately allow
   non-lists and improper lists. */
DEF_CFUNC_RAW(OPER(val2val),ps0a1,arg1,K_ANY,T_NO_K);
DEF_BOXED_APPLICATIVE(val2val, REF_OPER (val2val));

/*_   , k_quit */
RGSTR(ground,"exit",REF_OPER(k_quit))
DEF_CFUNC(ps0a0,k_quit,K_NO_TYPE,0)
{
  if(!nest_depth_ok_p(sc))
    { sc->retcode = 1; }

  sc->done = 1;
  return K_INERT;  		/* Value is unused anyways */
}
/*_   , gc */
RGSTR(ground,"gc",REF_OPER(k_gc))
DEF_CFUNC(ps0a0,k_gc,K_NO_TYPE,0)
{
  GC_gcollect();
  return K_INERT;
}

/*_   , k_if */

RGSTR(ground, "$if", REF_OPER(k_if))
FORWARD_DECL_CFUNC(static,ps0a3,k_if_literal);
SIG_CHKARRAY(k_if) = { K_ANY, K_ANY, K_ANY, };
DEF_SIMPLE_DESTR( k_if );
SIG_CHAIN(k_if) =
  {
    /* Store (test consequent alternative) */
    ANON_STORE(REF_DESTR(k_if)),

    ANON_LOAD(ANON_LIST1(ANON_LOAD_IX( 0, 0 ))),
    /* value = (test) */
	    
    REF_OPER(kernel_eval),
    /* test_result */
    /* Store (test_result) */
    ANON_STORE(K_ANY),

    ANON_LOAD(ANON_LIST3(ANON_LOAD_IX( 0, 0 ),
			 ANON_LOAD_IX( 1, 1 ),
			 ANON_LOAD_IX( 1, 2 ))),

    /* test_result, consequent, alternative */
    REF_OPER(k_if_literal),
  };

DEF_SIMPLE_CHAIN(k_if);

SIG_CHKARRAY(k_if_literal) = { REF_OPER(is_bool), K_ANY, K_ANY, };
DEF_SIMPLE_CFUNC(ps0a3,k_if_literal,0)
{
  WITH_3_ARGS(test, consequent, alternative);
  if(test == K_T) { return kernel_eval(sc, consequent, sc->envir); }
  if(test == K_F) { return kernel_eval(sc, alternative, sc->envir); }
  KERNEL_ERROR_1(sc,"Must be a boolean: ", test);
}

/*_  . Routines for applicatives */
BOX_OF_VOID (K_APPLICATIVE);

DEF_SIMPLE_PRED (is_applicative,T_NO_K,ground, "applicative?/o1")
{
  WITH_1_ARGS(p);
  return is_encap (REF_KEY(K_APPLICATIVE), p);
}

DEF_SIMPLE_PRED (is_combiner,T_NO_K,ground, "combiner?/o1")
{
  WITH_1_ARGS(p);
  return is_applicative(p) || is_operative(p);
}

SIG_CHKARRAY(wrap) = { REF_OPER(is_combiner) };
DEF_SIMPLE_APPLICATIVE (p00a1, wrap,T_NO_K,ground, "wrap")
{
  WITH_1_ARGS(p);
  return mk_encap (REF_KEY(K_APPLICATIVE), p);
}

SIG_CHKARRAY(unwrap) = { REF_OPER(is_applicative) };
DEF_SIMPLE_APPLICATIVE (ps0a1, unwrap,T_NO_K,ground, "unwrap")
{
  WITH_1_ARGS(p);
  return unencap (sc, REF_KEY(K_APPLICATIVE), p);
}

SIG_CHKARRAY(unwrap_all) = { REF_OPER(is_combiner) };
DEF_SIMPLE_APPLICATIVE (p00a1, unwrap_all,T_NO_K,ground, "unwrap-all")
{
  WITH_1_ARGS(p);
  /* Wrapping does not allowing circular wrapping, so this will
     terminate.  */
  while(is_encap (REF_KEY(K_APPLICATIVE), p))
    { p = unencap (0, REF_KEY(K_APPLICATIVE), p); }
  return p;
}


/*_  . Operatives */
/*_   , is_operative */
/* This can be hacked quicker by suppressing 1 more bit and testing
 * just once.  Requires keeping those T_ types co-ordinated, though. */
DEF_SIMPLE_PRED (is_operative,T_NO_K,ground, "operative?/o1")
{
  WITH_1_ARGS(p);
  return
       is_type (p, T_CFUNC)
    || is_type (p, T_CFUNC_RESUME)
    || is_type (p, T_CURRIED)
    || is_type (p, T_LISTLOOP)
    || is_type (p, T_CHAIN)
    || is_type (p, T_STORE)
    || is_type (p, T_LOAD)
    || is_type (p, T_TYPEP); 
}

/*_  . vau_1 */
RGSTR(simple, "$vau/3", REF_OPER(vau_1))

/* This is a simple vau for bootstrap.  It handles just a single
   expression.  It's in ground for now, but will be only in
   low-for-optimization later */ 

/* $$IMPROVE ME Check that formals is a non-circular list with no
   duplicated symbols.  If this check is typical for
   kernel_define_tree (probably), pass that an initially blank
   environment and it can check for symbols and error if they are
   already defined.

   eformal is almost REF_OPER(is_symbol) but must accept #ignore also.
*/
SIG_CHKARRAY(vau_1) = { K_ANY, K_ANY, K_ANY };
DEF_SIMPLE_CFUNC (ps0a3, vau_1,0)
{
  pko env = sc->envir;
  WITH_3_ARGS(formals, eformal, expression);
  /* This defines a vau object.  Evaluating it is different.
     See 4.10.3 */

  /* $$IMPROVE ME Could compile the expression now, but that's not so
     easy in Kernel. At least make a hook for that. */

  /* Vau data is a list of the 4 things:
     The dynamic environment
     The eformal symbol
     An immutable copy of the formals es
     An immutable copy of the expression

     $$IMPROVE ME Make not a list but a dedicated struct.
   */
  pko vau_data =
    LIST4(env,
	  eformal,
	  copy_es_immutable(sc, formals),
	  copy_es_immutable (sc, expression));
  return
    mk_curried (dcrry_5VLLdotALL, vau_data, REF_OPER (eval_vau));
}

/*_  . Evaluation, Kernel style */
/*_   , Calling operatives */
/*_    . eval_vau */
/* Again, can't simply say REF_OPER(is_symbol) because it might be
   #ignore */
SIG_CHKARRAY(eval_vau) =
{ K_ANY,
  REF_OPER(is_environment),
  K_ANY,
  K_ANY,
  K_ANY };
DEF_SIMPLE_CFUNC (ps0a5, eval_vau,0)
{ 
  pko env = sc->envir;
  WITH_5_ARGS(args, old_env, eformal, formals, expression);

  /* Make a new environment, child of the static environment (which
     we get now while making the vau) and put it into the envir
     register. */
  new_frame_in_env (sc, old_env);

  /* This will change in kernel_define, not here. */
  /* Bind the dynamic environment to the eformal symbol. */
  kernel_define_tree (sc, env, eformal, sc->envir);

  /* Bind the formals (symbols) to the operands (values) treewise.  */
  pko extra_result;
  kt_destr_outcome outcome =
    kernel_define_tree_aux(sc, args, formals, sc->envir, &extra_result);
  switch (outcome)
    {
    case destr_success:
      break;
    case destr_err:
      /* Later this may raise the error */
      return K_INERT;
    case destr_must_call_k:
      CONTIN_2 (dcrry_2dotALL, kernel_eval, sc, expression, sc->envir);
      schedule_rv_list (sc, extra_result);
      return K_INERT;
    default:
      errx (7, "Unrecognized enumeration");
    }  
  
  /* Evaluate the expression. */
  return kernel_eval (sc, expression, sc->envir);
}

/*_   , Kernel eval mutual callers */
/*_    . kernel_eval */

/* Optionally define a tracing kernel_eval  */
SIG_CHKARRAY(kernel_eval) = { K_ANY, REF_KEY(K_TYCH_OPTIONAL), REF_OPER(is_environment), };
DEF_SIMPLE_DESTR(kernel_eval);
#if USE_TRACING
FORWARD_DECL_CFUNC(static,ps0a2,kernel_real_eval);
DEF_APPLICATIVE_W_DESTR (ps0a2, kernel_eval, REF_DESTR(kernel_eval),0,ground, "eval")
{
  WITH_2_ARGS(form, env);
  /* $$RETHINK ME  Set sc->envir here, remove arg from
     kernel_real_eval, and the tracing call will know its own env,
     it may just be a closure with form as value. */
  if(env == K_INERT)
    {
      env = sc->envir;
    }
  if (sc->tracing)
    {
      CONTIN_2 (dcrry_2dotALL, kernel_real_eval, sc, form, env);
      putstr (sc, "\nEval: ");
      CONTIN_1 (dcrry_1dotALL, kernel_print_sexp, sc, form);
      return K_INERT;
    }
  else
    {
      return kernel_real_eval (sc, form, env);
    }
}
#endif

/* Define either kernel_eval (if not defined above) or kernel_real_eval  */
#if USE_TRACING
/* $$IMPROVE MY DESIGN  Don't like the pointers being different
   levels of pointingness.  In fact, we always potentially have
   tracing (or w/e) so let's lose the preprocessor condition. */

DEF_CFUNC (ps0a2, kernel_real_eval, REF_DESTR(kernel_eval),0)
#else
DEF_APPLICATIVE_W_DESTR (ps0a2, kernel_eval, REF_DESTR(kernel_eval),0,ground, "eval")
#endif
{
  WITH_REPORTER(0);
  WITH_2_ARGS(form, env);

  /* Evaluate form in env */
  /* Arguments:
     form: form to be evaluated
     env: environment to evaluate it in.
   */
  assert (form);
  assert (env);
  /* $$IMPROVE ME Let this be done in kernel_eval and lose the env
     argument, here just assert that we have an environment. */
  if(env != K_INERT)
    {
      if (is_environment (env))
	{ sc->envir = env; }
      else
	{
	  KERNEL_ERROR_0 (sc, "eval: Arg 2 must be an environment:");
	}
    }
  /* symbol */
  if (is_symbol (form))
    {
      pko x = find_slot_in_env (env, form, 1);
      if (x != 0)
	{
	  return slot_value_in_env (x);
	}
      else
	{
	  KERNEL_ERROR_1 (sc, "eval: unbound variable:", form);
	}
    }
  /* pair */
  else if (is_pair (form))
    {
      CONTIN_2 (dcrry_3VLLdotALL, kernel_eval_aux, sc, cdr (form), env);
      return kernel_eval (sc, car (form), env);
    }
  /* Otherwise return the object literally. */
  else
    {
      return form;
    }
}
/*_    . kernel_eval_aux */
/* The stage of `eval' when we've already decided that we're to use a
   combiner and what that combiner is. */
/* $$IMPROVE ME  Lose the env argument, it's always sc->envir */
SIG_CHKARRAY(kernel_eval_aux) =
{ REF_OPER(is_combiner), K_ANY, REF_OPER(is_environment), };
DEF_SIMPLE_DESTR(kernel_eval_aux);
DEF_CFUNC (ps0a3, kernel_eval_aux, REF_DESTR(kernel_eval_aux),0)
{
  WITH_3_ARGS(functor, args, env);
  assert (is_environment (env));
  /* Args:
     functor: what the car of the form has evaluated to.
     args: cdr of form, as yet unevaluated.
     env: environment to evaluate in.
   */
  k_profiling_new_frame(sc, functor);
  if(is_type(functor, T_CFUNC))
    {
      return klink_call_cfunc(sc, functor, env, args);
    }
  else if(is_type(functor, T_CURRIED))
    {
      return call_curried(sc, functor, args);
    }
  else if(is_type(functor, T_TYPEP))
    {
      /* $$MOVE ME Into something paralleling the other operative calls */
      /* $$IMPROVE ME  Check arg number */
      WITH_REPORTER(0);
      if(!is_pair(args))
	{ KERNEL_ERROR_1 (sc, "Takes one arg: ", functor); }
      return kernel_bool(call_T_typecheck(functor,car(args)));
    }
  else if(is_type(functor, T_LISTLOOP))
    {
      return eval_listloop(sc, functor,args);
    }
  else if(is_type(functor, T_CHAIN))
    {
      return eval_chain( sc, functor, args );
    }
  else if ( is_type( functor, T_STORE ))
    {
      return k_do_store( sc, functor, args );
    }
  else if ( is_type( functor, T_LOAD ))
    {
      return k_do_load( sc, functor, args );
    }
  else if (is_applicative (functor))
    {
      /* Operation:
         Get the underlying operative.
         Evaluate arguments (may make frames)
         Use the oper on the arguments
       */
      pko oper = unwrap (sc, functor);
      assert (oper);
      int4 metrics;
      get_list_metrics_aux(args, metrics);
      if(metrics[lm_cyc_len] != 0)
	{
	  KERNEL_ERROR_1 (sc, "kernel_eval_aux: Arguments must be a list", args);
	}
      sc->envir = env;		/* $$IMPROVE ME  Treat this cache better */
      CONTIN_2 (dcrry_2CA01VLLA02, kernel_eval, sc, oper, env);
#if USE_TRACING
      if (sc->tracing)
	{
	  CONTIN_3 (dcrry_4dotALL, kernel_mapeval, sc, K_NIL, args, env);
	  CONTIN_1 (dcrry_1dotALL, kernel_print_sexp, sc, args);
	  putstr (sc, "\nApply to: ");
	  return K_T;
	}
      else
#endif
	{ return kernel_mapeval (sc, K_NIL, args, env);	}
    }
  else
    {
      KERNEL_ERROR_1 (sc, "eval: can't apply:", functor);
    }
}
/*_   , Eval mappers */
/*_    . kernel_mapeval */
/* Evaluate each datum in list arg2, Kernel-returning a list of the results. */
SIG_CHKARRAY(kernel_mapeval) =
{ REF_OPER(is_finite_list), REF_OPER(is_finite_list), REF_OPER(is_environment), };
DEF_SIMPLE_DESTR(kernel_mapeval);
DEF_CFUNC (ps0a3, kernel_mapeval, REF_DESTR(kernel_mapeval),0)
{
  WITH_REPORTER(0);
  WITH_3_ARGS(accum, args, env);
  assert (is_environment (env));
  /* Arguments:
     accum:
     * The list of evaluated arguments, in reverse order.
     * Purpose: Used as an accumulator.

     args: list of forms to be evaluated.
     * Precondition: Must be a proper list (is_list must give true)
     * When called by itself: The forms that remain yet to be evaluated

     env: The environment to evaluate in.
  */

  /* If there are remaining arguments, arrange to evaluate one,
     add the result to accumulator, and return control here. */
  if (is_pair (args))
    {
      /* This can't be converted to a loop because we don't know
	 whether kernel_eval_aux will create more frames. */
      CONTIN_3 (dcrry_3CVLLA01dotAX1,
		kernel_mapeval, sc, accum, cdr (args), env);
      return kernel_eval (sc, car (args), env);
    }
  /* If there are no remaining arguments, reverse the accumulator
     and return it.  Can't reverse in place because other
     continuations might re-use the same accumulator state.  */
  else if (args == K_NIL)
    { return reverse (sc, accum); }
  else
    {
      /* This shouldn't be reachable because we check for it being
	 a list beforehand in kernel_eval_aux. */
      errx (4, "mapeval: arguments must be a list:");
    }
}

RGSTR(ground,"$bad-sequence",REF_OPER(kernel_sequence))
SIG_CHKARRAY(kernel_sequence) =
{ REF_KEY(K_TYCH_DOT), REF_OPER(is_countable_list), };
DEF_SIMPLE_CFUNC(ps0a1,kernel_sequence,0)
{
  WITH_1_ARGS(forms);
  /* Ultimately return #inert */
  /* $$IMPROVE ME This shouldn't accumulate args only to discard
     them. */
  CONTIN_0_RAW(mk_curried(dcrry_NdotALL, K_INERT, 0), sc);
  return kernel_mapeval(sc,K_NIL,forms,sc->envir);
}

/*_    . kernel_mapand_aux */
/* Call proc on each datum in args, Kernel-returning true if all
   succeed, otherwise false. */
SIG_CHKARRAY(kernel_mapand_aux) =
{ REF_OPER(is_bool),
  REF_OPER(is_combiner),
  REF_OPER(is_finite_list),
};
DEF_SIMPLE_CFUNC (ps0a3, kernel_mapand_aux,0)
{
  WITH_REPORTER(0);
  WITH_3_ARGS(ok, proc, args);
  /* Arguments:
     * succeeded: 
       * Whether the last invocation of this succeeded.  Initialize with
         K_T. 

     * proc: A boolean combiner (predicate) to apply to these objects

     * args: list of objects to apply proc to
       * Precondition: Must be a proper list
  */
  if(ok == K_F)
    { return K_F; }
  if(ok != K_T)
    { KERNEL_ERROR_1(sc, "kernel_mapand_aux: Must be boolean: ", ok); }
  /* If there are remaining arguments, arrange to evaluate one and
     return control here. */
  if (is_pair (args))
    {
      /* This can't be converted to a loop because we don't know
	 whether kernel_eval_aux will create more frames. */
      CONTIN_2 (dcrry_3VLLdotALL,
		kernel_mapand_aux, sc, proc, cdr (args));
      return kernel_eval_aux (sc, proc, car (args), sc->envir);
    }
  /* If there are no remaining arguments, return true.  */
  else if (args == K_NIL)
    { return K_T; }
  else
    {
      /* This shouldn't be reachable because we check for it being a
	 list beforehand. */
      errx (4, "mapbool: arguments must be a list:");
    }
}

/*_    . kernel_mapand */
SIG_CHKARRAY(kernel_mapand) =
{ REF_OPER(is_combiner),
  REF_OPER(is_finite_list),
};
DEF_SIMPLE_APPLICATIVE (ps0a2, kernel_mapand,0,simple, "every?/2-xary")
{
  WITH_2_ARGS(proc, args);
  /* $$IMPROVE ME  Get list metrics here and if we get a circular
     list, treat it correctly (How is TBD). */
  return kernel_mapand_aux(sc,REF_KEY(K_T), proc, args);
}
/*_    . kernel_mapor_aux */
/* Call proc on each datum in args, Kernel-returning true if all
   succeed, otherwise false. */
SIG_CHKARRAY(kernel_mapor_aux) =
{ REF_OPER(is_bool),
  REF_OPER(is_combiner),
  REF_OPER(is_finite_list),
};
DEF_SIMPLE_CFUNC (ps0a3, kernel_mapor_aux,0)
{
  WITH_REPORTER(0);
  WITH_3_ARGS(ok, proc, args);
  /* Arguments:
     * succeeded: 
       * Whether the last invocation of this succeeded.  Initialize with
         K_T. 

     * proc: A boolean combiner (predicate) to apply to these objects

     * args: list of objects to apply proc to
       * Precondition: Must be a proper list
  */
  if(ok == K_T)
    { return K_T; }
  if(ok != K_F)
    { KERNEL_ERROR_1(sc, "kernel_mapor_aux: Must be boolean: ", ok); }
  /* If there are remaining arguments, arrange to evaluate one and
     return control here. */
  if (is_pair (args))
    {
      /* This can't be converted to a loop because we don't know
	 whether kernel_eval_aux will create more frames. */
      CONTIN_2 (dcrry_3VLLdotALL,
		kernel_mapor_aux, sc, proc, cdr (args));
      return kernel_eval_aux (sc, proc, car (args), sc->envir);
    }
  /* If there are no remaining arguments, return false.  */
  else if (args == K_NIL)
    { return K_F; }
  else
    {
      /* This shouldn't be reachable because we check for it being a
	 list beforehand. */
      errx (4, "mapbool: arguments must be a list:");
    }
}
/*_    . kernel_mapor */
SIG_CHKARRAY(kernel_mapor) =
{ REF_OPER(is_combiner),
  REF_OPER(is_finite_list),
};
DEF_SIMPLE_APPLICATIVE (ps0a2, kernel_mapor,0,simple, "some?/2-xary")
{
  WITH_2_ARGS(proc, args);
  /* $$IMPROVE ME  Get list metrics here and if we get a circular
     list, treat it correctly (How is TBD). */
  return kernel_mapor_aux(sc,REF_KEY(K_F), proc, args);
}

/*_   , Kernel combiners */
/*_    . $and? */
/* $$IMPROVE ME  Make referring to curried operatives neater. */
RGSTR(ground, "$and?", REF_OBJ(k_oper_andp))
DEF_BOXED_CURRIED(k_oper_andp,
		  dcrry_2ALLVLL,
		  REF_OPER(kernel_internal_eval),
		  REF_OPER(kernel_mapand));

/*_    . $or? */
RGSTR(ground, "$or?", REF_OBJ(k_oper_orp))
DEF_BOXED_CURRIED(k_oper_orp,
		  dcrry_2ALLVLL,
		  REF_OPER(kernel_internal_eval),
		  REF_OPER(kernel_mapor));

/*_   , map */
/*_    . k_counted_map_aux */
/* $$USE ME MORE Export both to simple: "counted-map1-car"
   "counted-map1-cdr" */
pko
k_counted_map_car(klink * sc, int count, pko list, _kt_tag t_enum)
{
  int i;
  pko rv_result = K_NIL;
  for(i = 0; i < count; ++i, list = pair_cdr(0, list))
    {
      assert(is_pair(list));
      pko obj = pair_car(0, list);
      rv_result = v2cons (t_enum, pair_car(sc, obj), rv_result);
    }
  
  /* Reverse the list in place. */
  return unsafe_v2reverse_in_place(K_NIL, rv_result);
}

pko
k_counted_map_cdr(klink * sc, int count, pko list, _kt_tag t_enum)
{
  int i;
  pko rv_result = K_NIL;
  for(i = 0; i < count; ++i, list = pair_cdr(0, list))
    {
      assert(is_pair(list));
      pko obj = pair_car(0, list);
      rv_result = v2cons (t_enum, pair_cdr(sc, obj), rv_result);
    }
  
  /* Reverse the list in place. */
  return unsafe_v2reverse_in_place(K_NIL, rv_result);
}

/* Evaluate COUNT datums in list ARGS, Kernel-returning a list of the
   results. */
SIG_CHKARRAY(k_counted_map_aux) =
{ REF_OPER(is_finite_list),
  REF_OPER(is_integer),
  REF_OPER(is_integer),
  REF_OPER(is_operative),
  REF_OPER(is_finite_list),
};
DEF_SIMPLE_APPLICATIVE (ps0a5, k_counted_map_aux, 0,simple, "counted-map/5")
{
  WITH_5_ARGS(accum, count, len, oper, args);
  assert (is_integer (count));
  /* $$IMPROVE ME  Check the other args too */

  /* Arguments:
     accum:
     * The list of evaluated arguments, in reverse order.
     * Purpose: Used as an accumulator.

     count:
     * The number of arguments remaining

     len:
     * The effective length of args.
    
     oper
     * An xary operative
     
     args: list of lists of arguments to this.

     * Precondition: Must be a proper list (is_finite_list must give
       true).  args will not be cyclic, we'll check for and handle
       encycling outside of here.
  */

  /* If there are remaining arguments, arrange to operate on one, cons
     the result to accumulator, and return control here. */
  if (ivalue (count) > 0)
    {
      assert(is_pair(args));
      int len_v = ivalue(len);
      /* This can't be converted to a loop because we don't know
	 whether kernel_eval_aux will create more frames.

	 $$IMPROVE ME Check T_NO_K of proc; if it's set, we can loop.
      */
      CONTIN_5 (dcrry_5CVLLA01dotAX1,
		k_counted_map_aux, sc, accum,
		mk_integer(ivalue(count) - 1),
		len,
		oper,
		k_counted_map_cdr(sc, len_v, args, T_PAIR));

      return kernel_eval_aux (sc,
			      oper,
			      k_counted_map_car(sc, len_v, args, T_PAIR),
			      sc->envir);
    }
  /* If there are no remaining arguments, reverse the accumulator
     and return it.  Can't reverse in place because other
     continuations might re-use the same accumulator state.  */
  else
    { return reverse (sc, accum); }
}

/*_   , every? */
/*_    . counted-every?/5 */
SIG_CHKARRAY(k_counted_every) =
{ REF_OPER(is_bool),
  REF_OPER(is_integer),
  REF_OPER(is_integer),
  REF_OPER(is_operative),
  REF_OPER(is_finite_list),
};
DEF_SIMPLE_APPLICATIVE (ps0a5, k_counted_every,0,simple,"counted-every?/5")
{
  WITH_5_ARGS(ok, count, len, oper, args);
  assert (is_bool (ok));
  assert (is_integer (count));
  assert (is_integer (len));

  /* Arguments:
     * succeeded: 
       * Whether the last invocation of this succeeded.  Initialize with
         K_T. 

     count:
     * The number of arguments remaining

     len:
     * The effective length of args.
    
     oper
     * An xary operative
     
     args: list of lists of arguments to this.

     * Precondition: Must be a proper list (is_finite_list must give
       true).  args will not be cyclic, we'll check for and handle
       encycling outside of here.
  */

  if(ok == K_F)
    { return K_F; }
  if(ok != K_T)
    { KERNEL_ERROR_1(sc, "k_counted_every: Must be boolean: ", ok); }

  /* If there are remaining arguments, arrange to evaluate one and
     return control here. */
  if (ivalue (count) > 0)
    {
      assert(is_pair(args));
      int len_v = ivalue(len);
      /* This can't be converted to a loop because we don't know
	 whether kernel_eval_aux will create more frames.

	 $$IMPROVE ME Check T_NO_K of proc; if it's set, we can loop.
      */
      CONTIN_4 (dcrry_4VLLdotALL,
		k_counted_every, sc, 
		mk_integer(ivalue(count) - 1),
		len,
		oper,
		k_counted_map_cdr(sc, len_v, args, T_PAIR));

      return kernel_eval_aux (sc,
			      oper,
			      k_counted_map_car(sc, len_v, args, T_PAIR),
			      sc->envir);
    }
  /* If there are no remaining arguments, return true.  */
  else
    { return K_T; }
}

/*_   , some? */
/*_    . counted-some?/5 */
SIG_CHKARRAY(k_counted_some) =
{ REF_OPER(is_bool),
  REF_OPER(is_integer),
  REF_OPER(is_integer),
  REF_OPER(is_operative),
  REF_OPER(is_finite_list),
};
DEF_SIMPLE_APPLICATIVE (ps0a5, k_counted_some,0,simple,"counted-some?/5")
{
  WITH_5_ARGS(ok, count, len, oper, args);
  assert (is_bool (ok));
  assert (is_integer (count));
  assert (is_integer (len));

  if(ok == K_T)
    { return K_T; }
  if(ok != K_F)
    { KERNEL_ERROR_1(sc, "k_counted_some: Must be boolean: ", ok); }

  /* If there are remaining arguments, arrange to evaluate one and
     return control here. */
  if (ivalue (count) > 0)
    {
      assert(is_pair(args));
      int len_v = ivalue(len);
      /* This can't be converted to a loop because we don't know
	 whether kernel_eval_aux will create more frames.

	 $$IMPROVE ME Check T_NO_K of proc; if it's set, we can loop.
      */
      CONTIN_4 (dcrry_4VLLdotALL,
		k_counted_some, sc, 
		mk_integer(ivalue(count) - 1),
		len,
		oper,
		k_counted_map_cdr(sc, len_v, args, T_PAIR));

      return kernel_eval_aux (sc,
			      oper,
			      k_counted_map_car(sc, len_v, args, T_PAIR),
			      sc->envir);
    }
  /* If there are no remaining arguments, return false.  */
  else
    { return K_F; }
}


/*_  . Klink top level */
/*_   , kernel_repl */
DEF_CFUNC(ps0a0, kernel_repl, K_NO_TYPE,0)
{
  /* If we reached the end of file, this loop is done. */
  port *pt = portvalue (klink_find_dyn_binding(sc,K_INPORT));

  if (pt->kind & port_saw_EOF)
    { return K_INERT; }

  putstr (sc, "\n");
  putstr (sc, prompt);

  assert (is_environment (sc->envir));

  /* Arrange another iteration */
  CONTIN_0 (kernel_repl, sc);
  klink_push_dyn_binding(sc,K_PRINT_FLAG,K_T);
  klink_push_cont(sc, REF_OBJ(print_value));
#if USE_TRACING
  CONTIN_1 (dcrry_2A01VLL, tracing_say, sc, mk_string("\nGives: "));
#endif      
  CONTIN_0 (kernel_internal_eval, sc);
  CONTIN_0 (kernel_read_internal, sc);
  return K_INERT;
}

/*_   , kernel_rel */
static const kt_vector rel_chain =
  {
    3,
    ((pko[])
    {
      REF_OPER(kernel_read_internal),
	REF_OPER(kernel_internal_eval),
	REF_OPER(kernel_rel),
	}),
  };

DEF_CFUNC(ps0a0, kernel_rel, K_NO_TYPE,0)
{
  /* If we reached the end of file, this loop is done. */
  port *pt = portvalue (klink_find_dyn_binding(sc,K_INPORT));

  if (pt->kind & port_saw_EOF)
    { return K_INERT; }

  assert (is_environment (sc->envir));

#if 1
  schedule_chain( sc, &rel_chain); 
#else  
  /* Arrange another iteration */
  CONTIN_0 (kernel_rel, sc);
  CONTIN_0 (kernel_internal_eval, sc);
  CONTIN_0 (kernel_read_internal, sc);
#endif  
  return K_INERT;
}

/*_   , kernel_internal_eval */
/* Convert the aftermath of kernel_read_internal to something kernel_eval
   can accept.  */
/* $$IMPROVE ME realize this as a currier.  But it's not a curried
   object as such because it carries no internal data.  */
DEF_CFUNC (ps0a1, kernel_internal_eval, K_ANY,0)
{
  pko value = arg1;
  if( sc->new_tracing )
    { klink_push_dyn_binding( sc, K_TRACING, K_T ); }
  return kernel_eval (sc, value, sc->envir);
}

/*_  . Constructing environments */
/*_   , Declarations for built-in environments */
/* These are initialized before they are registered. */
static pko print_lookup_env = 0;
static pko all_builtins_env = 0;
static pko ground_env       = 0;
#define unsafe_env ground_env
#define simple_env ground_env
static pko typecheck_env_syms = 0;

/*_   , What to include */
#ifndef COLLECT_RGSTRS		/* If we're collecting, these files may not
				   have been generated yet */
const kernel_registerable preregister[] =
  {
    /* $$MOVE ME These others will move into dedicated arrays, and be
       combined so that they can all be seen in init.krn but not in
       ground env. */
#include "registerables/ground.inc"
#include "registerables/unsafe.inc"
#include "registerables/simple.inc"
    /* $$TRANSITIONAL */
    { "type?",  		     REF_APPL(typecheck),                },
    { "do-destructure",     	     REF_APPL(do_destructure),           },
  };

const kernel_registerable all_builtins[] =
  {
#include "registerables/all-builtins.inc"
  };

const kernel_registerable print_lookup_rgsts[] =
{
  { "#f",      REF_KEY(K_F),       },
  { "#t",      REF_KEY(K_T),       },
  { "#inert",  REF_KEY(K_INERT),   },
  { "#ignore", REF_KEY(K_IGNORE),  },
  
  { "$quote",  REF_OPER(arg1),     },
  
  /* $$IMPROVE ME  Add the other quote-like symbols here. */
  /* quasiquote, unquote, unquote-splicing */
  
};

const kernel_registerable typecheck_syms_rgsts[] =
  {
#include "registerables/type-keys.inc"
  };
#endif


/*_   , How to add */

/* Bind each of an array of kernel_registerables into env. */
void
k_register_list (const kernel_registerable * list, int count, pko env)
{
  int i;
  assert(list);
  assert (is_environment (env));
  for (i = 0; i < count; i++)
    {
      kernel_define (env, mk_symbol (list[i].name), list[i].data);
    }
}

/*_   , k_regstrs_to_env */
pko
k_regstrs_to_env(const kernel_registerable * list, int count)
{
  pko env = make_new_frame(K_NIL);
  k_register_list (list, count, env);
  return env;
}

#define K_REGSTRS_TO_ENV(RGSTRS)\
  k_regstrs_to_env(RGSTRS, sizeof (RGSTRS) / sizeof (RGSTRS[0]))
/*_   , setup_print_secondary_lookup */
static pko print_lookup_unwraps = 0;
static pko print_lookup_to_xary = 0;
void
setup_print_secondary_lookup(void)
{
  /* Quick and dirty: Set up tables corresponding to the ground env
     and put the registering stuff in them. */
  /* What this really accomplishes is to make prepared lookup tables
     available for particular print operations.  Later we'll use a
     more general approach and this will become just a cache. */
  print_lookup_unwraps = make_new_frame(K_NIL);
  print_lookup_to_xary = make_new_frame(K_NIL);
  int i;
  const kernel_registerable * list = preregister;
  int count = sizeof (preregister) / sizeof (preregister[0]);
  for (i = 0; i < count; i++)
    {
      pko obj = list[i].data;
      if(is_applicative(obj))
	{
	  kernel_define (print_lookup_unwraps,
			 mk_symbol (list[i].name),
			 unwrap(0,obj));
	}
      pko xary = k_to_trivpred(obj);
      if((xary != K_NIL) && xary != obj)
	{
	  kernel_define (print_lookup_to_xary,
			 mk_symbol (list[i].name),
			 xary);
	}
    }  
}
  
/*_   , make-kernel-standard-environment */
/* Though it would be neater for this to define ground environment if
   there is none, that would mean it would need the eval loop and so
   couldn't be done early.  So it relies on the ground environment
   being already defined.  */
RGSTR(ground,"make-kernel-standard-environment", REF_OPER(mk_std_environment))
DEF_CFUNC(p00a0, mk_std_environment, K_NO_TYPE,T_NO_K)
{
  assert(ground_env);
  return make_new_frame(ground_env);
}

/*_  . The eval cycle */
/*_   , Helpers */
/*_    . Make an error continuation */
static void
klink_record_error_cont (klink * sc, pko error_continuation)
{
  /* Record error continuation. */
  kernel_define (sc->envir,
		 mk_symbol ("error-continuation"),
		 error_continuation);
  /* Also record it in interpreter, so built-ins can see it w/o
     lookup. */
  sc->error_continuation = error_continuation;
}

/*_   , Entry points */
/*_    . Eval cycle that restarts on error */
static void
klink_cycle_restarting (klink * sc, pko combiner)
{
  assert(is_combiner(combiner));
  assert(is_environment(sc->envir));
  /* Arrange to stop if we ever reach where we started. */
  klink_push_cont (sc, REF_OPER (k_quit));
    
  /* Grab root continuation. */
  kernel_define (sc->envir,
		 mk_symbol ("root-continuation"),
		 current_continuation (sc));

  /* Make main continuation */
  klink_push_cont (sc, combiner);

  /* Make error continuation on top of main continuation. */
  pko error_continuation =
    extend_continuation_aux(sc->dump, REF_OPER(kernel_err), sc->envir);

  klink_record_error_cont(sc, error_continuation);

  /* Conceptually sc->retcode is a keyed dynamic variable that
     kernel_err sets. */
  sc->retcode = 0;
  _klink_cycle (sc);
  /* $$RECONSIDER ME Maybe indicate quit value  */
}  
/*_    . Eval cycle that terminates on error */
static int
klink_cycle_no_restart (klink * sc, pko combiner)
{
  assert(is_combiner(combiner));
  assert(is_environment(sc->envir));
  /* Arrange to stop if we ever reach where we started. */
  klink_push_cont (sc, REF_OPER (k_quit));

  /* Grab root continuation. */
  kernel_define (sc->envir,
		 mk_symbol ("root-continuation"),
		 current_continuation (sc));

  /* Make error continuation that quits. */
  pko error_continuation =
    extend_continuation_aux(sc->dump, REF_OPER(kernel_err_return), sc->envir);
  
  klink_record_error_cont(sc, error_continuation);
  
  klink_push_cont (sc, combiner);

  /* Conceptually sc->retcode is a keyed dynamic variable that
     kernel_err sets.  Actually it's entirely cached in the
     interpreter. */
  sc->retcode = 0;
  _klink_cycle (sc);
  return sc->retcode;
}
  
/*_   , _klink_cycle (Don't use this directly) */
static void
_klink_cycle (klink * sc)
{
  pko value = K_INERT;

  sc->done = 0;
  while (!sc->done)
    {
      int i = setjmp (sc->pseudocontinuation);
      if (i == 0)
	{
	  k_profiling_step();
	  int got_new_frame = klink_pop_cont (sc);
	  /* $$RETHINK ME  Is this test still needed?  Could be just
	     an assertion. */
	  if (got_new_frame)
	    {
	      /* $$IMPROVE ME Instead, a function that governs
		 whether to eval. */
	      if (sc->new_tracing)
		{
		  if(_get_type( sc->next_func ) == T_NOTRACE )
		    {
		      sc->next_func = notrace_comb( sc->next_func );
		      goto normal;
		    }
		  pko tracing =
		       klink_find_dyn_binding(sc, K_TRACING );
		  /* Now we know the other branch should have been
		     taken. */
		  if( !tracing || ( tracing == K_F ))
		    { goto normal; }

		  /* Enqueue a version that will execute without
		     tracing.  Its descendants will be traced. */
		  CONTIN_0_RAW (mk_notrace(mk_curried(dcrry_1dotALL,
						      value,
						      mk_notrace(sc->next_func))),
				 sc );
		  switch (_get_type (sc->next_func))
		    {
		    case T_LOAD:
		      putstr (sc, "\nLoad ");
		      break;
		      
		    case T_STORE:
		      putstr (sc, "\nStore ");
		      break;
		      
		    case T_CURRIED:
		      putstr (sc, "\nDecurry ");
		      break;
		      
		    default:
		      /* Print tracing */
		      {
			/* Find and print current frame depth */
			int depth = curr_frame_depth (sc->dump);
			char * str = sc->strbuff;
			snprintf (str, STRBUFFSIZE, "\n%d: ", depth);
			putstr (sc, str);
		      }
		      klink_push_dyn_binding (sc, K_TRACING, K_F);
		      putstr (sc, "Eval: ");
		      value = kernel_print_sexp (sc,
						 cons (sc->next_func, value),
						 K_INERT);		      
		    }
		}
	      else
		{
		normal:
		  value = kernel_eval_aux (sc, sc->next_func, value, sc->envir);
		}
	      
	    }
	  /* Stop looping if stack is empty. */
	  else
	    { break; }
	}
      else
	/* Otherwise something jumped to a continuation.  Get the
	   value and keep looping.  */
	{
	  value = sc->value;
	}
    }
  /* In case we're called nested in another _klink_cycle, don't
     affect it. */
  sc->done = 0;
}

/*_  . Vtable interface */
/* initialization of Klink */
#if USE_INTERFACE

static struct klink_interface vtbl =
  {
    klink_define,
    mk_mutable_pair,
    mk_pair,
    mk_integer,
    mk_real,
    mk_symbol,
    mk_string,
    mk_counted_string,
    mk_character,
    mk_vector,
    putstr,
    putcharacter,

    is_string,
    string_value,
    is_number,
    nvalue,
    ivalue,
    rvalue,
    is_integer,
    is_real,
    is_character,
    charvalue,
    is_finite_list,
    is_vector,
    list_length,
    vector_len,
    fill_vector,
    vector_elem,
    set_vector_elem,
    is_port,

    is_pair,
    pair_car,
    pair_cdr,
    set_car,
    set_cdr,

    is_symbol,
    symname,

    is_continuation,
    is_environment,
    is_immutable,
    setimmutable,

    klink_load_file,
    klink_load_string,
  };
#if USE_DL
/* $$MOVE ME  Later after I separate some headers
   This belongs in dynload.c, could be just:
   SIG_CHKARRAY(klink_load_ext) = { REF_OPER(is_string), };
   DEF_SIMPLE_APPLICATIVE(ps0a1,klink_load_ext,0,ground, "load-extension") {...}
 */
RGSTR(ground, "load-extension", REF_APPL(klink_load_ext))
SIG_CHKARRAY(klink_load_ext) = { REF_OPER(is_string), };
DEF_SIMPLE_DESTR(klink_load_ext);
DEF_CFUNC_PSYCNAME(ps0a1,klink_load_ext, REF_DESTR(klink_load_ext),0);
DEF_BOXED_APPLICATIVE(klink_load_ext, REF_OPER (klink_load_ext));

#endif

#endif

/*_  . Initializing Klink */
/*_   , Allocate and initialize */

klink *
klink_alloc_init (FILE * in, FILE * out)
{
  klink *sc = (klink *) GC_MALLOC (sizeof (klink));
  if (!klink_init (sc, in, out))
    {
      GC_FREE (sc);
      return 0;
    }
  else
    {
      return sc;
    }
}

/*_   , Initialization without allocation */
int
klink_init (klink * sc, FILE * in, FILE * out)
{
  /* Init stack first, just in case something calls _klink_error_1. */
  dump_stack_initialize (sc);
  /* Initialize ports early in case something prints. */
  /* $$IMPROVE ME  Should accept general ports of appropriate in/out. */
  klink_set_input_port_file (sc, in);
  klink_set_output_port_file (sc, out);
  
#if USE_INTERFACE
  /* Why do we need this field if there is a static table? */
  sc->vptr = &vtbl;
#endif

  sc->tracing = 0;
  sc->new_tracing = 0;

  if(!oblist)
    { oblist = oblist_initial_value (); }


  /* Add the Kernel built-ins */
  if(!print_lookup_env)
    {
      print_lookup_env = K_REGSTRS_TO_ENV(print_lookup_rgsts);
    }
  if(!all_builtins_env)
    {
      all_builtins_env = K_REGSTRS_TO_ENV(all_builtins);
    }
  if(!typecheck_env_syms)
    { typecheck_env_syms = K_REGSTRS_TO_ENV(typecheck_syms_rgsts); }
  if(!ground_env)
    {
      /** Register objects from hard-coded list. **/
      ground_env = K_REGSTRS_TO_ENV(preregister);
      /* $$TRANSITIONAL  Set up special lookup tables related to preregister. */
      setup_print_secondary_lookup();
      /** Bind certain objects that we make at init time.  **/
      kernel_define (ground_env,
		     mk_symbol ("print-lookup-env"),
		     print_lookup_env);
      kernel_define (unsafe_env,
		     mk_symbol ("typecheck-special-syms"),
		     typecheck_env_syms);

      /** Read some definitions from a prolog **/
      /* We need an envir before klink_call, because that defines a
	 few things.  Those bindings are specific to one instance of
	 the interpreter so they do not belong in anything shared such
	 as ground_env.  */
      /* $$IMPROVE ME Something in the call chain (klink_call?) should
	 guarantee an environment.  Needn't have anything in it to
	 begin with.  */
      sc->envir = make_new_frame(K_NIL);
      
      /* Can't easily merge this with klink_load_named_file.  Two
	 difficulties: it uses klink_cycle_restarting while klink_call
	 uses klink_cycle_no_restart, and here we need to control the
	 load environment.  */
      pko p = port_from_filename (InitFile, port_file | port_input);
      if (p == K_NIL) { return 0; }

      /* We can't use k_get_mod_fm_port to manage parameters because
	 later we will need the environment to have several parents:
	 ground, simple, unsafe, possibly more. */
      /* Params: `into' =  ground environment */
      /* We can't share this with the previous frame-making, because
	 it should not define in the same environment.  */
      pko params = make_new_frame(K_NIL);
      kernel_define (params, mk_symbol ("into"), ground_env);
      pko env = make_new_frame(ground_env);
      kernel_define (env, mk_symbol ("module-parameters"), params);
      int retcode = klink_call(sc,
			       REF_OPER(load_from_port),
			       LIST2(p, env));
      if(retcode) { return 0; }
      
      /* The load will have written various things into ground
	 environment.  sc->envir is unsuitable now because it is this
	 load's environment. */
    }

  assert (is_environment (ground_env));
  sc->envir = make_new_frame(ground_env);
  
#if 1  				/* Transitional.  Leave this on for the moment */
  /* initialization of global pointers to special symbols */
  sc->QUOTE = mk_symbol ("quote");
  sc->QQUOTE = mk_symbol ("quasiquote");
  sc->UNQUOTE = mk_symbol ("unquote");
  sc->UNQUOTESP = mk_symbol ("unquote-splicing");
  sc->COLON_HOOK = mk_symbol ("*colon-hook*");
  sc->SHARP_HOOK = mk_symbol ("*sharp-hook*");
#endif
  return 1;
}

/*_   , Deinit */
void
klink_deinit (klink * sc)
{
  sc->envir = K_NIL;
  sc->value = K_NIL;
}
/*_  . Using Klink from C */
/*_   , To set ports */
void
klink_set_input_port_file (klink * sc, FILE * fin)
{
  klink_push_dyn_binding(sc,K_INPORT,port_from_file (fin, port_input));
}

void
klink_set_input_port_string (klink * sc, char *start, char *past_the_end)
{
  klink_push_dyn_binding(sc,
			 K_INPORT,
			 port_from_string (start, past_the_end, port_input));
}

void
klink_set_output_port_file (klink * sc, FILE * fout)
{
  klink_push_dyn_binding(sc,K_OUTPORT,port_from_file (fout, port_output));
}

void
klink_set_output_port_string (klink * sc, char *start, char *past_the_end)
{
  klink_push_dyn_binding(sc,
			 K_OUTPORT,
			 port_from_string (start, past_the_end, port_output));
}
/*_   , To set external data */
void
klink_set_external_data (klink * sc, void *p)
{
  sc->ext_data = p;
}


/*_   , To load */
/*_    . Load file (C) */
/*_     , Worker */
void
klink_load_port (klink * sc, pko p, int interactive)
{
  if (p == K_NIL)
    {
      sc->retcode = 2;
      return;
    }
  else
    {
      klink_push_dyn_binding(sc,K_INPORT,p);
    }

  {
    pko combiner =
      interactive ?
      REF_OPER (kernel_repl) :
      REF_OPER (kernel_rel);
    klink_cycle_restarting (sc, combiner);
  }
}

/*_     , klink_load_file */
void
klink_load_file (klink * sc, FILE * fin)
{
  klink_load_port (sc,
		   port_from_file (fin, port_file | port_input),
		   (fin == stdin));
}

/*_     , klink_load_named_file */
void
klink_load_named_file (klink * sc, FILE * fin, const char *filename)
{
  klink_load_port(sc,
		  port_from_filename (filename, port_file | port_input),
		  (fin == stdin));
}

/*_    . load string (C) */

void
klink_load_string (klink * sc, const char *cmd)
{
  klink_load_port(sc,
		  port_from_string ((char *)cmd,
				    (char *)cmd + strlen (cmd),
				    port_input | port_string),
		  0);
}

/*_   , Apply combiner */
/* sc is presumed to be already set up.
   The final value or error argument is in sc->value.
   The return code is duplicated in sc->retcode.
 */
int
klink_call (klink * sc, pko func, pko args)
{
  klink_cycle_no_restart (sc,
			  mk_curried(dcrry_NdotALL,args,func));
  return sc->retcode;
}

/*_   , Eval form */
/* This is completely unexercised. */

int
klink_eval (klink * sc, pko obj)
{
  klink_cycle_no_restart(sc,
			 mk_curried(dcrry_2dotALL,
				    LIST2(obj,sc->envir),
				    REF_OPER(kernel_eval)));
  return sc->retcode;
}

/*_  . Main (if standalone) */
#if STANDALONE
/*_   , Mac */
#if defined(__APPLE__) && !defined (OSX)
int
main ()
{
  extern MacTS_main (int argc, char **argv);
  char **argv;
  int argc = ccommand (&argv);
  MacTS_main (argc, argv);
  return 0;
}

/*_   , General */
int
MacTS_main (int argc, char **argv)
{
#else
int
main (int argc, char **argv)
{
#endif
  klink sc;
  FILE *fin = 0;
  char *file_name = 0;		/* Was InitFile */
  int retcode;
  int isfile = 1;
  GC_INIT ();
  if (argc == 1)
    {
      printf (banner);
    }
  if (argc == 2 && strcmp (argv[1], "-?") == 0)
    {
      printf ("Usage: klink -?\n");
      printf ("or:    klink [<file1> <file2> ...]\n");
      printf ("followed by\n");
      printf ("          -1 <file> [<arg1> <arg2> ...]\n");
      printf ("          -c <Kernel commands> [<arg1> <arg2> ...]\n");
      printf ("assuming that the executable is named klink.\n");
      printf ("Use - as filename for stdin.\n");
      return 1;
    }

  /* Make error_continuation semi-safe until it's properly set.  */
  sc.error_continuation = 0;
  int i = setjmp (sc.pseudocontinuation);
  if (i == 0)
    {
      if (!klink_init (&sc, stdin, stdout))
	{
	  fprintf (stderr, "Could not initialize!\n");
	  return 2;
	}
    }
  else
    {
      fprintf (stderr, "Kernel error encountered while initializing!\n");
      return 3;
    }
  argv++;
  /* $$IMPROVE ME Maybe use get_opts instead. */
  while(1)
    {
      /* $$IMPROVE ME Add a principled way of sometimes including
	 filename defined in environment.  Eg getenv
	 ("KLINKINIT").  */
      file_name = *argv;
      argv++;
      if(!file_name) { break; }
      if (strcmp (file_name, "-") == 0)
	{
	  fin = stdin;
	}
      else if (strcmp (file_name, "-1") == 0 || strcmp (file_name, "-c") == 0)
	{
	  pko args = K_NIL;
	  /* $$FACTOR ME This is a messy way to distinguish command
	     string from filename string */
	  isfile = (file_name[1] == '1');
	  file_name = *argv++;
	  if (strcmp (file_name, "-") == 0)
	    {
	      fin = stdin;
	    }
	  else if (isfile)
	    {
	      fin = fopen (file_name, "r");
	    }

	  /* Put remaining command-line args into *args* in envir. */
	  for (; *argv; argv++)
	    {
	      pko value = mk_string (*argv);
	      args = mcons (value, args);
	    }
	  args = unsafe_v2reverse_in_place (K_NIL, args);
	  /* Instead, use (command-line) as accessor and provide the
	     whole command line as a list of strings.  */
	  kernel_define (sc.envir, mk_symbol ("*args*"), args);

	}
      else
	{
	  fin = fopen (file_name, "r");
	}
      if (isfile && fin == 0)
	{
	  fprintf (stderr, "Could not open file %s\n", file_name);
	}
      else
	{
	  if (isfile)
	    {
	      /* $$IMPROVE ME Use klink_load_named_file, replacing the
		 file-opening code, so we can report filename  */
	      klink_load_file (&sc, fin);
	    }
	  else
	    {
	      klink_load_string (&sc, file_name);
	    }
	  if (!isfile || fin != stdin)
	    {
	      if (sc.retcode != 0)
		{
		  fprintf (stderr, "Errors encountered reading %s\n",
			   file_name);
		}
	      if (isfile)
		{
		  fclose (fin);
		}
	    }
	}
    }

  if (argc == 1)
    {
      /* $$MAKE ME CLEANER Quick and dirty for now, we make an
	 environment for this but let everything else modify ground
	 env.  I'd like to be more correct about that. */
      /* Make an interactive environment over ground_env. */
      new_frame_in_env (&sc, sc.envir);
      klink_load_file (&sc, stdin);
    }
  retcode = sc.retcode;
  klink_deinit (&sc);

  return retcode;
}

#endif

/*_ , Footers */
/*
Local variables:
c-file-style: "gnu"
mode: allout
End:
*/
