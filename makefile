# Makefile for Klink
# Time-stamp: <2002-06-24 14:13:27 gildea> 
  
# Windows/2000 
#CC = cl -nologo 
#DEBUG= -W3 -Z7 -MD 
#DL_FLAGS= 
#SYS_LIBS= 
#Osuf=obj 
#SOsuf=dll 
#LIBsuf=.lib
#EXE_EXT=.exe 
#LD = link -nologo 
#LDFLAGS = -debug -map -dll -incremental:no 
#LIBPREFIX = 
#OUT = -out:$@ 
#RM= -del
#AR= echo

# Unix, generally 
CC = gcc -fpic $(CFLAGS)
DEBUG=-g -Wall -Wno-char-subscripts -O 
Osuf=o 
SOsuf=so 
LIBsuf=a
EXE_EXT=
LIBPREFIX=lib
OUT = -o $@ 
RM= -rm -f
AR= ar crs
 
# Linux 
LD = gcc 
LOCAL_LDFLAGS = -shared $(LDFLAGS)
DEBUG=-g -O -Wall -Wno-unused-function
SYS_LIBS= -ldl -lgc
PLATFORM_FEATURES= 

# Cygwin
#PLATFORM_FEATURES = -DUSE_STRLWR=0

 
# Solaris 
#SYS_LIBS= -ldl -lc 
#Osuf=o 
#SOsuf=so 
#EXE_EXT= 
#LD = ld 
#LOCAL_LDFLAGS = -G -Bsymbolic -z text $(LDFLAGS)
#LIBPREFIX = lib 
#OUT = -o $@ 
 
FEATURES = $(PLATFORM_FEATURES) -DUSE_DL=1 -DUSE_MATH=0 -DUSE_ASCII_NAMES=0
 
OBJS = klink.$(Osuf) dynload.$(Osuf) 
 
LIBTARGET = $(LIBPREFIX)klink.$(SOsuf) 
STATICLIBTARGET = $(LIBPREFIX)klink.$(LIBsuf)
RGSTRBLS = registerables/ground.inc registerables/unsafe.inc \
registerables/simple.inc registerables/type-keys.inc \
registerables/all-builtins.inc

all: $(LIBTARGET) $(STATICLIBTARGET) klink$(EXE_EXT)

%.$(Osuf): %.c 
	$(CC) -I. -c $(DEBUG) $(FEATURES) $(DL_FLAGS) $< 

$(LIBTARGET): $(OBJS) 
	$(LD) $(LOCAL_LDFLAGS) $(OUT) $(OBJS) $(SYS_LIBS)

klink$(EXE_EXT): $(OBJS) 
	$(CC) -o $@ $(DEBUG) $(OBJS) $(SYS_LIBS) 

$(STATICLIBTARGET): $(OBJS)
	$(AR) $@ $(OBJS)

$(OBJS): klink.h klink-private.h $(RGSTRBLS)
dynload.$(Osuf): dynload.h 

clean: 
	$(RM) $(OBJS) $(LIBTARGET) $(STATICLIBTARGET) klink$(EXE_EXT)
	$(RM) klink.ilk klink.map klink.pdb klink.lib klink.exp
	$(RM) *~

TAGS_SRCS = klink.h klink.c dynload.h dynload.c

tags: TAGS 
TAGS: $(TAGS_SRCS) 
	etags $(TAGS_SRCS) 

%.reg:%.c
	gcc -DCOLLECT_RGSTRS -E $*.c \
	| sed 's/_K_END_RGSTR/_K_END_RGSTR\n/' \
	| grep --only-matching --no-filename "_K_RGSTR.*_K_END_RGSTR" \
	| sed 's/_K_RGSTR *\(.*\)_K_END_RGSTR/\1/' \
	> $*.reg

registerables/%.inc:
	cat $^ \
	| grep --no-filename "^$*:" \
	| cut --delimiter=":" -f 2- \
	> registerables/$*.inc

##Explicitly give each inclusion's dependencies.
registerables/ground.inc: klink.reg
registerables/unsafe.inc: klink.reg
registerables/simple.inc: klink.reg
registerables/type-keys.inc: klink.reg
registerables/all-builtins.inc: klink.reg
