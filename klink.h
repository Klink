/*_. Klink 0.0: klink.h */
/*_ , Header */
/*_  . Credits and License */
/*
    Copyright (C) 2010,2011 Tom Breton (Tehom)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*_  . Once */
#ifndef _KLINK_H
#define _KLINK_H

/*_  . Own includes */
#include <stdio.h>

/*_  . C++ nonmangling */
#ifdef __cplusplus
extern "C"
{
#endif

/*_ , Body */

/*_  . Default values for #define'd symbols */
/*
 * Default values for #define'd symbols
 */
#ifndef STANDALONE		/* If used as standalone interpreter */
# define STANDALONE 1
#endif

#ifndef _MSC_VER
# define USE_STRCASECMP 1
# ifndef USE_STRLWR
#   define USE_STRLWR 1
# endif
# define KLINK_EXPORT
#else
# define USE_STRCASECMP 0
# define USE_STRLWR 0
# ifdef _KLINK_SOURCE
#  define KLINK_EXPORT __declspec(dllexport)
# else
#  define KLINK_EXPORT __declspec(dllimport)
# endif
#endif

#if USE_NO_FEATURES
# define USE_MATH 0
# define USE_CHAR_CLASSIFIERS 0
# define USE_ASCII_NAMES 0
# define USE_STRING_PORTS 0
# define USE_ERROR_HOOK 0
# define USE_TRACING 0
# define USE_COLON_HOOK 0
# define USE_DL 0
# define USE_PLIST 0
#endif

#define PROFILING   		/* $$TRANSITIONAL Define profiling */
#define USE_OBJECT_LIST		/* Temporary diagnostic */
#if USE_DL
# define USE_INTERFACE 1
#endif


#ifndef USE_MATH		/* If math support is needed */
# define USE_MATH 1
#endif

#ifndef USE_CHAR_CLASSIFIERS	/* If char classifiers are needed */
# define USE_CHAR_CLASSIFIERS 1
#endif

#ifndef USE_ASCII_NAMES		/* If extended escaped characters are needed */
# define USE_ASCII_NAMES 1
#endif

#ifndef USE_STRING_PORTS	/* Enable string ports */
# define USE_STRING_PORTS 1
#endif

#ifndef USE_TRACING
# define USE_TRACING 1
#endif

#ifndef USE_PLIST
# define USE_PLIST 0
#endif

/* To force system errors through user-defined error handling (see *error-hook*) */
#ifndef USE_ERROR_HOOK
# define USE_ERROR_HOOK 1
#endif

#ifndef USE_COLON_HOOK		/* Enable qualified qualifier */
# define USE_COLON_HOOK 1
#endif

#ifndef USE_STRCASECMP		/* stricmp for Unix */
# define USE_STRCASECMP 0
#endif

#ifndef USE_STRLWR
# define USE_STRLWR 1
#endif

#ifndef STDIO_ADDS_CR		/* Define if DOS/Windows */
# define STDIO_ADDS_CR 0
#endif

#ifndef INLINE
# define INLINE
#endif

#ifndef USE_INTERFACE
# define USE_INTERFACE 0
#endif

#ifndef SHOW_ERROR_LINE		/* Show error line in file */
# define SHOW_ERROR_LINE 1
#endif
  
/*_  .  */
#ifdef COLLECT_RGSTRS 		/* Not compiling, collecting registerables */
# define RGSTR(ARRAY,K_NAME,C_NAME) \
  _K_RGSTR ARRAY:{ K_NAME, C_NAME, }, _K_END_RGSTR
#else  
# define RGSTR(ARRAY,K_NAME,C_NAME)		/* Do nothing */
#endif

/*_  . Fundamental types */
/*_   , Fundamental enumerations */
/*_    . The T_ types */

typedef enum klink_types
{
  T_STRING,
  T_NUMBER,
  T_SYMBOL,
  T_PAIR,
  T_CHARACTER,
  T_PORT,
  T_VECTOR,
  T_ENCAP,
  T_CONTINUATION,
  T_CFUNC,
  T_CURRIED,
  T_DESTRUCTURE,
  T_TYPECHECK,
  T_TYPEP,
  T_ENV_FRAME,
  T_ENV_PAIR,
  T_KEY,
  T_RECURRENCES, 		/* Returned by get-recurrences */
  T_RECUR_TRACKER, 		
  T_LISTLOOP_STYLE,
  T_LISTLOOP,
  T_PROMISE,
  T_CHAIN,
  T_CHAIN_ARG,
  T_CHAIN_ACCUM,
  T_STORE,		/* Operative  */
  T_LOAD,		/* Operative */
  T_NOTRACE,		/* Operative */
  T_DESTR_RESULT,
  T_CFUNC_RESUME,
  
  T_NO_K      = 16384,          /* Function will not need _klink_cycle */
  T_IMMUTABLE = 32768,          /* Object can't mutate  */
  T_MASKTYPE  = ~(T_IMMUTABLE | T_NO_K),
} _kt_tag;

/*_   , Fundamental typedefs */

/*_    . Interpreters, error reporting */
  
  typedef struct klink klink;
  /* Error-reporting, allowed to be 0 to indicate that only C static
     checking is wanted. */
  typedef klink * sc_or_null;
/*_    . Kernel objects */

  typedef struct
  {
    _kt_tag type;
  } kt_boxed_any;

  typedef kt_boxed_any kt_boxed_void;

/*_     , Macros helping this */
#define BOX_OF(TYPE)				\
  struct { _kt_tag type; TYPE data; }


/*_    . Pointers to kernel objects */
  /* We can't reasonably have a `const' variant, because pko's are
     passed around in a way governed by Kernel logic, not C constness
     logic.  */
  typedef _kt_tag *pko;

/*_     , Macros converting from box to pko */
#define PTR2PKO(OBJ) (&(OBJ)->type)
#define REF_OBJ(OBJ) (&(OBJ).type)

/*_   , Struct definitions */

/* num, for generic arithmetic */
  typedef struct num
  {
    char is_fixnum;
    union
    {
      long ivalue;
      double rvalue;
    } value;
  } num;

/*_  . Signatures of exported functions */
  KLINK_EXPORT klink *klink_alloc_init (FILE * in, FILE * out);
  KLINK_EXPORT int klink_init (klink * sc, FILE * in, FILE * out);
  KLINK_EXPORT void klink_deinit (klink * sc);
  void klink_set_input_port_file (klink * sc, FILE * fin);
  void klink_set_input_port_string (klink * sc, char *start,
				     char *past_the_end);
  KLINK_EXPORT void klink_set_output_port_file (klink * sc, FILE * fin);
  void klink_set_output_port_string (klink * sc, char *start,
				      char *past_the_end);
  KLINK_EXPORT void klink_load_file (klink * sc, FILE * fin);
  KLINK_EXPORT void klink_load_named_file (klink * sc, FILE * fin,
					     const char *filename);
  KLINK_EXPORT void klink_load_string (klink * sc, const char *cmd);
  KLINK_EXPORT pko klink_apply0 (klink * sc, const char *procname);
  KLINK_EXPORT int klink_call (klink * sc, pko func, pko args);
  KLINK_EXPORT int klink_eval (klink * sc, pko obj);
  void klink_set_external_data (klink * sc, void *p);
  KLINK_EXPORT void klink_define (klink * sc, pko symbol, pko value);

/*_  . Macros to define type signatures for functions */
  /* Macros to define type signatures for functions.
     One define-macro per function type.

     Naming klink:
     Letters occur in order.  Some cannot occur together.

     P = returns a pko
     B = returns an integer interpreted as a boolean
     S = takes a pointer to the klink interpreter as first argument "sc"
     AN = takes its Kernel arguments as a single object "args"
     A1 = takes one Kernel argument "arg1"
     A2, A3 etc = takes 2 (3, etc) Kernel arguments, named "arg1", "arg2"

     Macro arguments:

     C_NAME: The name that the C function should have.  This can also
     be a (*functype_name) for typedefs.

   */


#define KERNEL_FUN_SIG_b00a1(C_NAME)		\
  int C_NAME(pko arg1)

#define KERNEL_FUN_SIG_b00a2(C_NAME)		\
  int C_NAME(pko arg1, pko arg2)

#define KERNEL_FUN_SIG_bs0a2(C_NAME)		\
  int C_NAME(klink *sc, pko arg1, pko arg2)

#define KERNEL_FUN_SIG_p00a0(C_NAME)		\
  pko C_NAME(void)

#define KERNEL_FUN_SIG_p00a1(C_NAME)		\
  pko C_NAME(pko arg1)

#define KERNEL_FUN_SIG_p00a2(C_NAME)		\
    pko C_NAME(pko arg1, pko arg2)

#define KERNEL_FUN_SIG_p00a3(C_NAME)		\
    pko C_NAME(pko arg1, pko arg2, pko arg3)

#define KERNEL_FUN_SIG_ps0a0(C_NAME)		\
    pko C_NAME(klink *sc)

#define KERNEL_FUN_SIG_ps0a1(C_NAME)		\
    pko C_NAME(klink *sc, pko arg1)

#define KERNEL_FUN_SIG_ps0a2(C_NAME)				\
    pko C_NAME(klink *sc, pko arg1, pko arg2)

#define KERNEL_FUN_SIG_ps0a3(C_NAME)				\
  pko C_NAME(klink *sc, pko arg1, pko arg2,	\
		 pko arg3)

#define KERNEL_FUN_SIG_ps0a4(C_NAME)				\
  pko C_NAME(klink *sc, pko arg1, pko arg2,	\
		 pko arg3, pko arg4)

#define KERNEL_FUN_SIG_ps0a5(C_NAME)				\
  pko C_NAME(klink *sc, pko arg1, pko arg2,		\
		 pko arg3, pko arg4, pko arg5)

#define KERNEL_FUN_SIG_vs0a3(C_NAME)				\
  void C_NAME(klink *sc, pko arg1, pko arg2, pko arg3)

  #define KERNEL_FUN_SIG_vs0a2(C_NAME)				\
  void C_NAME(klink *sc, pko arg1, pko arg2)

  /* Typedefs of the functions. */
#define FFTYPE(X,Y) typedef KERNEL_FUN_SIG_##X((*kernel_f_##X));
#include "fftypes.inc"
#undef FFTYPE

  /* Union of all function types */
  typedef union kernel_f_union
  {
    void *dummy;		/* So we can initialize the union. */
#define FFTYPE(X,Y) kernel_f_##X f_##X;
#include "fftypes.inc"
#undef FFTYPE
  } kernel_f_union;

  /* Enumeration of function types */
  typedef enum klink_ftype_enum
  {
    klink_ftype_dummy,	/* Allowing 0 would be risky. */
#define FFTYPE(X,Y) klink_ftype_##X,
#include "fftypes.inc"
#undef FFTYPE
    klink_ftype_beyond,
  } klink_ftype_enum;

  /* Data for Kernel to know about a function */
  typedef struct kt_cfunc
  {
    kernel_f_union   func;
    klink_ftype_enum type;
    pko              argcheck;
    pko              rettype;
  } kt_cfunc;

/*_  . Internal functions that are exported */

  pko mk_pair (pko a, pko b);
  pko mk_mutable_pair (pko a, pko b);
  pko mk_integer (long num);
  pko mk_real (double num);
  pko mk_symbol (const char *name);
  pko mk_string (const char *str);
  pko mk_counted_string (const char *str, int len);
  pko mk_empty_string (int len, char fill);
  pko mk_character (int c);
  pko mk_cfunc (const kt_cfunc * f);

  void putstr (klink * sc, const char *s);
  int list_length (pko a);
  int eqv (pko a, pko b);


#if USE_INTERFACE
  struct klink_interface
  {
    void (*klink_define) (klink * sc, pko symbol, pko value);
    pko (*mcons) (pko a, pko b);
    pko (*cons) (pko a, pko b);
    pko (*mk_integer) (long num);
    pko (*mk_real) (double num);
    pko (*mk_symbol) (const char *name);
    pko (*mk_string) (const char *str);
    pko (*mk_counted_string) (const char *str, int len);
    pko (*mk_character) (int c);
    pko (*mk_vector) (int len, pko fill);
    void (*putstr) (klink * sc, const char *s);
    void (*putcharacter) (klink * sc, int c);

    int (*is_string) (pko p);
    char *(*string_value) (pko p);
    int (*is_number) (pko p);
    num (*nvalue) (pko p);
    long (*ivalue) (pko p);
    double (*rvalue) (pko p);
    int (*is_integer) (pko p);
    int (*is_real) (pko p);
    int (*is_character) (pko p);
    long (*charvalue) (pko p);
    int (*is_list) (pko p);
    int (*is_vector) (pko p);
    int (*list_length) (pko vec);
    int (*vector_length) (pko vec);
    void (*fill_vector) (pko vec, pko elem);
    pko (*vector_elem) (pko vec, int ielem);
    void (*set_vector_elem) (pko vec, int ielem, pko newel);
    int (*is_port) (pko p);

    int (*is_pair) (pko p);
    pko (*pair_car) (klink * sc, pko p);
    pko (*pair_cdr) (klink * sc, pko p);
    pko (*set_car) (klink * sc, pko p, pko q);
    pko (*set_cdr) (klink * sc, pko p, pko q);

    int (*is_symbol) (pko p);
    char *(*symname) (klink * sc, pko p);

    int (*is_continuation) (pko p);
    int (*is_environment) (pko p);
    int (*is_immutable) (pko p);
    void (*setimmutable) (pko p);
    void (*load_file) (klink * sc, FILE * fin);
    void (*load_string) (klink * sc, const char *input);
  };
#endif

  typedef struct kernel_registerable
  {
    const char *const name;
    const pko data;
  } kernel_registerable;

  void
  k_register_list (const kernel_registerable * list, int count, pko env);


/*_  . Pairs */
/*_   , Define mcons */

#define mcons(a,b) v2cons(T_PAIR,a,b)
#define cons(a,b)  v2cons(T_PAIR | T_IMMUTABLE,a,b)

/*_   , Define c{ad}*r etc */
  /* To be used with WITH_REPORTER */
#define car(p) (v2car(_err_reporter,T_PAIR,p))
#define cdr(p) (v2cdr(_err_reporter,T_PAIR,p))
#define caar(p)          car(car(p))
#define cadr(p)          car(cdr(p))
#define cdar(p)          cdr(car(p))
#define cddr(p)          cdr(cdr(p))
#define cadar(p)         car(cdr(car(p)))
#define caddr(p)         car(cdr(cdr(p)))
#define cdaar(p)         cdr(car(car(p)))
#define cadaar(p)        car(cdr(car(car(p))))
#define cadddr(p)        car(cdr(cdr(cdr(p))))
#define cddddr(p)        cdr(cdr(cdr(cdr(p))))

/*_ , Footers */
/*_  . End C++ nonmangling */
#ifdef __cplusplus
}
#endif
  
/*_  . End Once */
#endif


/*
Local variables:
c-file-style: "gnu"
mode: allout
End:
*/
